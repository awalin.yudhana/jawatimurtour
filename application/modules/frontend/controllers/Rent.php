<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rent extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('twig');
    }

    public function car()
    {
        $this->twig->display('rent-car');
    }
}
