<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->library('twig');
	}

	public function index()
	{
	    //echo "a";
        //$this->twig->addGlobal('session', $this->session);
        $this->twig->display('home');
	}
}
