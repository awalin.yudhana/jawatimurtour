<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tours extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('twig');
    }

    public function index()
    {
        $this->twig->display('tours');
    }

    public function bromo_midnight()
    {
        $this->twig->display('tours/bromo-midnight');
    }

    public function menjangan_fullday()
    {
        $this->twig->display('tours/menjangan-fullday');
    }

    public function sempu_fullday()
    {
        $this->twig->display('tours/sempu-fullday');
    }

    public function sempu_2d1n()
    {
        $this->twig->display('tours/sempu-2d1n');
    }

    public function ijen_2d1n()
    {
        $this->twig->display('tours/ijen-2d1n');
    }

    public function malang_batu_city_tour_fullday()
    {
        $this->twig->display('tours/malang-batu-fullday');
    }

    public function malang_batu_city_tour_2d1n()
    {
        $this->twig->display('tours/malang-batu-2d1n');
    }

    public function malang_batu_city_tour_3d2n()
    {
        $this->twig->display('tours/malang-batu-3d2n');
    }

    public function bawean_4D3N()
    {
        $this->twig->display('tours/bawean-4d3n');
    }

    public function banyuwangi_3D2N()
    {
        $this->twig->display('tours/banyuwangi-3d2n');
    }

    public function banyuwangi_4D3N()
    {
        $this->twig->display('tours/banyuwangi-4d3n');
    }

    public function request()
    {
        $this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Nama', 'required');
        $this->form_validation->set_rules('country', 'Kewarganegaraan', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('phone', 'No Telp', 'required');
        $this->form_validation->set_rules('tours', 'Paket Trip', 'required');
        $this->form_validation->set_rules('check_in', 'Check In', 'required');
        $this->form_validation->set_rules('check_out', 'Check Out', 'required');
        $this->form_validation->set_rules('adults', 'Dewasa', 'required');
        $this->form_validation->set_rules('children', 'Anak - anak', 'required');
        $this->form_validation->set_rules('message', 'Pesan', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', 'data tidak lengkap');
        } else {
            $this->session->set_flashdata('success', 'terima kasih telah mengisi form request, kami akan menghubungi anda.');
            $this->db->insert('request', $this->input->post());
        }
        redirect(site_url('frontend/home'));
    }
}
