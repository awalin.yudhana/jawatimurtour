<?php

/* layout/_post_detail_form.twig */
class __TwigTemplate_f2fa05458e75763eeb508b347ff3f45ef415b9558a59bbe0b67dc9c5d6851077 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"grid_6\">
    <h3 style=\"padding-top: 0px !important;\">Booking Form</h3>
    ";
        // line 3
        $this->loadTemplate("layout/_paket_form.twig", "layout/_post_detail_form.twig", 3)->display($context);
        // line 4
        echo "</div>
<div class=\"grid_3\">
    <div class=\"map\">
        <figure class=\"\">
            <div style=\"height:311px;width:378px;max-width:100%;list-style:none; transition: none;overflow:hidden;\"><div id=\"embedded-map-canvas\" style=\"height:100%; width:100%;max-width:100%;\"><iframe style=\"height:100%;width:100%;border:0;\" frameborder=\"0\" src=\"https://www.google.com/maps/embed/v1/place?q=jawatimurtour&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU\"></iframe></div><a class=\"embed-map-html\" rel=\"nofollow\" href=\"http://www.dog-checks.com\" id=\"make-map-data\">dog checks</a><style>#embedded-map-canvas img{max-width:none!important;background:none!important;}</style></div><script src=\"https://www.dog-checks.com/google-maps-authorization.js?id=4035a2f4-be9e-0f1a-6ae9-1f9a860de15e&c=embed-map-html&u=1463027788\" defer=\"defer\" async=\"async\"></script>
        </figure>
        <address>
            <dl>
                <dt>JAWATIMURTOUR OFFICE. <br>
                    Griya Tanjung Priok Jaya 1 <br>
                    Blok F-6,<br>
                    Bakalan Krajan 65148, Sukun, Malang.
                </dt>
                <dd><span>Office Hours : </span><br> 08:30 AM - 17:00 PM</dd>
                <dd><span>Telephone : </span><br>+8261336661922</dd>
                <dd>E-mail : <br><a href=\"#\" class=\"col1\">service@jawatimurtour.com</a></dd>
            </dl>
        </address>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "layout/_post_detail_form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 4,  23 => 3,  19 => 1,);
    }
}
/* <div class="grid_6">*/
/*     <h3 style="padding-top: 0px !important;">Booking Form</h3>*/
/*     {% include 'layout/_paket_form.twig' %}*/
/* </div>*/
/* <div class="grid_3">*/
/*     <div class="map">*/
/*         <figure class="">*/
/*             <div style="height:311px;width:378px;max-width:100%;list-style:none; transition: none;overflow:hidden;"><div id="embedded-map-canvas" style="height:100%; width:100%;max-width:100%;"><iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=jawatimurtour&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU"></iframe></div><a class="embed-map-html" rel="nofollow" href="http://www.dog-checks.com" id="make-map-data">dog checks</a><style>#embedded-map-canvas img{max-width:none!important;background:none!important;}</style></div><script src="https://www.dog-checks.com/google-maps-authorization.js?id=4035a2f4-be9e-0f1a-6ae9-1f9a860de15e&c=embed-map-html&u=1463027788" defer="defer" async="async"></script>*/
/*         </figure>*/
/*         <address>*/
/*             <dl>*/
/*                 <dt>JAWATIMURTOUR OFFICE. <br>*/
/*                     Griya Tanjung Priok Jaya 1 <br>*/
/*                     Blok F-6,<br>*/
/*                     Bakalan Krajan 65148, Sukun, Malang.*/
/*                 </dt>*/
/*                 <dd><span>Office Hours : </span><br> 08:30 AM - 17:00 PM</dd>*/
/*                 <dd><span>Telephone : </span><br>+8261336661922</dd>*/
/*                 <dd>E-mail : <br><a href="#" class="col1">service@jawatimurtour.com</a></dd>*/
/*             </dl>*/
/*         </address>*/
/*     </div>*/
/* </div>*/
