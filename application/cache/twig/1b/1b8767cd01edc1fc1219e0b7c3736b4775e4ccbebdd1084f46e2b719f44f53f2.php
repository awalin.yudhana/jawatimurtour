<?php

/* layout/_header.twig */
class __TwigTemplate_7c0f6de30f291d28d8dbfa777761afbed0cba891dfc5de984e733f932f71ea5b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header>
    <div class=\"container_12\">
        <div class=\"grid_12\">
            <div class=\"menu_block\">
                <nav class=\"horizontal-nav full-width horizontalNav-notprocessed\">
                    <ul class=\"sf-menu\">
                        <li ";
        // line 7
        if ((current_url() == site_url("frontend/home"))) {
            // line 8
            echo "                                class=\"current\"
                            ";
        }
        // line 9
        echo " >
                            <a href=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("frontend/home"), "html", null, true);
        echo "\">Beranda</a>
                        </li>
                        <li ";
        // line 12
        if ((current_url() == site_url("frontend/tours"))) {
            // line 13
            echo "                                class=\"current\"
                            ";
        }
        // line 14
        echo ">
                            <a href=\"";
        // line 15
        echo twig_escape_filter($this->env, site_url("frontend/tours"), "html", null, true);
        echo "\">Paket Wisata</a></li>
                        <li ";
        // line 16
        if ((current_url() == site_url("frontend/terms"))) {
            // line 17
            echo "                                class=\"current\"
                            ";
        }
        // line 18
        echo ">
                            <a href=\"";
        // line 19
        echo twig_escape_filter($this->env, site_url("frontend/terms"), "html", null, true);
        echo "\">Ketentuan</a>
                        </li>
                        <li ";
        // line 21
        if ((current_url() == site_url("frontend/service"))) {
            // line 22
            echo "                                class=\"current\"
                            ";
        }
        // line 23
        echo ">
                            <a href=\"";
        // line 24
        echo twig_escape_filter($this->env, site_url("frontend/service"), "html", null, true);
        echo "\">Layanan</a>
                        </li>
                        <li ";
        // line 26
        if ((current_url() == site_url("frontend/contact"))) {
            // line 27
            echo "                                class=\"current\"
                            ";
        }
        // line 28
        echo " >
                            <a href=\"";
        // line 29
        echo twig_escape_filter($this->env, site_url("frontend/contact"), "html", null, true);
        echo "\">Hubungi</a>
                        </li>
                    </ul>
                </nav>
                <div class=\"clear\"></div>
            </div>
        </div>
        <div class=\"grid_12\">
            <h1>
                <a href=\"";
        // line 38
        echo twig_escape_filter($this->env, site_url("frontend/home"), "html", null, true);
        echo "\">
                    <img src=\"";
        // line 39
        echo twig_escape_filter($this->env, site_url("assets/images/logo.png"), "html", null, true);
        echo "\" alt=\"Make Your Trip Easier - Jawatimurtour\">
                </a>
            </h1>
        </div>
    </div>
</header>";
    }

    public function getTemplateName()
    {
        return "layout/_header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 39,  103 => 38,  91 => 29,  88 => 28,  84 => 27,  82 => 26,  77 => 24,  74 => 23,  70 => 22,  68 => 21,  63 => 19,  60 => 18,  56 => 17,  54 => 16,  50 => 15,  47 => 14,  43 => 13,  41 => 12,  36 => 10,  33 => 9,  29 => 8,  27 => 7,  19 => 1,);
    }
}
/* <header>*/
/*     <div class="container_12">*/
/*         <div class="grid_12">*/
/*             <div class="menu_block">*/
/*                 <nav class="horizontal-nav full-width horizontalNav-notprocessed">*/
/*                     <ul class="sf-menu">*/
/*                         <li {% if current_url() ==  site_url('frontend/home') %}*/
/*                                 class="current"*/
/*                             {% endif %} >*/
/*                             <a href="{{ site_url('frontend/home') }}">Beranda</a>*/
/*                         </li>*/
/*                         <li {% if current_url() ==  site_url('frontend/tours') %}*/
/*                                 class="current"*/
/*                             {% endif %}>*/
/*                             <a href="{{ site_url('frontend/tours') }}">Paket Wisata</a></li>*/
/*                         <li {% if current_url() ==  site_url('frontend/terms') %}*/
/*                                 class="current"*/
/*                             {% endif %}>*/
/*                             <a href="{{ site_url('frontend/terms') }}">Ketentuan</a>*/
/*                         </li>*/
/*                         <li {% if current_url() ==  site_url('frontend/service') %}*/
/*                                 class="current"*/
/*                             {% endif %}>*/
/*                             <a href="{{ site_url('frontend/service') }}">Layanan</a>*/
/*                         </li>*/
/*                         <li {% if current_url() ==  site_url('frontend/contact') %}*/
/*                                 class="current"*/
/*                             {% endif %} >*/
/*                             <a href="{{ site_url('frontend/contact') }}">Hubungi</a>*/
/*                         </li>*/
/*                     </ul>*/
/*                 </nav>*/
/*                 <div class="clear"></div>*/
/*             </div>*/
/*         </div>*/
/*         <div class="grid_12">*/
/*             <h1>*/
/*                 <a href="{{ site_url('frontend/home') }}">*/
/*                     <img src="{{ site_url('assets/images/logo.png') }}" alt="Make Your Trip Easier - Jawatimurtour">*/
/*                 </a>*/
/*             </h1>*/
/*         </div>*/
/*     </div>*/
/* </header>*/
