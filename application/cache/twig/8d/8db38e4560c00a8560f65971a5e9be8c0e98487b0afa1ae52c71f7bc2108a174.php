<?php

/* layout/_head.twig */
class __TwigTemplate_78ca17adfb077c3337064e2a7b0309dddbfbf8ee898dd57c21de20d0cd70b425 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<meta charset=\"utf-8\">
<meta name=\"format-detection\" content=\"telephone=no\" />
<link rel=\"icon\" href=\"";
        // line 3
        echo twig_escape_filter($this->env, base_url("assets/favicon.ico"), "html", null, true);
        echo "\">
<link rel=\"shortcut icon\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, base_url("assets/favicon.ico"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, base_url("assets/booking/css/booking.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, base_url("assets/css/camera.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, base_url("assets/css/owl.carousel.css"), "html", null, true);
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, base_url("assets/css/style.css"), "html", null, true);
        echo "\">
<script src=\"";
        // line 9
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 10
        echo twig_escape_filter($this->env, base_url("assets/js/jquery-migrate-1.2.1.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 11
        echo twig_escape_filter($this->env, base_url("assets/js/script.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 12
        echo twig_escape_filter($this->env, base_url("assets/js/superfish.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 13
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.ui.totop.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 14
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.equalheights.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 15
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.mobilemenu.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 16
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.easing.1.3.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 17
        echo twig_escape_filter($this->env, base_url("assets/js/owl.carousel.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 18
        echo twig_escape_filter($this->env, base_url("assets/js/camera.js"), "html", null, true);
        echo "\"></script>
<!--[if (gt IE 9)|!(IE)]><!-->
<script src=\"";
        // line 20
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.mobile.customized.min.js"), "html", null, true);
        echo "\"></script>
<!--<![endif]-->


<link rel=\"stylesheet\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, base_url("assets/css/form.css"), "html", null, true);
        echo "\">
<script src=\"";
        // line 25
        echo twig_escape_filter($this->env, base_url("assets/js/TMForm.js"), "html", null, true);
        echo "\"></script>


<script src=\"";
        // line 28
        echo twig_escape_filter($this->env, base_url("assets/booking/js/jquery-ui-1.10.3.custom.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 29
        echo twig_escape_filter($this->env, base_url("assets/booking/js/jquery.fancyform.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 30
        echo twig_escape_filter($this->env, base_url("assets/booking/js/jquery.placeholder.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 31
        echo twig_escape_filter($this->env, base_url("assets/booking/js/regula.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 32
        echo twig_escape_filter($this->env, base_url("assets/booking/js/booking.js"), "html", null, true);
        echo "\"></script>
<script>
    \$(document).ready(function(){
        jQuery('#camera_wrap').camera({
            loader: false,
            pagination: false ,
            minHeight: '444',
            thumbnails: false,
            height: '48.375%',
            caption: true,
            navigation: true,
            fx: 'mosaic'
        });
        /*carousel*/
        var owl=\$(\"#owl\");
        owl.owlCarousel({
            items : 2, //10 items above 1000px browser width
            itemsDesktop : [995,2], //5 items between 1000px and 901px
            itemsDesktopSmall : [767, 2], // betweem 900px and 601px
            itemsTablet: [700, 2], //2 items between 600 and 0
            itemsMobile : [479, 1], // itemsMobile disabled - inherit from itemsTablet option
            navigation : true,
            pagination : false
        });
        \$().UItoTop({ easingType: 'easeOutQuart' });
    });
</script>
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
    <a href=\"http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode\">
        <img src=\"http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg\" border=\"0\" height=\"42\" width=\"820\" alt=\"You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today.\" />
    </a>
</div>
<![endif]-->
<!--[if lt IE 9]>
<script src=\"";
        // line 67
        echo twig_escape_filter($this->env, base_url("assets/js/html5shiv.js"), "html", null, true);
        echo "\"></script>
<link rel=\"stylesheet\" media=\"screen\" href=\"";
        // line 68
        echo twig_escape_filter($this->env, base_url("assets/css/ie.css"), "html", null, true);
        echo "\">
<![endif]-->";
    }

    public function getTemplateName()
    {
        return "layout/_head.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 68,  159 => 67,  121 => 32,  117 => 31,  113 => 30,  109 => 29,  105 => 28,  99 => 25,  95 => 24,  88 => 20,  83 => 18,  79 => 17,  75 => 16,  71 => 15,  67 => 14,  63 => 13,  59 => 12,  55 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  35 => 6,  31 => 5,  27 => 4,  23 => 3,  19 => 1,);
    }
}
/* <meta charset="utf-8">*/
/* <meta name="format-detection" content="telephone=no" />*/
/* <link rel="icon" href="{{ base_url('assets/favicon.ico') }}">*/
/* <link rel="shortcut icon" href="{{ base_url('assets/favicon.ico') }}" />*/
/* <link rel="stylesheet" href="{{ base_url('assets/booking/css/booking.css') }}">*/
/* <link rel="stylesheet" href="{{ base_url('assets/css/camera.css') }}">*/
/* <link rel="stylesheet" href="{{ base_url('assets/css/owl.carousel.css') }}">*/
/* <link rel="stylesheet" href="{{ base_url('assets/css/style.css') }}">*/
/* <script src="{{ base_url('assets/js/jquery.js') }}"></script>*/
/* <script src="{{ base_url('assets/js/jquery-migrate-1.2.1.js') }}"></script>*/
/* <script src="{{ base_url('assets/js/script.js') }}"></script>*/
/* <script src="{{ base_url('assets/js/superfish.js') }}"></script>*/
/* <script src="{{ base_url('assets/js/jquery.ui.totop.js') }}"></script>*/
/* <script src="{{ base_url('assets/js/jquery.equalheights.js') }}"></script>*/
/* <script src="{{ base_url('assets/js/jquery.mobilemenu.js') }}"></script>*/
/* <script src="{{ base_url('assets/js/jquery.easing.1.3.js') }}"></script>*/
/* <script src="{{ base_url('assets/js/owl.carousel.js') }}"></script>*/
/* <script src="{{ base_url('assets/js/camera.js') }}"></script>*/
/* <!--[if (gt IE 9)|!(IE)]><!-->*/
/* <script src="{{ base_url('assets/js/jquery.mobile.customized.min.js') }}"></script>*/
/* <!--<![endif]-->*/
/* */
/* */
/* <link rel="stylesheet" href="{{ base_url('assets/css/form.css') }}">*/
/* <script src="{{ base_url('assets/js/TMForm.js') }}"></script>*/
/* */
/* */
/* <script src="{{ base_url('assets/booking/js/jquery-ui-1.10.3.custom.min.js') }}"></script>*/
/* <script src="{{ base_url('assets/booking/js/jquery.fancyform.js') }}"></script>*/
/* <script src="{{ base_url('assets/booking/js/jquery.placeholder.js') }}"></script>*/
/* <script src="{{ base_url('assets/booking/js/regula.js') }}"></script>*/
/* <script src="{{ base_url('assets/booking/js/booking.js') }}"></script>*/
/* <script>*/
/*     $(document).ready(function(){*/
/*         jQuery('#camera_wrap').camera({*/
/*             loader: false,*/
/*             pagination: false ,*/
/*             minHeight: '444',*/
/*             thumbnails: false,*/
/*             height: '48.375%',*/
/*             caption: true,*/
/*             navigation: true,*/
/*             fx: 'mosaic'*/
/*         });*/
/*         /*carousel*//* */
/*         var owl=$("#owl");*/
/*         owl.owlCarousel({*/
/*             items : 2, //10 items above 1000px browser width*/
/*             itemsDesktop : [995,2], //5 items between 1000px and 901px*/
/*             itemsDesktopSmall : [767, 2], // betweem 900px and 601px*/
/*             itemsTablet: [700, 2], //2 items between 600 and 0*/
/*             itemsMobile : [479, 1], // itemsMobile disabled - inherit from itemsTablet option*/
/*             navigation : true,*/
/*             pagination : false*/
/*         });*/
/*         $().UItoTop({ easingType: 'easeOutQuart' });*/
/*     });*/
/* </script>*/
/* <!--[if lt IE 8]>*/
/* <div style=' clear: both; text-align:center; position: relative;'>*/
/*     <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">*/
/*         <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />*/
/*     </a>*/
/* </div>*/
/* <![endif]-->*/
/* <!--[if lt IE 9]>*/
/* <script src="{{ base_url('assets/js/html5shiv.js') }}"></script>*/
/* <link rel="stylesheet" media="screen" href="{{ base_url('assets/css/ie.css') }}">*/
/* <![endif]-->*/
