<?php

/* layout/_footer_script.twig */
class __TwigTemplate_fb4d86c7e417dc155b7de3638e7ad217472f001195a8a788333f40a27b106cce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<!--Start of Tawk.to Script-->
<script type=\"text/javascript\">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement(\"script\"),s0=document.getElementsByTagName(\"script\")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/573e922f2bbab01a3c1b4779/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->";
    }

    public function getTemplateName()
    {
        return "layout/_footer_script.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* */
/* <!--Start of Tawk.to Script-->*/
/* <script type="text/javascript">*/
/*     var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();*/
/*     (function(){*/
/*         var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];*/
/*         s1.async=true;*/
/*         s1.src='https://embed.tawk.to/573e922f2bbab01a3c1b4779/default';*/
/*         s1.charset='UTF-8';*/
/*         s1.setAttribute('crossorigin','*');*/
/*         s0.parentNode.insertBefore(s1,s0);*/
/*     })();*/
/* </script>*/
/* <!--End of Tawk.to Script-->*/
