<?php

/* layout/_footer.twig */
class __TwigTemplate_b14b7be981c07a7a7c61824480325cdaa6037de8022abf4ab95db7495a56890d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<footer>
    <div class=\"container_12\">
        <div class=\"grid_12\">
            <div class=\"socials\">
                <a href=\"https://www.facebook.com/yudhanatravel/\" class=\"fa fa-facebook\"></a>
                <a href=\"#\" class=\"fa fa-twitter\"></a>
                <a href=\"#\" class=\"fa fa-google-plus\"></a>
            </div>
            <div class=\"copy\">
                (c) Jawatimurtour 2015 | Powered by <a href=\"http://www.templatemonster.com/\" rel=\"nofollow\">nanomit.es</a>
            </div>
        </div>
    </div>
</footer>";
    }

    public function getTemplateName()
    {
        return "layout/_footer.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <footer>*/
/*     <div class="container_12">*/
/*         <div class="grid_12">*/
/*             <div class="socials">*/
/*                 <a href="https://www.facebook.com/yudhanatravel/" class="fa fa-facebook"></a>*/
/*                 <a href="#" class="fa fa-twitter"></a>*/
/*                 <a href="#" class="fa fa-google-plus"></a>*/
/*             </div>*/
/*             <div class="copy">*/
/*                 (c) Jawatimurtour 2015 | Powered by <a href="http://www.templatemonster.com/" rel="nofollow">nanomit.es</a>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </footer>*/
