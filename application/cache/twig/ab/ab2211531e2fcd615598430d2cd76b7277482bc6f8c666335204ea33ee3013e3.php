<?php

/* layout/main.twig */
class __TwigTemplate_fa5896ec4e0dccd0ee411146d0e662891c64c789590b6e00d2c3ebafb8a16e55 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'slider' => array($this, 'block_slider'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <title>";
        // line 4
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 5
        $this->loadTemplate("layout/_head.twig", "layout/main.twig", 5)->display($context);
        // line 6
        echo "</head>
<body
        ";
        // line 8
        if (((current_url() == site_url("frontend/home")) || (current_url() == site_url()))) {
            // line 9
            echo "            class=\"page1\" id=\"top\"
        ";
        }
        // line 11
        echo ">
<!--==============================header=================================-->
";
        // line 13
        $this->loadTemplate("layout/_header.twig", "layout/main.twig", 13)->display($context);
        // line 14
        $this->displayBlock('slider', $context, $blocks);
        // line 15
        echo "<!--==============================Content=================================-->
<div class=\"content\">
    ";
        // line 17
        $this->displayBlock('content', $context, $blocks);
        // line 18
        echo "</div>
<!--==============================footer=================================-->
";
        // line 20
        $this->loadTemplate("layout/_footer.twig", "layout/main.twig", 20)->display($context);
        // line 21
        echo "</body>
</html>

";
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
    }

    // line 14
    public function block_slider($context, array $blocks = array())
    {
    }

    // line 17
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "layout/main.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 17,  75 => 14,  70 => 4,  63 => 21,  61 => 20,  57 => 18,  55 => 17,  51 => 15,  49 => 14,  47 => 13,  43 => 11,  39 => 9,  37 => 8,  33 => 6,  31 => 5,  27 => 4,  22 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/* <head>*/
/*     <title>{% block title %}{% endblock %}</title>*/
/*     {% include 'layout/_head.twig' %}*/
/* </head>*/
/* <body*/
/*         {% if current_url() ==  site_url('frontend/home') or current_url() == site_url() %}*/
/*             class="page1" id="top"*/
/*         {% endif %}*/
/* >*/
/* <!--==============================header=================================-->*/
/* {% include 'layout/_header.twig' %}*/
/* {% block slider %}{% endblock %}*/
/* <!--==============================Content=================================-->*/
/* <div class="content">*/
/*     {% block content %}{% endblock %}*/
/* </div>*/
/* <!--==============================footer=================================-->*/
/* {% include 'layout/_footer.twig' %}*/
/* </body>*/
/* </html>*/
/* */
/* */
