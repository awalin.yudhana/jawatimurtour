<?php

/* tours.twig */
class __TwigTemplate_702089947c3f7585ffc248ddc6aed2308d19c36804f523dacd815bc730ce375b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout/main.twig", "tours.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/main.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "    Contact Us - JAWATIMURTOUR
";
    }

    // line 6
    public function block_content($context, array $blocks = array())
    {
        // line 7
        echo "    <div class=\"container_12\">
        <div class=\"banners\">
            <div class=\"grid_4\">
                <div class=\"banner\">
                    <img src=\"";
        // line 11
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-beach-fullday.jpg"), "html", null, true);
        echo "\" alt=\"\">
                    <div class=\"label\">
                        <div class=\"title\">Fullday</div>
                        <div class=\"title\">Beach Of Malang</div>
                        <div class=\"price\"><span>CALL</span></div>
                        ";
        // line 17
        echo "                    </div>
                </div>
            </div>
            <div class=\"grid_4\">
                <div class=\"banner\">
                    <img src=\"";
        // line 22
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-sempu.jpg"), "html", null, true);
        echo "\" alt=\"\">
                    <div class=\"label\">
                        <div class=\"title\">Fullday</div>
                        <div class=\"title\">Sempu Island</div>
                        <div class=\"price\">from<span>Rp 399.999</span></div>
                        <a href=\"";
        // line 27
        echo twig_escape_filter($this->env, site_url("frontend/tours/sempu-fullday"), "html", null, true);
        echo "\">LEARN MORE</a>
                    </div>
                </div>
            </div>
            <div class=\"grid_4\">
                <div class=\"banner\">
                    <img src=\"";
        // line 33
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-sempu-2d1n.jpg"), "html", null, true);
        echo "\" alt=\"\">
                    <div class=\"label\">
                        <div class=\"title\">2D1N</div>
                        <div class=\"title\">Sempu Island</div>
                        <div class=\"price\">from<span>Rp 199.999</span></div>
                        <a href=\"";
        // line 38
        echo twig_escape_filter($this->env, site_url("frontend/tours/sempu-2d1n"), "html", null, true);
        echo "\">LEARN MORE</a>
                    </div>
                </div>
            </div>
            <div class=\"clear\"></div>
            <div class=\"grid_4\">
                <div class=\"banner\">
                    <img src=\"";
        // line 45
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-citytour-fullday-1.jpg"), "html", null, true);
        echo "\" alt=\"\">
                    <div class=\"label\">
                        <div class=\"title\">Fullday</div>
                        <div class=\"title\">Batu City Tour</div>
                        <div class=\"price\"><span>CALL</span></div>
                        ";
        // line 51
        echo "                    </div>
                </div>
            </div>
            <div class=\"grid_4\">
                <div class=\"banner\">
                    <img src=\"";
        // line 56
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-citytour-2d1n.jpg"), "html", null, true);
        echo "\" alt=\"\">
                    <div class=\"label\">
                        <div class=\"title\">2D1N</div>
                        <div class=\"title\">City Tour</div>
                        <div class=\"price\">from<span>Rp 850.000</span></div>
                        <a href=\"";
        // line 61
        echo twig_escape_filter($this->env, site_url("frontend/tours/malang-batu-city-tour-2d1n"), "html", null, true);
        echo "\">LEARN MORE</a>
                    </div>
                </div>
            </div>
            <div class=\"grid_4\">
                <div class=\"banner\">
                    <img src=\"";
        // line 67
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-citytour-3d1n.jpg"), "html", null, true);
        echo "\" alt=\"\">
                    <div class=\"label\">
                        <div class=\"title\">3D2N</div>
                        <div class=\"title\">City Tour</div>
                        <div class=\"price\"><span>CALL</span></div>
                        ";
        // line 73
        echo "                    </div>
                </div>
            </div>
            <div class=\"clear\"></div>
            <div class=\"grid_4\">
                <div class=\"banner\">
                    <img src=\"";
        // line 79
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-bromo.jpg"), "html", null, true);
        echo "\" alt=\"\">
                    <div class=\"label\">
                        <div class=\"title\">Midnight</div>
                        <div class=\"title\">Bromo</div>
                        <div class=\"price\">from<span>Rp 375.000</span></div>
                        <a href=\"";
        // line 84
        echo twig_escape_filter($this->env, site_url("frontend/tours/bromo-midnight"), "html", null, true);
        echo "\">LEARN MORE</a>
                    </div>
                </div>
            </div>
            <div class=\"grid_4\">
                <div class=\"banner\">
                    <img src=\"";
        // line 90
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-menjangan.jpg"), "html", null, true);
        echo "\" alt=\"\">
                    <div class=\"label\">
                        <div class=\"title\">Snorkeling</div>
                        <div class=\"title\">Menjangan Island</div>
                        <div class=\"price\">from<span>Rp 450.000</span></div>
                        <a href=\"";
        // line 95
        echo twig_escape_filter($this->env, site_url("frontend/tours/menjangan-fullday"), "html", null, true);
        echo "\">LEARN MORE</a>
                    </div>
                </div>
            </div>
            <div class=\"grid_4\">
                <div class=\"banner\">
                    <img src=\"";
        // line 101
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-ijen.jpg"), "html", null, true);
        echo "\" alt=\"\">
                    <div class=\"label\">
                        <div class=\"title\">Bluefire</div>
                        <div class=\"title\">Ijen Crater</div>
                        <div class=\"price\">from<span>Rp 699.999</span></div>
                        <a href=\"";
        // line 106
        echo twig_escape_filter($this->env, site_url("frontend/tours/ijen-2d1n"), "html", null, true);
        echo "\">LEARN MORE</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "tours.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 106,  178 => 101,  169 => 95,  161 => 90,  152 => 84,  144 => 79,  136 => 73,  128 => 67,  119 => 61,  111 => 56,  104 => 51,  96 => 45,  86 => 38,  78 => 33,  69 => 27,  61 => 22,  54 => 17,  46 => 11,  40 => 7,  37 => 6,  32 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends 'layout/main.twig' %}*/
/* {% block title %}*/
/*     Contact Us - JAWATIMURTOUR*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="container_12">*/
/*         <div class="banners">*/
/*             <div class="grid_4">*/
/*                 <div class="banner">*/
/*                     <img src="{{ base_url('assets/images/thumbnail-beach-fullday.jpg') }}" alt="">*/
/*                     <div class="label">*/
/*                         <div class="title">Fullday</div>*/
/*                         <div class="title">Beach Of Malang</div>*/
/*                         <div class="price"><span>CALL</span></div>*/
/*                         {#<a href="#">LEARN MORE</a>#}*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="grid_4">*/
/*                 <div class="banner">*/
/*                     <img src="{{ base_url('assets/images/thumbnail-sempu.jpg') }}" alt="">*/
/*                     <div class="label">*/
/*                         <div class="title">Fullday</div>*/
/*                         <div class="title">Sempu Island</div>*/
/*                         <div class="price">from<span>Rp 399.999</span></div>*/
/*                         <a href="{{ site_url('frontend/tours/sempu-fullday') }}">LEARN MORE</a>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="grid_4">*/
/*                 <div class="banner">*/
/*                     <img src="{{ base_url('assets/images/thumbnail-sempu-2d1n.jpg') }}" alt="">*/
/*                     <div class="label">*/
/*                         <div class="title">2D1N</div>*/
/*                         <div class="title">Sempu Island</div>*/
/*                         <div class="price">from<span>Rp 199.999</span></div>*/
/*                         <a href="{{ site_url('frontend/tours/sempu-2d1n') }}">LEARN MORE</a>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="clear"></div>*/
/*             <div class="grid_4">*/
/*                 <div class="banner">*/
/*                     <img src="{{ base_url('assets/images/thumbnail-citytour-fullday-1.jpg') }}" alt="">*/
/*                     <div class="label">*/
/*                         <div class="title">Fullday</div>*/
/*                         <div class="title">Batu City Tour</div>*/
/*                         <div class="price"><span>CALL</span></div>*/
/*                         {#<a href="#">LEARN MORE</a>#}*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="grid_4">*/
/*                 <div class="banner">*/
/*                     <img src="{{ base_url('assets/images/thumbnail-citytour-2d1n.jpg') }}" alt="">*/
/*                     <div class="label">*/
/*                         <div class="title">2D1N</div>*/
/*                         <div class="title">City Tour</div>*/
/*                         <div class="price">from<span>Rp 850.000</span></div>*/
/*                         <a href="{{ site_url('frontend/tours/malang-batu-city-tour-2d1n') }}">LEARN MORE</a>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="grid_4">*/
/*                 <div class="banner">*/
/*                     <img src="{{ base_url('assets/images/thumbnail-citytour-3d1n.jpg') }}" alt="">*/
/*                     <div class="label">*/
/*                         <div class="title">3D2N</div>*/
/*                         <div class="title">City Tour</div>*/
/*                         <div class="price"><span>CALL</span></div>*/
/*                         {#<a href="#">LEARN MORE</a>#}*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="clear"></div>*/
/*             <div class="grid_4">*/
/*                 <div class="banner">*/
/*                     <img src="{{ base_url('assets/images/thumbnail-bromo.jpg') }}" alt="">*/
/*                     <div class="label">*/
/*                         <div class="title">Midnight</div>*/
/*                         <div class="title">Bromo</div>*/
/*                         <div class="price">from<span>Rp 375.000</span></div>*/
/*                         <a href="{{ site_url('frontend/tours/bromo-midnight') }}">LEARN MORE</a>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="grid_4">*/
/*                 <div class="banner">*/
/*                     <img src="{{ base_url('assets/images/thumbnail-menjangan.jpg') }}" alt="">*/
/*                     <div class="label">*/
/*                         <div class="title">Snorkeling</div>*/
/*                         <div class="title">Menjangan Island</div>*/
/*                         <div class="price">from<span>Rp 450.000</span></div>*/
/*                         <a href="{{ site_url('frontend/tours/menjangan-fullday') }}">LEARN MORE</a>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="grid_4">*/
/*                 <div class="banner">*/
/*                     <img src="{{ base_url('assets/images/thumbnail-ijen.jpg') }}" alt="">*/
/*                     <div class="label">*/
/*                         <div class="title">Bluefire</div>*/
/*                         <div class="title">Ijen Crater</div>*/
/*                         <div class="price">from<span>Rp 699.999</span></div>*/
/*                         <a href="{{ site_url('frontend/tours/ijen-2d1n') }}">LEARN MORE</a>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/* {% endblock %}*/
