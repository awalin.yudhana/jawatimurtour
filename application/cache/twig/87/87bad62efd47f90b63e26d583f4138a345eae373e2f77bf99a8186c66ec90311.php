<?php

/* layout/post-detail.twig */
class __TwigTemplate_7babfac89407f0224eb11239ed94e3eda8d9cb97b2a1b0c6cea603f8e6dc9150 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'slider' => array($this, 'block_slider'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <title>";
        // line 4
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 5
        $this->loadTemplate("layout/_head.twig", "layout/post-detail.twig", 5)->display($context);
        // line 6
        echo "</head>
<body
        ";
        // line 8
        if (((current_url() == site_url("frontend/home")) || (current_url() == site_url()))) {
            // line 9
            echo "            class=\"page1\" id=\"top\"
        ";
        }
        // line 11
        echo ">
<!--==============================header=================================-->
";
        // line 13
        $this->loadTemplate("layout/_header.twig", "layout/post-detail.twig", 13)->display($context);
        // line 14
        $this->displayBlock('slider', $context, $blocks);
        // line 15
        echo "<!--==============================Content=================================-->
<div class=\"content\">
    <div class=\"container_12\">
        <div class=\"grid_9\">
            ";
        // line 19
        $this->displayBlock('content', $context, $blocks);
        // line 20
        echo "        </div>
        <div class=\"grid_3\">
            ";
        // line 22
        $this->loadTemplate("layout/_sidebar.twig", "layout/post-detail.twig", 22)->display($context);
        // line 23
        echo "        </div>
    </div>
</div>
<!--==============================footer=================================-->
";
        // line 27
        $this->loadTemplate("layout/_footer.twig", "layout/post-detail.twig", 27)->display($context);
        // line 28
        echo "</body>
</html>

";
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
    }

    // line 14
    public function block_slider($context, array $blocks = array())
    {
    }

    // line 19
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "layout/post-detail.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 19,  85 => 14,  80 => 4,  73 => 28,  71 => 27,  65 => 23,  63 => 22,  59 => 20,  57 => 19,  51 => 15,  49 => 14,  47 => 13,  43 => 11,  39 => 9,  37 => 8,  33 => 6,  31 => 5,  27 => 4,  22 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/* <head>*/
/*     <title>{% block title %}{% endblock %}</title>*/
/*     {% include 'layout/_head.twig' %}*/
/* </head>*/
/* <body*/
/*         {% if current_url() ==  site_url('frontend/home') or current_url() == site_url() %}*/
/*             class="page1" id="top"*/
/*         {% endif %}*/
/* >*/
/* <!--==============================header=================================-->*/
/* {% include 'layout/_header.twig' %}*/
/* {% block slider %}{% endblock %}*/
/* <!--==============================Content=================================-->*/
/* <div class="content">*/
/*     <div class="container_12">*/
/*         <div class="grid_9">*/
/*             {% block content %}{% endblock %}*/
/*         </div>*/
/*         <div class="grid_3">*/
/*             {% include 'layout/_sidebar.twig' %}*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <!--==============================footer=================================-->*/
/* {% include 'layout/_footer.twig' %}*/
/* </body>*/
/* </html>*/
/* */
/* */
