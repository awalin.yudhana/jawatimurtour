<?php

/* tours/malang-batu-2d1n.twig */
class __TwigTemplate_bdf33baa300e834e750098f40e8e3b963aa529c47256dae3ea9088b87600104b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout/paket-detail.twig", "tours/malang-batu-2d1n.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'price_tag' => array($this, 'block_price_tag'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/paket-detail.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "    2D1N - Paket Wisata Malang Batu City Tour | JAWATIMURTOUR
";
    }

    // line 6
    public function block_content($context, array $blocks = array())
    {
        // line 7
        echo "    <div class=\"container_12\">
        <div class=\"grid_9\">
            <h3>2D1N - Paket Wisata Malang Batu City Tour</h3>
            <div class=\"blog\">
                <time datetime=\"\">2D<span>1N</span></time>

                <div class=\"extra_wrapper\">
                    <div class=\"text1 col1\">Bebagai objek wisata berada di Kota ini seperti : Museum Satwa, Batu Secreet Zoo, Eco Greenpark, Museum Angkut, Batu Night Spectacular, Alun - Alun kota Batu, dan masih banyak yang lainnya.</div>
                </div>

                <div class=\"tours-tabs__info\">
                    <div class=\"tours-tabs__info__item\">
                        <div class=\"tours-tabs__info__item__content\">
                            <div class=\"tours-tabs__info__item__icon\"><div class=\"td-clock\"></div></div>
                            <div class=\"tours-tabs__info__item__title\">2 hari</div>
                            <div class=\"tours-tabs__info__item__description\">Durasi</div>
                        </div>
                    </div>
                    <div class=\"tours-tabs__info__item\">
                        <div class=\"tours-tabs__info__item__content\">
                            <div class=\"tours-tabs__info__item__icon\"><i class=\"td-user\"></i></div>
                            <div class=\"tours-tabs__info__item__title\">3 – 60 Tahun</div>
                            <div class=\"tours-tabs__info__item__description\">Usia</div>
                        </div>
                    </div>
                    <div class=\"tours-tabs__info__item\">
                        <div class=\"tours-tabs__info__item__content\">
                            <div class=\"tours-tabs__info__item__icon\"><i class=\"td-kategori\"></i></div>
                            <div class=\"tours-tabs__info__item__title\">Education, City Tour, Culinary, Nature, Shopping, Culture</div>
                            <div class=\"tours-tabs__info__item__description\">Tipe Wisata Dalam Paket Ini</div>
                        </div>
                    </div>
                </div>
                <div class=\"clear\"></div>
                <div class=\"timeline__item\">
                    <div class=\"timeline__item__icon-wrap\">
                        <div class=\"timeline__item__icon\">
                            <div class=\"timeline__item__icon__text\">1</div>
                        </div>
                    </div>
                    <div class=\"timeline__item__content padding-left\">
                        <div class=\"timeline__item__title\">Day 1</div>
                        <div class=\"timeline__item__description\">
                            <div style=\"margin-left: 5pt;\">
                                <table class=\"itenerary\" width=\"100%\">
                                    <colgroup>
                                        <col width=\"17%\">
                                        <col width=\"17%\">
                                        <col >
                                    </colgroup>
                                    <thead>
                                    <th>Mulai</th>
                                    <th>Akhir</th>
                                    <th>Trip Itenerary</th>
                                    </thead>
                                    <tbody valign=\"top\">
                                    <tr style=\"background: #d9d9d9;\">
                                        <td>06.00</td>
                                        <td>06.30</td>
                                        <td>Peserta dijemput di lokasi meeting poin yang ditentukan peserta</td>
                                    </tr>
                                    <tr>
                                        <td>07.30</td>
                                        <td>10.00</td>
                                        <td>Malang City Tour (alun-alun malang dan balai kota malang) + wisata kuliner</td>
                                    </tr>
                                    <tr style=\"background: #d9d9d9;\">
                                        <td>10.00</td>
                                        <td>11.00</td>
                                        <td>Menuju ke Kota Batu</td>
                                    </tr>
                                    <tr >
                                        <td>11.00</td>
                                        <td>14.00</td>
                                        <td>Wisata JatimPark 2</td>
                                    </tr>
                                    <tr style=\"background: #d9d9d9;\">
                                        <td>14.00</td>
                                        <td>15.00</td>
                                        <td>Makan siang</td>
                                    </tr>
                                    <tr >
                                        <td>15.00</td>
                                        <td>18.00</td>
                                        <td>Wisata Musium Angkut</td>
                                    </tr>
                                    <tr style=\"background: #d9d9d9;\">
                                        <td>18.00</td>
                                        <td>19.30</td>
                                        <td>Wisata Alun-Alun Kota Batu + Wisata Kuliner</td>
                                    </tr>
                                    <tr>
                                        <td>19.30</td>
                                        <td>21.00</td>
                                        <td>Wisata BNS (Batu Night Spectaculer)</td>
                                    </tr>
                                    <tr style=\"background: #d9d9d9;\">
                                        <td>21.00</td>
                                        <td> - </td>
                                        <td>Menuju ke Penginapan</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"timeline__item\">
                    <div class=\"timeline__item__icon-wrap\">
                        <div class=\"timeline__item__icon\">
                            <div class=\"timeline__item__icon__text\">2</div>
                        </div>
                    </div>
                    <div class=\"timeline__item__content padding-left\">
                        <div class=\"timeline__item__title\">Day 2</div>
                        <div class=\"timeline__item__description\">
                            <div style=\"margin-left: 5pt;\">
                                <table class=\"itenerary\" width=\"100%\">
                                    <colgroup>
                                        <col width=\"17%\">
                                        <col width=\"17%\">
                                        <col >
                                    </colgroup>
                                    <thead>
                                    <th>Mulai</th>
                                    <th>Akhir</th>
                                    <th>Trip Itenerary</th>
                                    </thead>
                                    <tbody valign=\"top\">
                                    <tr style=\"background: #d9d9d9;\">
                                        <td>07.00</td>
                                        <td>07.30</td>
                                        <td>Breakfast dan penjemputan</td>
                                    </tr>
                                    <tr>
                                        <td>07.30</td>
                                        <td>08.00</td>
                                        <td>Perjalanan menuju wisata petik apel</td>
                                    </tr>
                                    <tr style=\"background: #d9d9d9;\">
                                        <td>08.00</td>
                                        <td>09.30</td>
                                        <td>Wisata petik apel</td>
                                    </tr>
                                    <tr >
                                        <td>10.00</td>
                                        <td>13.00</td>
                                        <td>Wisata Eco Greenpark</td>
                                    </tr>
                                    <tr style=\"background: #d9d9d9;\">
                                        <td>13.00</td>
                                        <td>14.00</td>
                                        <td>Menuju Coban Rondo + makan siang</td>
                                    </tr>
                                    <tr >
                                        <td>14.00</td>
                                        <td>15.30</td>
                                        <td>Wisata Coban Rondo</td>
                                    </tr>
                                    <tr style=\"background: #d9d9d9;\">
                                        <td>15.30</td>
                                        <td>16.30</td>
                                        <td>Perjalanan menuju Malang –tempat oleh – oleh</td>
                                    </tr>
                                    <tr>
                                        <td>16.30</td>
                                        <td>17.00</td>
                                        <td>(Drop out Stasiun / Terminal / Hotel)</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"timeline__item\">
                    <div class=\"timeline__item__icon-wrap\">
                        <div class=\"timeline__item__icon\">
                            <div class=\"timeline__item__icon__text\">S</div>
                        </div>
                    </div>
                    <div class=\"timeline__item__content padding-left\">
                        <div class=\"timeline__item__title\">Selesai, Sampai Jumpa di Trip Selanjutnya</div>
                    </div>
                </div>
                <ul class=\"detail\">
                    <li><em>Jadwal  sudah dimodifikasi sesuai kebutuhan anda, tetapi masih bisa menyesuaikan dengan kebutuhan anda.</em></li>
                    <li><em>Perlengkapan Yang Harus Dibawa : Jaket Hangat, Obat Pribadi.</em></li>
                    <li><em>No Travel Bag/Koper – Tas Ransel Only 1 tas /orang.</em></li>
                </ul>

                <hr>
                <h1 class=\"promo_title\">DAPATKAN HARGA PROMO SETIAP HARINYA!</h1>
                <div class=\"container_12\">
                <div class=\"grid_3\">
                    <div class=\"p_table_1\">
                        <div class=\"column_ribbon ribbon_style1_hot\"></div>
                        <ul>
                            <li class=\"header\"><span>2 - 6 pax</span></li>
                            <li class=\"content\">
                                <span class=\"suffix\">Rp.</span> <span>850 K</span>
                                <p> pax / (Makin banyak peserta makin murah!!)</p>
                            </li>

                            <li style=\"height: 55px;\" class=\"css3_grid_row_2 footer_row css3_grid_row_2_responsive\"><span class=\"css3_grid_vertical_align_table\"><span class=\"css3_grid_vertical_align\">Harga berlaku untuk 6 peserta. Lebih Banyak Lebih MURAH! Hubungi Hotline Kami di nomor +6281336661922</span></span>
                            </li>
                        </ul>

                    </div></div>
                <div class=\"grid_3\">
                    <div class=\"p_table_1\">
                        <ul>
                            <li class=\"header\"><span> 7 - 14 pax</span></li>
                            <li class=\"content\">
                                <span class=\"suffix\">Rp.</span> <span>799 K</span>
                                <p> pax / (Makin banyak peserta makin murah!!)</p>
                            </li>

                            <li style=\"height: 55px;\" class=\"css3_grid_row_2 footer_row css3_grid_row_2_responsive\"><span class=\"css3_grid_vertical_align_table\"><span class=\"css3_grid_vertical_align\">Harga berlaku untuk 14 peserta. Lebih Banyak Lebih MURAH! Hubungi Hotline Kami di nomor +6281336661922</span></span>
                            </li>
                        </ul>

                    </div></div>
                <div class=\"grid_3\">
                    <div class=\"p_table_1\">
                        <ul>
                            <li class=\"header\"><span>12 pax +</span></li>
                            <li class=\"content\">
                                <span class=\"suffix\">-.</span> <span>CALL</span>
                                <p> pax / (Makin banyak peserta makin murah!!)</p>
                            </li>

                            <li style=\"height: 55px;\" class=\"css3_grid_row_2 footer_row css3_grid_row_2_responsive\"><span class=\"css3_grid_vertical_align_table\"><span class=\"css3_grid_vertical_align\">Silakan hubungi hotline kami di +6281336661922 untuk informasi lebih lanjut</span></span>
                            </li>
                        </ul>

                    </div></div>
                </div>
            </div>
        </div>
    </div>
    <script>
        \$(function (){
            \$('#bookingForm').bookingForm({
                ownerEmail: '#'
            });
        })
        \$(function() {
            \$('#bookingForm input, #bookingForm textarea').placeholder();
        });
    </script>

";
    }

    // line 261
    public function block_price_tag($context, array $blocks = array())
    {
        // line 262
        echo "    <div class=\"price-decoration block-after-indent\">
        <div class=\"price-decoration__value\">
            <div class=\"tag-icon\"></div>
            <del>
                <span class=\"amount\">Rp&nbsp;900.000</span>
            </del>
            <ins><span class=\"amount\">Rp&nbsp;850.000</span></ins>
        </div>
        <div class=\"price-decoration__label\">One tour per person</div>
        <div class=\"\">
            <div class=\"price-decoration__label-round\" style=\"background-color:#ff0000\"><span>BEST SELLER!</span></div></div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "tours/malang-batu-2d1n.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  300 => 262,  297 => 261,  41 => 7,  38 => 6,  33 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends 'layout/paket-detail.twig' %}*/
/* {% block title %}*/
/*     2D1N - Paket Wisata Malang Batu City Tour | JAWATIMURTOUR*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="container_12">*/
/*         <div class="grid_9">*/
/*             <h3>2D1N - Paket Wisata Malang Batu City Tour</h3>*/
/*             <div class="blog">*/
/*                 <time datetime="">2D<span>1N</span></time>*/
/* */
/*                 <div class="extra_wrapper">*/
/*                     <div class="text1 col1">Bebagai objek wisata berada di Kota ini seperti : Museum Satwa, Batu Secreet Zoo, Eco Greenpark, Museum Angkut, Batu Night Spectacular, Alun - Alun kota Batu, dan masih banyak yang lainnya.</div>*/
/*                 </div>*/
/* */
/*                 <div class="tours-tabs__info">*/
/*                     <div class="tours-tabs__info__item">*/
/*                         <div class="tours-tabs__info__item__content">*/
/*                             <div class="tours-tabs__info__item__icon"><div class="td-clock"></div></div>*/
/*                             <div class="tours-tabs__info__item__title">2 hari</div>*/
/*                             <div class="tours-tabs__info__item__description">Durasi</div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="tours-tabs__info__item">*/
/*                         <div class="tours-tabs__info__item__content">*/
/*                             <div class="tours-tabs__info__item__icon"><i class="td-user"></i></div>*/
/*                             <div class="tours-tabs__info__item__title">3 – 60 Tahun</div>*/
/*                             <div class="tours-tabs__info__item__description">Usia</div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="tours-tabs__info__item">*/
/*                         <div class="tours-tabs__info__item__content">*/
/*                             <div class="tours-tabs__info__item__icon"><i class="td-kategori"></i></div>*/
/*                             <div class="tours-tabs__info__item__title">Education, City Tour, Culinary, Nature, Shopping, Culture</div>*/
/*                             <div class="tours-tabs__info__item__description">Tipe Wisata Dalam Paket Ini</div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="clear"></div>*/
/*                 <div class="timeline__item">*/
/*                     <div class="timeline__item__icon-wrap">*/
/*                         <div class="timeline__item__icon">*/
/*                             <div class="timeline__item__icon__text">1</div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="timeline__item__content padding-left">*/
/*                         <div class="timeline__item__title">Day 1</div>*/
/*                         <div class="timeline__item__description">*/
/*                             <div style="margin-left: 5pt;">*/
/*                                 <table class="itenerary" width="100%">*/
/*                                     <colgroup>*/
/*                                         <col width="17%">*/
/*                                         <col width="17%">*/
/*                                         <col >*/
/*                                     </colgroup>*/
/*                                     <thead>*/
/*                                     <th>Mulai</th>*/
/*                                     <th>Akhir</th>*/
/*                                     <th>Trip Itenerary</th>*/
/*                                     </thead>*/
/*                                     <tbody valign="top">*/
/*                                     <tr style="background: #d9d9d9;">*/
/*                                         <td>06.00</td>*/
/*                                         <td>06.30</td>*/
/*                                         <td>Peserta dijemput di lokasi meeting poin yang ditentukan peserta</td>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>07.30</td>*/
/*                                         <td>10.00</td>*/
/*                                         <td>Malang City Tour (alun-alun malang dan balai kota malang) + wisata kuliner</td>*/
/*                                     </tr>*/
/*                                     <tr style="background: #d9d9d9;">*/
/*                                         <td>10.00</td>*/
/*                                         <td>11.00</td>*/
/*                                         <td>Menuju ke Kota Batu</td>*/
/*                                     </tr>*/
/*                                     <tr >*/
/*                                         <td>11.00</td>*/
/*                                         <td>14.00</td>*/
/*                                         <td>Wisata JatimPark 2</td>*/
/*                                     </tr>*/
/*                                     <tr style="background: #d9d9d9;">*/
/*                                         <td>14.00</td>*/
/*                                         <td>15.00</td>*/
/*                                         <td>Makan siang</td>*/
/*                                     </tr>*/
/*                                     <tr >*/
/*                                         <td>15.00</td>*/
/*                                         <td>18.00</td>*/
/*                                         <td>Wisata Musium Angkut</td>*/
/*                                     </tr>*/
/*                                     <tr style="background: #d9d9d9;">*/
/*                                         <td>18.00</td>*/
/*                                         <td>19.30</td>*/
/*                                         <td>Wisata Alun-Alun Kota Batu + Wisata Kuliner</td>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>19.30</td>*/
/*                                         <td>21.00</td>*/
/*                                         <td>Wisata BNS (Batu Night Spectaculer)</td>*/
/*                                     </tr>*/
/*                                     <tr style="background: #d9d9d9;">*/
/*                                         <td>21.00</td>*/
/*                                         <td> - </td>*/
/*                                         <td>Menuju ke Penginapan</td>*/
/*                                     </tr>*/
/*                                     </tbody>*/
/*                                 </table>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="timeline__item">*/
/*                     <div class="timeline__item__icon-wrap">*/
/*                         <div class="timeline__item__icon">*/
/*                             <div class="timeline__item__icon__text">2</div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="timeline__item__content padding-left">*/
/*                         <div class="timeline__item__title">Day 2</div>*/
/*                         <div class="timeline__item__description">*/
/*                             <div style="margin-left: 5pt;">*/
/*                                 <table class="itenerary" width="100%">*/
/*                                     <colgroup>*/
/*                                         <col width="17%">*/
/*                                         <col width="17%">*/
/*                                         <col >*/
/*                                     </colgroup>*/
/*                                     <thead>*/
/*                                     <th>Mulai</th>*/
/*                                     <th>Akhir</th>*/
/*                                     <th>Trip Itenerary</th>*/
/*                                     </thead>*/
/*                                     <tbody valign="top">*/
/*                                     <tr style="background: #d9d9d9;">*/
/*                                         <td>07.00</td>*/
/*                                         <td>07.30</td>*/
/*                                         <td>Breakfast dan penjemputan</td>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>07.30</td>*/
/*                                         <td>08.00</td>*/
/*                                         <td>Perjalanan menuju wisata petik apel</td>*/
/*                                     </tr>*/
/*                                     <tr style="background: #d9d9d9;">*/
/*                                         <td>08.00</td>*/
/*                                         <td>09.30</td>*/
/*                                         <td>Wisata petik apel</td>*/
/*                                     </tr>*/
/*                                     <tr >*/
/*                                         <td>10.00</td>*/
/*                                         <td>13.00</td>*/
/*                                         <td>Wisata Eco Greenpark</td>*/
/*                                     </tr>*/
/*                                     <tr style="background: #d9d9d9;">*/
/*                                         <td>13.00</td>*/
/*                                         <td>14.00</td>*/
/*                                         <td>Menuju Coban Rondo + makan siang</td>*/
/*                                     </tr>*/
/*                                     <tr >*/
/*                                         <td>14.00</td>*/
/*                                         <td>15.30</td>*/
/*                                         <td>Wisata Coban Rondo</td>*/
/*                                     </tr>*/
/*                                     <tr style="background: #d9d9d9;">*/
/*                                         <td>15.30</td>*/
/*                                         <td>16.30</td>*/
/*                                         <td>Perjalanan menuju Malang –tempat oleh – oleh</td>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>16.30</td>*/
/*                                         <td>17.00</td>*/
/*                                         <td>(Drop out Stasiun / Terminal / Hotel)</td>*/
/*                                     </tr>*/
/*                                     </tbody>*/
/*                                 </table>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="timeline__item">*/
/*                     <div class="timeline__item__icon-wrap">*/
/*                         <div class="timeline__item__icon">*/
/*                             <div class="timeline__item__icon__text">S</div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="timeline__item__content padding-left">*/
/*                         <div class="timeline__item__title">Selesai, Sampai Jumpa di Trip Selanjutnya</div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <ul class="detail">*/
/*                     <li><em>Jadwal  sudah dimodifikasi sesuai kebutuhan anda, tetapi masih bisa menyesuaikan dengan kebutuhan anda.</em></li>*/
/*                     <li><em>Perlengkapan Yang Harus Dibawa : Jaket Hangat, Obat Pribadi.</em></li>*/
/*                     <li><em>No Travel Bag/Koper – Tas Ransel Only 1 tas /orang.</em></li>*/
/*                 </ul>*/
/* */
/*                 <hr>*/
/*                 <h1 class="promo_title">DAPATKAN HARGA PROMO SETIAP HARINYA!</h1>*/
/*                 <div class="container_12">*/
/*                 <div class="grid_3">*/
/*                     <div class="p_table_1">*/
/*                         <div class="column_ribbon ribbon_style1_hot"></div>*/
/*                         <ul>*/
/*                             <li class="header"><span>2 - 6 pax</span></li>*/
/*                             <li class="content">*/
/*                                 <span class="suffix">Rp.</span> <span>850 K</span>*/
/*                                 <p> pax / (Makin banyak peserta makin murah!!)</p>*/
/*                             </li>*/
/* */
/*                             <li style="height: 55px;" class="css3_grid_row_2 footer_row css3_grid_row_2_responsive"><span class="css3_grid_vertical_align_table"><span class="css3_grid_vertical_align">Harga berlaku untuk 6 peserta. Lebih Banyak Lebih MURAH! Hubungi Hotline Kami di nomor +6281336661922</span></span>*/
/*                             </li>*/
/*                         </ul>*/
/* */
/*                     </div></div>*/
/*                 <div class="grid_3">*/
/*                     <div class="p_table_1">*/
/*                         <ul>*/
/*                             <li class="header"><span> 7 - 14 pax</span></li>*/
/*                             <li class="content">*/
/*                                 <span class="suffix">Rp.</span> <span>799 K</span>*/
/*                                 <p> pax / (Makin banyak peserta makin murah!!)</p>*/
/*                             </li>*/
/* */
/*                             <li style="height: 55px;" class="css3_grid_row_2 footer_row css3_grid_row_2_responsive"><span class="css3_grid_vertical_align_table"><span class="css3_grid_vertical_align">Harga berlaku untuk 14 peserta. Lebih Banyak Lebih MURAH! Hubungi Hotline Kami di nomor +6281336661922</span></span>*/
/*                             </li>*/
/*                         </ul>*/
/* */
/*                     </div></div>*/
/*                 <div class="grid_3">*/
/*                     <div class="p_table_1">*/
/*                         <ul>*/
/*                             <li class="header"><span>12 pax +</span></li>*/
/*                             <li class="content">*/
/*                                 <span class="suffix">-.</span> <span>CALL</span>*/
/*                                 <p> pax / (Makin banyak peserta makin murah!!)</p>*/
/*                             </li>*/
/* */
/*                             <li style="height: 55px;" class="css3_grid_row_2 footer_row css3_grid_row_2_responsive"><span class="css3_grid_vertical_align_table"><span class="css3_grid_vertical_align">Silakan hubungi hotline kami di +6281336661922 untuk informasi lebih lanjut</span></span>*/
/*                             </li>*/
/*                         </ul>*/
/* */
/*                     </div></div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     <script>*/
/*         $(function (){*/
/*             $('#bookingForm').bookingForm({*/
/*                 ownerEmail: '#'*/
/*             });*/
/*         })*/
/*         $(function() {*/
/*             $('#bookingForm input, #bookingForm textarea').placeholder();*/
/*         });*/
/*     </script>*/
/* */
/* {% endblock %}*/
/* */
/* {% block price_tag %}*/
/*     <div class="price-decoration block-after-indent">*/
/*         <div class="price-decoration__value">*/
/*             <div class="tag-icon"></div>*/
/*             <del>*/
/*                 <span class="amount">Rp&nbsp;900.000</span>*/
/*             </del>*/
/*             <ins><span class="amount">Rp&nbsp;850.000</span></ins>*/
/*         </div>*/
/*         <div class="price-decoration__label">One tour per person</div>*/
/*         <div class="">*/
/*             <div class="price-decoration__label-round" style="background-color:#ff0000"><span>BEST SELLER!</span></div></div>*/
/*     </div>*/
/* {% endblock %}*/
