<?php

/* tours/menjangan-fullday.twig */
class __TwigTemplate_37a4ea5eefa454eaf77879502110f41ab83cd100ba03193ca165dea719c25696 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout/paket-detail.twig", "tours/menjangan-fullday.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'price_tag' => array($this, 'block_price_tag'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/paket-detail.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "    Fullday - Paket Wisata Snorkeling Menjangan | JAWATIMURTOUR
";
    }

    // line 6
    public function block_content($context, array $blocks = array())
    {
        // line 7
        echo "    <div class=\"container_12\">
        <div class=\"grid_9\">
            <h3>Fullday - Paket Wisata Snorkeling Menjangan</h3>
            <div class=\"blog\">
                <time datetime=\"\">Full<span>day</span></time>

                <div class=\"extra_wrapper\">
                    <div class=\"text1 col1\">Dan tidak dipungkiri Pulau Menjangan mempunyai salah satu spot snorkeling terbaik di Jawa dan Bali.</div>
                </div>

                <div class=\"tours-tabs__info\">
                    <div class=\"tours-tabs__info__item\">
                        <div class=\"tours-tabs__info__item__content\">
                            <div class=\"tours-tabs__info__item__icon\"><div class=\"td-clock\"></div></div>
                            <div class=\"tours-tabs__info__item__title\">1 hari</div>
                            <div class=\"tours-tabs__info__item__description\">Durasi</div>
                        </div>
                    </div>
                    <div class=\"tours-tabs__info__item\">
                        <div class=\"tours-tabs__info__item__content\">
                            <div class=\"tours-tabs__info__item__icon\"><i class=\"td-user\"></i></div>
                            <div class=\"tours-tabs__info__item__title\">5 – 60 Tahun</div>
                            <div class=\"tours-tabs__info__item__description\">Usia</div>
                        </div>
                    </div>
                    <div class=\"tours-tabs__info__item\">
                        <div class=\"tours-tabs__info__item__content\">
                            <div class=\"tours-tabs__info__item__icon\"><i class=\"td-kategori\"></i></div>
                            <div class=\"tours-tabs__info__item__title\">Lite Adventure, Culture, Nature, Beach, Culture</div>
                            <div class=\"tours-tabs__info__item__description\">Tipe Wisata Dalam Paket Ini</div>
                        </div>
                    </div>
                </div>
                <div class=\"clear\"></div>
                <div class=\"timeline__item\">
                    <div class=\"timeline__item__icon-wrap\">
                        <div class=\"timeline__item__icon\">
                            <div class=\"timeline__item__icon__text\">1</div>
                        </div>
                    </div>
                    <div class=\"timeline__item__content padding-left\">
                        <div class=\"timeline__item__title\">Day 1</div>
                        <div class=\"timeline__item__description\">
                            <div style=\"margin-left: 5pt;\">
                                <table class=\"itenerary\" width=\"100%\">
                                    <colgroup>
                                        <col width=\"17%\">
                                        <col width=\"17%\">
                                        <col >
                                    </colgroup>
                                    <thead>
                                        <th>Mulai</th>
                                        <th>Akhir</th>
                                        <th>Trip Itenerary</th>
                                    </thead>
                                    <tbody valign=\"top\">
                                    <tr style=\"background: #d9d9d9;\">
                                        <td>00.30</td>
                                        <td>01.00</td>
                                        <td>Peserta dijemput di lokasi meeting poin yang ditentukan peserta</td>
                                    </tr>
                                    <tr>
                                        <td>01.30</td>
                                        <td>08.00</td>
                                        <td>Perjalanan ke Pantai Bangsring</td>
                                    </tr>
                                    <tr style=\"background: #d9d9d9;\">
                                        <td>08:00</td>
                                        <td>09.00</td>
                                        <td>Persiapan explore Pulau Menjangan</td>
                                    </tr>
                                    <tr >
                                        <td>09.00</td>
                                        <td>10:00</td>
                                        <td>Perjalanan menuju Pulau Menjangan menggunakan Perahu</td>
                                    </tr>
                                    <tr style=\"background: #d9d9d9;\">
                                        <td>10:00</td>
                                        <td>14.00</td>
                                        <td>Explore Pulau Menjangan dan Tabuhan(Optional) dan Snorkeling dan Makan siang </td>
                                    </tr>
                                    <tr >
                                        <td>14.30</td>
                                        <td>15.30</td>
                                        <td>Kembali ke Pantai Bangsring</td>
                                    </tr>
                                    <tr style=\"background: #d9d9d9;\">
                                        <td>15.30</td>
                                        <td>16.00</td>
                                        <td>Berenang bersama hiu di Rumah Apung Bangsring</td>
                                    </tr>
                                    <tr>
                                        <td>16.00</td>
                                        <td>17.00</td>
                                        <td>Bersih diri dan persiapan pulang</td>
                                    </tr>
                                    <tr style=\"background: #d9d9d9;\">
                                        <td>17.00</td>
                                        <td>23.30</td>
                                        <td>Perjalanan menuju Malang (Drop out Stasiun/Hotel)</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"timeline__item\">
                    <div class=\"timeline__item__icon-wrap\">
                        <div class=\"timeline__item__icon\">
                            <div class=\"timeline__item__icon__text\">S</div>
                        </div>
                    </div>
                    <div class=\"timeline__item__content padding-left\">
                        <div class=\"timeline__item__title\">Selesai, Sampai Jumpa di Trip Selanjutnya</div>
                    </div>
                </div>
                <ul class=\"detail\">
                    <li><em>Jadwal  sudah dimodifikasi sesuai kebutuhan anda, tetapi masih bisa menyesuaikan dengan kebutuhan anda.</em></li>
                    <li><em>Perlengkapan Yang Harus Dibawa : Alat mandi .</em></li>
                    <li><em>No Travel Bag/Koper – Tas Ransel Only 1 tas /orang.</em></li>
                </ul>

                <hr>
                <h1 class=\"promo_title\">DAPATKAN HARGA PROMO SETIAP HARINYA!</h1>
                <div class=\"container_12\">
                <div class=\"grid_3\">
                    <div class=\"p_table_1\">
                        <div class=\"column_ribbon ribbon_style1_hot\"></div>
                        <ul>
                            <li class=\"header\"><span>2 - 5 pax</span></li>
                            <li class=\"content\">
                                <span class=\"suffix\">Rp.</span> <span>875 K</span>
                                <p> pax / (Makin banyak peserta makin murah!!)</p>
                            </li>

                            <li style=\"height: 55px;\" class=\"css3_grid_row_2 footer_row css3_grid_row_2_responsive\"><span class=\"css3_grid_vertical_align_table\"><span class=\"css3_grid_vertical_align\">Harga berlaku untuk 5 peserta. Lebih Banyak Lebih MURAH! Hubungi Hotline Kami di nomor +6281336661922</span></span>
                            </li>
                        </ul>

                    </div></div>
                <div class=\"grid_3\">
                    <div class=\"p_table_1\">
                        <ul>
                            <li class=\"header\"><span> 6 - 12 pax</span></li>
                            <li class=\"content\">
                                <span class=\"suffix\">Rp.</span> <span>450 K</span>
                                <p> pax / (Makin banyak peserta makin murah!!)</p>
                            </li>

                            <li style=\"height: 55px;\" class=\"css3_grid_row_2 footer_row css3_grid_row_2_responsive\"><span class=\"css3_grid_vertical_align_table\"><span class=\"css3_grid_vertical_align\">Harga berlaku untuk 12 peserta. Lebih Banyak Lebih MURAH! Hubungi Hotline Kami di nomor +6281336661922</span></span>
                            </li>
                        </ul>

                    </div></div>
                <div class=\"grid_3\">
                    <div class=\"p_table_1\">
                        <ul>
                            <li class=\"header\"><span>12 pax +</span></li>
                            <li class=\"content\">
                                <span class=\"suffix\">-.</span> <span>CALL</span>
                                <p> pax / (Makin banyak peserta makin murah!!)</p>
                            </li>

                            <li style=\"height: 55px;\" class=\"css3_grid_row_2 footer_row css3_grid_row_2_responsive\"><span class=\"css3_grid_vertical_align_table\"><span class=\"css3_grid_vertical_align\">Silakan hubungi hotline kami di +6281336661922 untuk informasi lebih lanjut</span></span>
                            </li>
                        </ul>

                    </div></div>
                </div>
            </div>
        </div>
    </div>
    <script>
        \$(function (){
            \$('#bookingForm').bookingForm({
                ownerEmail: '#'
            });
        })
        \$(function() {
            \$('#bookingForm input, #bookingForm textarea').placeholder();
        });
    </script>

";
    }

    // line 193
    public function block_price_tag($context, array $blocks = array())
    {
        // line 194
        echo "    <div class=\"price-decoration block-after-indent\">
        <div class=\"price-decoration__value\">
            <div class=\"tag-icon\"></div>
            <del>
                <span class=\"amount\">Rp&nbsp;499.999</span>
            </del>
            <ins><span class=\"amount\">Rp&nbsp;450.000</span></ins>
        </div>
        <div class=\"price-decoration__label\">One tour per person</div>
        <div class=\"\">
            <div class=\"price-decoration__label-round\" style=\"background-color:#ff0000\"><span>BEST SELLER!</span></div></div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "tours/menjangan-fullday.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  232 => 194,  229 => 193,  41 => 7,  38 => 6,  33 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends 'layout/paket-detail.twig' %}*/
/* {% block title %}*/
/*     Fullday - Paket Wisata Snorkeling Menjangan | JAWATIMURTOUR*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="container_12">*/
/*         <div class="grid_9">*/
/*             <h3>Fullday - Paket Wisata Snorkeling Menjangan</h3>*/
/*             <div class="blog">*/
/*                 <time datetime="">Full<span>day</span></time>*/
/* */
/*                 <div class="extra_wrapper">*/
/*                     <div class="text1 col1">Dan tidak dipungkiri Pulau Menjangan mempunyai salah satu spot snorkeling terbaik di Jawa dan Bali.</div>*/
/*                 </div>*/
/* */
/*                 <div class="tours-tabs__info">*/
/*                     <div class="tours-tabs__info__item">*/
/*                         <div class="tours-tabs__info__item__content">*/
/*                             <div class="tours-tabs__info__item__icon"><div class="td-clock"></div></div>*/
/*                             <div class="tours-tabs__info__item__title">1 hari</div>*/
/*                             <div class="tours-tabs__info__item__description">Durasi</div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="tours-tabs__info__item">*/
/*                         <div class="tours-tabs__info__item__content">*/
/*                             <div class="tours-tabs__info__item__icon"><i class="td-user"></i></div>*/
/*                             <div class="tours-tabs__info__item__title">5 – 60 Tahun</div>*/
/*                             <div class="tours-tabs__info__item__description">Usia</div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="tours-tabs__info__item">*/
/*                         <div class="tours-tabs__info__item__content">*/
/*                             <div class="tours-tabs__info__item__icon"><i class="td-kategori"></i></div>*/
/*                             <div class="tours-tabs__info__item__title">Lite Adventure, Culture, Nature, Beach, Culture</div>*/
/*                             <div class="tours-tabs__info__item__description">Tipe Wisata Dalam Paket Ini</div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="clear"></div>*/
/*                 <div class="timeline__item">*/
/*                     <div class="timeline__item__icon-wrap">*/
/*                         <div class="timeline__item__icon">*/
/*                             <div class="timeline__item__icon__text">1</div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="timeline__item__content padding-left">*/
/*                         <div class="timeline__item__title">Day 1</div>*/
/*                         <div class="timeline__item__description">*/
/*                             <div style="margin-left: 5pt;">*/
/*                                 <table class="itenerary" width="100%">*/
/*                                     <colgroup>*/
/*                                         <col width="17%">*/
/*                                         <col width="17%">*/
/*                                         <col >*/
/*                                     </colgroup>*/
/*                                     <thead>*/
/*                                         <th>Mulai</th>*/
/*                                         <th>Akhir</th>*/
/*                                         <th>Trip Itenerary</th>*/
/*                                     </thead>*/
/*                                     <tbody valign="top">*/
/*                                     <tr style="background: #d9d9d9;">*/
/*                                         <td>00.30</td>*/
/*                                         <td>01.00</td>*/
/*                                         <td>Peserta dijemput di lokasi meeting poin yang ditentukan peserta</td>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>01.30</td>*/
/*                                         <td>08.00</td>*/
/*                                         <td>Perjalanan ke Pantai Bangsring</td>*/
/*                                     </tr>*/
/*                                     <tr style="background: #d9d9d9;">*/
/*                                         <td>08:00</td>*/
/*                                         <td>09.00</td>*/
/*                                         <td>Persiapan explore Pulau Menjangan</td>*/
/*                                     </tr>*/
/*                                     <tr >*/
/*                                         <td>09.00</td>*/
/*                                         <td>10:00</td>*/
/*                                         <td>Perjalanan menuju Pulau Menjangan menggunakan Perahu</td>*/
/*                                     </tr>*/
/*                                     <tr style="background: #d9d9d9;">*/
/*                                         <td>10:00</td>*/
/*                                         <td>14.00</td>*/
/*                                         <td>Explore Pulau Menjangan dan Tabuhan(Optional) dan Snorkeling dan Makan siang </td>*/
/*                                     </tr>*/
/*                                     <tr >*/
/*                                         <td>14.30</td>*/
/*                                         <td>15.30</td>*/
/*                                         <td>Kembali ke Pantai Bangsring</td>*/
/*                                     </tr>*/
/*                                     <tr style="background: #d9d9d9;">*/
/*                                         <td>15.30</td>*/
/*                                         <td>16.00</td>*/
/*                                         <td>Berenang bersama hiu di Rumah Apung Bangsring</td>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td>16.00</td>*/
/*                                         <td>17.00</td>*/
/*                                         <td>Bersih diri dan persiapan pulang</td>*/
/*                                     </tr>*/
/*                                     <tr style="background: #d9d9d9;">*/
/*                                         <td>17.00</td>*/
/*                                         <td>23.30</td>*/
/*                                         <td>Perjalanan menuju Malang (Drop out Stasiun/Hotel)</td>*/
/*                                     </tr>*/
/*                                     </tbody>*/
/*                                 </table>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="timeline__item">*/
/*                     <div class="timeline__item__icon-wrap">*/
/*                         <div class="timeline__item__icon">*/
/*                             <div class="timeline__item__icon__text">S</div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="timeline__item__content padding-left">*/
/*                         <div class="timeline__item__title">Selesai, Sampai Jumpa di Trip Selanjutnya</div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <ul class="detail">*/
/*                     <li><em>Jadwal  sudah dimodifikasi sesuai kebutuhan anda, tetapi masih bisa menyesuaikan dengan kebutuhan anda.</em></li>*/
/*                     <li><em>Perlengkapan Yang Harus Dibawa : Alat mandi .</em></li>*/
/*                     <li><em>No Travel Bag/Koper – Tas Ransel Only 1 tas /orang.</em></li>*/
/*                 </ul>*/
/* */
/*                 <hr>*/
/*                 <h1 class="promo_title">DAPATKAN HARGA PROMO SETIAP HARINYA!</h1>*/
/*                 <div class="container_12">*/
/*                 <div class="grid_3">*/
/*                     <div class="p_table_1">*/
/*                         <div class="column_ribbon ribbon_style1_hot"></div>*/
/*                         <ul>*/
/*                             <li class="header"><span>2 - 5 pax</span></li>*/
/*                             <li class="content">*/
/*                                 <span class="suffix">Rp.</span> <span>875 K</span>*/
/*                                 <p> pax / (Makin banyak peserta makin murah!!)</p>*/
/*                             </li>*/
/* */
/*                             <li style="height: 55px;" class="css3_grid_row_2 footer_row css3_grid_row_2_responsive"><span class="css3_grid_vertical_align_table"><span class="css3_grid_vertical_align">Harga berlaku untuk 5 peserta. Lebih Banyak Lebih MURAH! Hubungi Hotline Kami di nomor +6281336661922</span></span>*/
/*                             </li>*/
/*                         </ul>*/
/* */
/*                     </div></div>*/
/*                 <div class="grid_3">*/
/*                     <div class="p_table_1">*/
/*                         <ul>*/
/*                             <li class="header"><span> 6 - 12 pax</span></li>*/
/*                             <li class="content">*/
/*                                 <span class="suffix">Rp.</span> <span>450 K</span>*/
/*                                 <p> pax / (Makin banyak peserta makin murah!!)</p>*/
/*                             </li>*/
/* */
/*                             <li style="height: 55px;" class="css3_grid_row_2 footer_row css3_grid_row_2_responsive"><span class="css3_grid_vertical_align_table"><span class="css3_grid_vertical_align">Harga berlaku untuk 12 peserta. Lebih Banyak Lebih MURAH! Hubungi Hotline Kami di nomor +6281336661922</span></span>*/
/*                             </li>*/
/*                         </ul>*/
/* */
/*                     </div></div>*/
/*                 <div class="grid_3">*/
/*                     <div class="p_table_1">*/
/*                         <ul>*/
/*                             <li class="header"><span>12 pax +</span></li>*/
/*                             <li class="content">*/
/*                                 <span class="suffix">-.</span> <span>CALL</span>*/
/*                                 <p> pax / (Makin banyak peserta makin murah!!)</p>*/
/*                             </li>*/
/* */
/*                             <li style="height: 55px;" class="css3_grid_row_2 footer_row css3_grid_row_2_responsive"><span class="css3_grid_vertical_align_table"><span class="css3_grid_vertical_align">Silakan hubungi hotline kami di +6281336661922 untuk informasi lebih lanjut</span></span>*/
/*                             </li>*/
/*                         </ul>*/
/* */
/*                     </div></div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     <script>*/
/*         $(function (){*/
/*             $('#bookingForm').bookingForm({*/
/*                 ownerEmail: '#'*/
/*             });*/
/*         })*/
/*         $(function() {*/
/*             $('#bookingForm input, #bookingForm textarea').placeholder();*/
/*         });*/
/*     </script>*/
/* */
/* {% endblock %}*/
/* */
/* {% block price_tag %}*/
/*     <div class="price-decoration block-after-indent">*/
/*         <div class="price-decoration__value">*/
/*             <div class="tag-icon"></div>*/
/*             <del>*/
/*                 <span class="amount">Rp&nbsp;499.999</span>*/
/*             </del>*/
/*             <ins><span class="amount">Rp&nbsp;450.000</span></ins>*/
/*         </div>*/
/*         <div class="price-decoration__label">One tour per person</div>*/
/*         <div class="">*/
/*             <div class="price-decoration__label-round" style="background-color:#ff0000"><span>BEST SELLER!</span></div></div>*/
/*     </div>*/
/* {% endblock %}*/
