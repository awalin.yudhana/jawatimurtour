<?php

/* home.twig */
class __TwigTemplate_f7bbde2a5491de482ae8fd5a22ca79888edac98fbf2e1ad8a8c708534c7ae13e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout/main.twig", "home.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'slider' => array($this, 'block_slider'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/main.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "    Homepage - JAWATIMURTOUR
";
    }

    // line 6
    public function block_slider($context, array $blocks = array())
    {
        // line 7
        echo "    <div class=\"slider_wrapper\">
        <div id=\"camera_wrap\" class=\"\">
            <div data-src=\"";
        // line 9
        echo twig_escape_filter($this->env, base_url("assets/images/slider-sempu.jpg"), "html", null, true);
        echo "\">
                <div class=\"caption fadeIn\">
                    <h2>Sempu Island</h2>
                    <div class=\"price\">
                        FROM
                        <span>199K</span>
                    </div>
                    <a href=\"";
        // line 16
        echo twig_escape_filter($this->env, site_url("frontend/tours/sempu-fullday"), "html", null, true);
        echo "\">LEARN MORE</a>
                </div>
            </div>
            <div data-src=\"";
        // line 19
        echo twig_escape_filter($this->env, base_url("assets/images/slider-bromo.jpg"), "html", null, true);
        echo "\">
                <div class=\"caption fadeIn\">
                    <h2>Midnight Bromo</h2>
                    <div class=\"price\">
                        FROM
                        <span>375K</span>
                    </div>
                    <a href=\"";
        // line 26
        echo twig_escape_filter($this->env, site_url("frontend/tours/bromo-midnight"), "html", null, true);
        echo "\">LEARN MORE</a>
                </div>
            </div>
            <div data-src=\"";
        // line 29
        echo twig_escape_filter($this->env, base_url("assets/images/slider-batu.jpg"), "html", null, true);
        echo "\">
                <div class=\"caption fadeIn\">
                    <h2>Malang Batu City Tour</h2>
                    <div class=\"price\">
                        FROM
                        <span>850K</span>
                    </div>
                    <a href=\"";
        // line 36
        echo twig_escape_filter($this->env, site_url("frontend/tours/malang-batu-city-tour-2d1n"), "html", null, true);
        echo "\">LEARN MORE</a>
                </div>
            </div>
        </div>
    </div>
";
    }

    // line 43
    public function block_content($context, array $blocks = array())
    {
        // line 44
        echo "    <div class=\"container_12\">
        <div class=\"grid_4\">
            <div class=\"banner\">
                <img src=\"";
        // line 47
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-sempu.jpg"), "html", null, true);
        echo "\" alt=\"\">
                <div class=\"label\">
                    <div class=\"title\">Fullday</div>
                    <div class=\"title\">Sempu Island</div>
                    <div class=\"price\">FROM<span>199K</span></div>
                    <a href=\"";
        // line 52
        echo twig_escape_filter($this->env, site_url("frontend/tours/sempu-fullday"), "html", null, true);
        echo "\">LEARN MORE</a>
                </div>
            </div>
        </div>
        <div class=\"grid_4\">
            <div class=\"banner\">
                <img src=\"";
        // line 58
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-bromo.jpg"), "html", null, true);
        echo "\" alt=\"\">
                <div class=\"label\">
                    <div class=\"title\">Midnight</div>
                    <div class=\"title\">Bromo</div>
                    <div class=\"price\">FROM<span>375K</span></div>
                    <a href=\"";
        // line 63
        echo twig_escape_filter($this->env, site_url("frontend/tours/bromo-midnight"), "html", null, true);
        echo "\">LEARN MORE</a>
                </div>
            </div>
        </div>
        <div class=\"grid_4\">
            <div class=\"banner\">
                <img src=\"";
        // line 69
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-batu.jpg"), "html", null, true);
        echo "\" alt=\"\">
                <div class=\"label\">
                    <div class=\"title\">2D1N</div>
                    <div class=\"title\">Batu City Tour</div>
                    <div class=\"price\">FROM<span>850K</span></div>
                    <a href=\"";
        // line 74
        echo twig_escape_filter($this->env, site_url("frontend/tours/malang-batu-city-tour-2d1n"), "html", null, true);
        echo "\"\">LEARN MORE</a>
                </div>
            </div>
        </div>
        <div class=\"clear\"></div>
        <div class=\"grid_6\">
            <h3>Booking Form</h3>
            ";
        // line 81
        $this->loadTemplate("layout/_paket_form.twig", "home.twig", 81)->display($context);
        // line 82
        echo "        </div>
        <div class=\"grid_5 prefix_1\">
            <h3>Welcome</h3>
            <img src=\"";
        // line 85
        echo twig_escape_filter($this->env, base_url("assets/images/logo-oval.png"), "html", null, true);
        echo "\" alt=\"\" class=\"img_inner fleft\" style=\"width:100px !important;\">
            <div class=\"extra_wrapper\">
                <p><b>JAWATIMURTOUR</b> kini hadir sebagai agent tour and travel terbaik untuk anda. Dengan motto</p>
                <strong><i>\"Make Your Trip Easier\"</i></strong>
            </div>
            <div class=\"clear cl1\"></div>
            <p>kami memeberikan pelayanan terbaik bagi para pelanggan. Kepuasan dan Kenyamanan perjalanan anda adalah fokus utama kami.</p>
            <p>Dengan didukung oleh para tour guide professional yang mempunyai kemampuan multibahasa dan armada yang aman dan nyaman. Kepuasan anda adalah jaminan layanan kami</p>
            <h4>Clients’ Quotes</h4>
            <blockquote class=\"bq1\">
                <img src=\"";
        // line 95
        echo twig_escape_filter($this->env, base_url("assets/images/testimonial-pristo.jpg"), "html", null, true);
        echo "\" alt=\"\" class=\"img_inner noresize fleft\">
                <div class=\"extra_wrapper\">
                    <p>Outbound yang dangat mengesankan bersama JAWATIMURTOUR. Itenerary dapat disesuaikan saat di lapangan, belum lagi dapat MUG yang bagus. Terima kasih JAWATIMURTOUR.com sudah menjadi partner perusahaan kami.</p>
                    <div class=\"alright\">
                        <div class=\"col1\">PT. PRISTO KUKUH ABADI</div>
                        <a href=\"#\" class=\"btn\">More</a>
                    </div>
                </div>
            </blockquote>
        </div>
        <div class=\"grid_12\">
            <h3 class=\"head1\">Layanan Lainnya</h3>
        </div>
        <div class=\"grid_4\">
            <div class=\"p_table_1\">
                <div class=\"column_ribbon ribbon_style1_hot\"></div>
                <ul>
                    <li class=\"header\"><span>Sewa Mobil Fullday</span></li>
                    <li class=\"content\">
                        <span class=\"suffix\">Rp.</span> <span>550 K</span>
                        <p>include bbm + parkir + makan driver</p>
                        <a class=\"button_more\" href=\"";
        // line 116
        echo twig_escape_filter($this->env, site_url("frontend/service"), "html", null, true);
        echo "\">LEARN MORE</a>
                    </li>

                    <li style=\"height: 55px;\" class=\"css3_grid_row_2 footer_row css3_grid_row_2_responsive\"><span class=\"css3_grid_vertical_align_table\"><span class=\"css3_grid_vertical_align\">Harga di atas berlaku hanya untuk hari biasa, untuk hari libur (tahun baru / lebaran) tidak berlaku!. Hubungi Hotline Kami di nomor +6281336661922 </span></span>
                    </li>
                </ul>

            </div></div>
        <div class=\"grid_4\">
            <div class=\"p_table_1\">
                <ul>
                    <li class=\"header\"><span> Pick Up / Drop Off Juanda</span></li>
                    <li class=\"content\">
                        <span class=\"suffix\">Rp.</span> <span>500 K</span>
                        <p>kapasitas 7 orang</p>
                        <a class=\"button_more\" href=\"";
        // line 131
        echo twig_escape_filter($this->env, site_url("frontend/service"), "html", null, true);
        echo "\">LEARN MORE</a>
                    </li>

                    <li style=\"height: 55px;\" class=\"css3_grid_row_2 footer_row css3_grid_row_2_responsive\"><span class=\"css3_grid_vertical_align_table\"><span class=\"css3_grid_vertical_align\"> Privasi anda prioritas kami, walaupun anda hanya 1 orang / 2 orang maka mobil tersebut tidak mengisi dengan penumpang lain. Hotline Kami +6281336661922</span></span>
                    </li>
                </ul>

            </div></div>
        <div class=\"grid_4\">
            <div class=\"p_table_1\">
                <ul>
                    <li class=\"header\"><span>Sewa Bus Wisata</span></li>
                    <li class=\"content\">
                        <span class=\"suffix\">-.</span> <span>CALL</span>
                        <p> kap 30 seat, ac, tv lcd, audio</p>
                        <a class=\"button_more\" href=\"";
        // line 146
        echo twig_escape_filter($this->env, site_url("frontend/service"), "html", null, true);
        echo "\">LEARN MORE</a>
                    </li>

                    <li style=\"height: 55px;\" class=\"css3_grid_row_2 footer_row css3_grid_row_2_responsive\"><span class=\"css3_grid_vertical_align_table\"><span class=\"css3_grid_vertical_align\">Kami menyediakan layanan sewa bus pariwisata untuk semua tujuan. Silakan hubungi hotline kami di +6281336661922 untuk informasi lebih lanjut</span></span>
                    </li>
                </ul>

            </div></div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  237 => 146,  219 => 131,  201 => 116,  177 => 95,  164 => 85,  159 => 82,  157 => 81,  147 => 74,  139 => 69,  130 => 63,  122 => 58,  113 => 52,  105 => 47,  100 => 44,  97 => 43,  87 => 36,  77 => 29,  71 => 26,  61 => 19,  55 => 16,  45 => 9,  41 => 7,  38 => 6,  33 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends 'layout/main.twig' %}*/
/* {% block title %}*/
/*     Homepage - JAWATIMURTOUR*/
/* {% endblock %}*/
/* */
/* {% block slider %}*/
/*     <div class="slider_wrapper">*/
/*         <div id="camera_wrap" class="">*/
/*             <div data-src="{{ base_url('assets/images/slider-sempu.jpg') }}">*/
/*                 <div class="caption fadeIn">*/
/*                     <h2>Sempu Island</h2>*/
/*                     <div class="price">*/
/*                         FROM*/
/*                         <span>199K</span>*/
/*                     </div>*/
/*                     <a href="{{ site_url('frontend/tours/sempu-fullday') }}">LEARN MORE</a>*/
/*                 </div>*/
/*             </div>*/
/*             <div data-src="{{ base_url('assets/images/slider-bromo.jpg') }}">*/
/*                 <div class="caption fadeIn">*/
/*                     <h2>Midnight Bromo</h2>*/
/*                     <div class="price">*/
/*                         FROM*/
/*                         <span>375K</span>*/
/*                     </div>*/
/*                     <a href="{{ site_url('frontend/tours/bromo-midnight') }}">LEARN MORE</a>*/
/*                 </div>*/
/*             </div>*/
/*             <div data-src="{{ base_url('assets/images/slider-batu.jpg') }}">*/
/*                 <div class="caption fadeIn">*/
/*                     <h2>Malang Batu City Tour</h2>*/
/*                     <div class="price">*/
/*                         FROM*/
/*                         <span>850K</span>*/
/*                     </div>*/
/*                     <a href="{{ site_url('frontend/tours/malang-batu-city-tour-2d1n') }}">LEARN MORE</a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="container_12">*/
/*         <div class="grid_4">*/
/*             <div class="banner">*/
/*                 <img src="{{ base_url('assets/images/thumbnail-sempu.jpg') }}" alt="">*/
/*                 <div class="label">*/
/*                     <div class="title">Fullday</div>*/
/*                     <div class="title">Sempu Island</div>*/
/*                     <div class="price">FROM<span>199K</span></div>*/
/*                     <a href="{{ site_url('frontend/tours/sempu-fullday') }}">LEARN MORE</a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <div class="grid_4">*/
/*             <div class="banner">*/
/*                 <img src="{{ base_url('assets/images/thumbnail-bromo.jpg') }}" alt="">*/
/*                 <div class="label">*/
/*                     <div class="title">Midnight</div>*/
/*                     <div class="title">Bromo</div>*/
/*                     <div class="price">FROM<span>375K</span></div>*/
/*                     <a href="{{ site_url('frontend/tours/bromo-midnight') }}">LEARN MORE</a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <div class="grid_4">*/
/*             <div class="banner">*/
/*                 <img src="{{ base_url('assets/images/thumbnail-batu.jpg') }}" alt="">*/
/*                 <div class="label">*/
/*                     <div class="title">2D1N</div>*/
/*                     <div class="title">Batu City Tour</div>*/
/*                     <div class="price">FROM<span>850K</span></div>*/
/*                     <a href="{{ site_url('frontend/tours/malang-batu-city-tour-2d1n') }}"">LEARN MORE</a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <div class="clear"></div>*/
/*         <div class="grid_6">*/
/*             <h3>Booking Form</h3>*/
/*             {% include 'layout/_paket_form.twig' %}*/
/*         </div>*/
/*         <div class="grid_5 prefix_1">*/
/*             <h3>Welcome</h3>*/
/*             <img src="{{ base_url('assets/images/logo-oval.png') }}" alt="" class="img_inner fleft" style="width:100px !important;">*/
/*             <div class="extra_wrapper">*/
/*                 <p><b>JAWATIMURTOUR</b> kini hadir sebagai agent tour and travel terbaik untuk anda. Dengan motto</p>*/
/*                 <strong><i>"Make Your Trip Easier"</i></strong>*/
/*             </div>*/
/*             <div class="clear cl1"></div>*/
/*             <p>kami memeberikan pelayanan terbaik bagi para pelanggan. Kepuasan dan Kenyamanan perjalanan anda adalah fokus utama kami.</p>*/
/*             <p>Dengan didukung oleh para tour guide professional yang mempunyai kemampuan multibahasa dan armada yang aman dan nyaman. Kepuasan anda adalah jaminan layanan kami</p>*/
/*             <h4>Clients’ Quotes</h4>*/
/*             <blockquote class="bq1">*/
/*                 <img src="{{ base_url('assets/images/testimonial-pristo.jpg') }}" alt="" class="img_inner noresize fleft">*/
/*                 <div class="extra_wrapper">*/
/*                     <p>Outbound yang dangat mengesankan bersama JAWATIMURTOUR. Itenerary dapat disesuaikan saat di lapangan, belum lagi dapat MUG yang bagus. Terima kasih JAWATIMURTOUR.com sudah menjadi partner perusahaan kami.</p>*/
/*                     <div class="alright">*/
/*                         <div class="col1">PT. PRISTO KUKUH ABADI</div>*/
/*                         <a href="#" class="btn">More</a>*/
/*                     </div>*/
/*                 </div>*/
/*             </blockquote>*/
/*         </div>*/
/*         <div class="grid_12">*/
/*             <h3 class="head1">Layanan Lainnya</h3>*/
/*         </div>*/
/*         <div class="grid_4">*/
/*             <div class="p_table_1">*/
/*                 <div class="column_ribbon ribbon_style1_hot"></div>*/
/*                 <ul>*/
/*                     <li class="header"><span>Sewa Mobil Fullday</span></li>*/
/*                     <li class="content">*/
/*                         <span class="suffix">Rp.</span> <span>550 K</span>*/
/*                         <p>include bbm + parkir + makan driver</p>*/
/*                         <a class="button_more" href="{{ site_url('frontend/service') }}">LEARN MORE</a>*/
/*                     </li>*/
/* */
/*                     <li style="height: 55px;" class="css3_grid_row_2 footer_row css3_grid_row_2_responsive"><span class="css3_grid_vertical_align_table"><span class="css3_grid_vertical_align">Harga di atas berlaku hanya untuk hari biasa, untuk hari libur (tahun baru / lebaran) tidak berlaku!. Hubungi Hotline Kami di nomor +6281336661922 </span></span>*/
/*                     </li>*/
/*                 </ul>*/
/* */
/*             </div></div>*/
/*         <div class="grid_4">*/
/*             <div class="p_table_1">*/
/*                 <ul>*/
/*                     <li class="header"><span> Pick Up / Drop Off Juanda</span></li>*/
/*                     <li class="content">*/
/*                         <span class="suffix">Rp.</span> <span>500 K</span>*/
/*                         <p>kapasitas 7 orang</p>*/
/*                         <a class="button_more" href="{{ site_url('frontend/service') }}">LEARN MORE</a>*/
/*                     </li>*/
/* */
/*                     <li style="height: 55px;" class="css3_grid_row_2 footer_row css3_grid_row_2_responsive"><span class="css3_grid_vertical_align_table"><span class="css3_grid_vertical_align"> Privasi anda prioritas kami, walaupun anda hanya 1 orang / 2 orang maka mobil tersebut tidak mengisi dengan penumpang lain. Hotline Kami +6281336661922</span></span>*/
/*                     </li>*/
/*                 </ul>*/
/* */
/*             </div></div>*/
/*         <div class="grid_4">*/
/*             <div class="p_table_1">*/
/*                 <ul>*/
/*                     <li class="header"><span>Sewa Bus Wisata</span></li>*/
/*                     <li class="content">*/
/*                         <span class="suffix">-.</span> <span>CALL</span>*/
/*                         <p> kap 30 seat, ac, tv lcd, audio</p>*/
/*                         <a class="button_more" href="{{ site_url('frontend/service') }}">LEARN MORE</a>*/
/*                     </li>*/
/* */
/*                     <li style="height: 55px;" class="css3_grid_row_2 footer_row css3_grid_row_2_responsive"><span class="css3_grid_vertical_align_table"><span class="css3_grid_vertical_align">Kami menyediakan layanan sewa bus pariwisata untuk semua tujuan. Silakan hubungi hotline kami di +6281336661922 untuk informasi lebih lanjut</span></span>*/
/*                     </li>*/
/*                 </ul>*/
/* */
/*             </div></div>*/
/*     </div>*/
/* {% endblock %}*/
