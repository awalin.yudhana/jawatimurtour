<?php

/* service.twig */
class __TwigTemplate_41d9dad9b6c9b73aebae77ecdc0c4ebf63548267039168687311b8108cad01e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout/post-detail.twig", "service.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/post-detail.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "    Sewa Mobil, Elf, Bus Pariwisata Malang | JAWATIMURTOUR
";
    }

    // line 6
    public function block_content($context, array $blocks = array())
    {
        // line 7
        echo "    <h3>Sewa Mobil All Include Malang</h3>
    <div class=\"blog\">
        <img src=\"";
        // line 9
        echo twig_escape_filter($this->env, base_url("assets/images/blog-avanza.jpg"), "html", null, true);
        echo "\" alt=\"sewa-mobil-malang-jawatimurtour\" class=\"img_inner\">
        <p>JAWATIMURTOUR melayani RENTAL MOBIL dan Elf MALANG atau SEWA MOBIL dan Elf DI MALANG MURAH Telp. +62813 3666 1922. Bagi anda yang menginginkan info rental mobil dan Elf / Carter Mobil dan Elf Malang tujuan di Kawasan Kota Malang Raya. Serta tujuan wisata ke Batu, Bromo, Kepanjen, Surabaya, Sidoarjo, Blitar. Info Rental Mobil dan Elf ke daerah Tulungagung, Kediri, Jombang, Pasuruan, Lumajang dan lain lain. Percayakan kepada kami, Jawatimurtour.</p>
        <p>Kami siap memberikan pelayanan rental mobil dan Elf malang + sopir dengan tujuan wisata batu, wisata bromo, wisata ijen. Selain itu juga menyediakan jasa sewa mobil wisuda di malang, acara kantor, survey, seminar, antar jemput tamu dan lain lain</p>
        <div class=\"extra_wrapper\">
            <div class=\"text1 col1\">Paket Sewa Mobil + Driver + BBM + Parkir. Area Malang dan sekitarnya.</div>
        </div>
        <ul>
            <li>Tujuan Malang – Batu 12jam <em>Rp.500.000</em></li>
            <li>Tujuan Malang – Batu Fullday <em>Rp.575.000</em></li>
            <li>Tujuan Malang - Pantai Selatan <em>Rp.575.000</em></li>
            <li>Tujuan Malang – Bandara Juanda <em>Rp.500.000</em></li>
            <li>Tujuan Malang – Surabaya Kota  <em>Rp.675.000</em></li>
            <li>Tujuan Malang – Bandara Abdul Rahman Saleh <em>Rp.225.000</em></li>
        </ul>
        <div class=\"extra_wrapper\">
            <div class=\"text1 col1\">Paket Sewa Mobil + Driver.</div>
        </div>
        <ul>
            <li>Wisata Malang – Batu 12jam <em>Rp.325.000</em></li>
            <li>Wisata Malang – Batu Fullday <em>Rp.375.000</em></li>
            <li>Tujuan Malang - Pantai Selatan  <em>Rp.40.000</em></li>
        </ul>
        <div class=\"extra_wrapper\">
            <div class=\"text1 col1\">Paket Sewa Elf (15 seat) + Driver Tujuan Wisata Malang - Batu - Pantai Selatan.</div>
        </div>
        <ul>
            <li>ELF Short + driver <em>Rp.750.000</em></li>
            <li>ELF Short + (All include) <em>Rp.1.100.000</em></li>
        </ul>
    </div>
    <h3>Jasa Pickup / Drop Off</h3>
    <div class=\"blog\">
        <img src=\"";
        // line 41
        echo twig_escape_filter($this->env, base_url("assets/images/blog-elf.jpg"), "html", null, true);
        echo "\" alt=\"sewa-elf-malang-jawatimurtour\" class=\"img_inner\">
        <p>JAWATIMURTOUR melayani sewa mobil dan elf malang untuk charter drop dari Malang ke bandara. Baik Bandara Juanda Surabaya atau Abdul Rahman Saleh, atau sebaliknya. Hubungi segera kontak, nomor telp, HP dan BB operator kami. Kami siap Online 24 jam dengan layanan terbaik untuk anda.</p>
    </div>

    <div class=\"extra_wrapper\">
        <div class=\"text1 col1\">Paket Pick up / Drop.</div>
    </div>
    <ul>
        <li>Drop Malang – Bandara Juanda <em>Rp.500.000</em></li>
        <li>Drop Bandara Malang – Batu <em>Rp.225.000</em></li>
        <li>Drop Stasiun Malang – Kota Malang <em>Rp.200.000</em></li>
        <li>Drop Malang – Bandara Malang <em>Rp.200.000</em></li>
    </ul>
    <p>Kemanapun tujuan anda percayakan kepada kami, Malang - Blitar, Malang - Surabaya, Malang - Pasuruan. Juga Malang - Jombang, Malang - Tulung agung, Malang - Kediri, Malang - Lamongan. Atau Malang - Probolinggo,  Malang - Bali, Malang - Jember, Malang - Madiun, Malang - Jogja.</p>
    <ul class=\"detail\">
        <li><em>Untuk tujuan lainnya ( luar kota ) silakan hubungi hotline kami di +62813 3666 1922.</em></li>
        <li><em>Kami juga melayani tujuan wisata religi seperti : ziarah wali (tulungagung, wali 5, jateng, madura, wali 8, wali 9, wali bali).</em></li>
    </ul>

    <h3>Sewa Bus Pariwisata Tujuan NUSANTARA</h3>
    <div class=\"blog\">
        <img src=\"";
        // line 62
        echo twig_escape_filter($this->env, base_url("assets/images/blog-bus.jpg"), "html", null, true);
        echo "\" alt=\"sewa-bus-malang-jawatimurtour\" class=\"img_inner\">
        <p>JAWATIMURTOUR melayani sewa Bus Pariwisata segala tujuan. Armada Bus kami memiliki fasilitas : free wifi. ac, kapasitas 30 seat, audio, lcd tv. Kami pastikan memberikan pelayanan terbaik untuk anda, dengan crew yang ramah dan profesional serta driver dengan SOD (Standart Operational Driver) menjadikan perjalanan anda aman dan nyaman.</p>
    </div>

    <div class=\"extra_wrapper\">
        <div class=\"text1 col1\">Tarif Bus Wisata Religi.</div>
    </div>
    <ul>
        <li><b>Ziarah Tulungagung</b> <em>Bedalem - Campurdarat - Sunan Kuning - Basarudin - Mangunsari - Pondok PETA - Pondok Panggung</em> Rp. 2.000.000</li>
        <li><b>Ziarah JATIM</b> <em>Gusdur - Mojoagung - Troloyo - Bungkul - Sunan Ampel - Sunan Giri - Sunan Gresik - Sunan Drajat - Asmorokondi - Sunan Bonang - + Syeh Kholil Bangkalan</em> Rp. 4.500.000</li>
        <li><b>Ziarah JATENG</b> <em>Sunan Muria - Sunan Kudus - Sunan Demak - Gunung Pring - Bayat</em> Rp. 5.500.000</li>
        <li><b>Ziarah JATIM + JATENG</b> Rp. 7.250.000</li>
        <li><b>Ziarah Madura</b> <em>Syeh Kholil Bangkalan - Air Mata Ibu - Batu Ampar - Asta Tinggi - Kraton Sumenep - Kali Anget</em> Rp. 6.500.000</li>
        <li><b>Ziarah Wali 9 <em>3 hari (Cirebon)</em></b> Rp. 7.500.000</li>
        <li><b>Ziarah Wali 9 <em>4 hari (Panjalu - Pamijahan)</em></b> Rp. 9.500.000</li>
        <li><b>Ziarah Wali 9 <em>5 hari (Banten)</em></b> Rp. 10.500.000</li>
        <li><b>Ziarah Wali 9 <em>6 hari (Lengkap)</em></b> Rp. 12.000.000</li>
        <li><b>Ziarah Wali Bali </b> Rp. 7.500.000</li>
    </ul>
    <p>Kami juga melayani tujuan seluruh nusantara. jangan sungkan - sungkan dan segera hubungi hotline kami di +62813 3666 1922</p>
    <ul class=\"detail\">
        <li><em>Untuk tujuan lainnya ( luar kota ) silakan hubungi hotline kami di +62813 3666 1922.</em></li>
        <li><em>Biaya di atas belum termasuk tiket tol dan penyeberangan (Bali).</em></li>
    </ul>
";
    }

    public function getTemplateName()
    {
        return "service.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 62,  79 => 41,  44 => 9,  40 => 7,  37 => 6,  32 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends 'layout/post-detail.twig' %}*/
/* {% block title %}*/
/*     Sewa Mobil, Elf, Bus Pariwisata Malang | JAWATIMURTOUR*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <h3>Sewa Mobil All Include Malang</h3>*/
/*     <div class="blog">*/
/*         <img src="{{ base_url('assets/images/blog-avanza.jpg') }}" alt="sewa-mobil-malang-jawatimurtour" class="img_inner">*/
/*         <p>JAWATIMURTOUR melayani RENTAL MOBIL dan Elf MALANG atau SEWA MOBIL dan Elf DI MALANG MURAH Telp. +62813 3666 1922. Bagi anda yang menginginkan info rental mobil dan Elf / Carter Mobil dan Elf Malang tujuan di Kawasan Kota Malang Raya. Serta tujuan wisata ke Batu, Bromo, Kepanjen, Surabaya, Sidoarjo, Blitar. Info Rental Mobil dan Elf ke daerah Tulungagung, Kediri, Jombang, Pasuruan, Lumajang dan lain lain. Percayakan kepada kami, Jawatimurtour.</p>*/
/*         <p>Kami siap memberikan pelayanan rental mobil dan Elf malang + sopir dengan tujuan wisata batu, wisata bromo, wisata ijen. Selain itu juga menyediakan jasa sewa mobil wisuda di malang, acara kantor, survey, seminar, antar jemput tamu dan lain lain</p>*/
/*         <div class="extra_wrapper">*/
/*             <div class="text1 col1">Paket Sewa Mobil + Driver + BBM + Parkir. Area Malang dan sekitarnya.</div>*/
/*         </div>*/
/*         <ul>*/
/*             <li>Tujuan Malang – Batu 12jam <em>Rp.500.000</em></li>*/
/*             <li>Tujuan Malang – Batu Fullday <em>Rp.575.000</em></li>*/
/*             <li>Tujuan Malang - Pantai Selatan <em>Rp.575.000</em></li>*/
/*             <li>Tujuan Malang – Bandara Juanda <em>Rp.500.000</em></li>*/
/*             <li>Tujuan Malang – Surabaya Kota  <em>Rp.675.000</em></li>*/
/*             <li>Tujuan Malang – Bandara Abdul Rahman Saleh <em>Rp.225.000</em></li>*/
/*         </ul>*/
/*         <div class="extra_wrapper">*/
/*             <div class="text1 col1">Paket Sewa Mobil + Driver.</div>*/
/*         </div>*/
/*         <ul>*/
/*             <li>Wisata Malang – Batu 12jam <em>Rp.325.000</em></li>*/
/*             <li>Wisata Malang – Batu Fullday <em>Rp.375.000</em></li>*/
/*             <li>Tujuan Malang - Pantai Selatan  <em>Rp.40.000</em></li>*/
/*         </ul>*/
/*         <div class="extra_wrapper">*/
/*             <div class="text1 col1">Paket Sewa Elf (15 seat) + Driver Tujuan Wisata Malang - Batu - Pantai Selatan.</div>*/
/*         </div>*/
/*         <ul>*/
/*             <li>ELF Short + driver <em>Rp.750.000</em></li>*/
/*             <li>ELF Short + (All include) <em>Rp.1.100.000</em></li>*/
/*         </ul>*/
/*     </div>*/
/*     <h3>Jasa Pickup / Drop Off</h3>*/
/*     <div class="blog">*/
/*         <img src="{{ base_url('assets/images/blog-elf.jpg') }}" alt="sewa-elf-malang-jawatimurtour" class="img_inner">*/
/*         <p>JAWATIMURTOUR melayani sewa mobil dan elf malang untuk charter drop dari Malang ke bandara. Baik Bandara Juanda Surabaya atau Abdul Rahman Saleh, atau sebaliknya. Hubungi segera kontak, nomor telp, HP dan BB operator kami. Kami siap Online 24 jam dengan layanan terbaik untuk anda.</p>*/
/*     </div>*/
/* */
/*     <div class="extra_wrapper">*/
/*         <div class="text1 col1">Paket Pick up / Drop.</div>*/
/*     </div>*/
/*     <ul>*/
/*         <li>Drop Malang – Bandara Juanda <em>Rp.500.000</em></li>*/
/*         <li>Drop Bandara Malang – Batu <em>Rp.225.000</em></li>*/
/*         <li>Drop Stasiun Malang – Kota Malang <em>Rp.200.000</em></li>*/
/*         <li>Drop Malang – Bandara Malang <em>Rp.200.000</em></li>*/
/*     </ul>*/
/*     <p>Kemanapun tujuan anda percayakan kepada kami, Malang - Blitar, Malang - Surabaya, Malang - Pasuruan. Juga Malang - Jombang, Malang - Tulung agung, Malang - Kediri, Malang - Lamongan. Atau Malang - Probolinggo,  Malang - Bali, Malang - Jember, Malang - Madiun, Malang - Jogja.</p>*/
/*     <ul class="detail">*/
/*         <li><em>Untuk tujuan lainnya ( luar kota ) silakan hubungi hotline kami di +62813 3666 1922.</em></li>*/
/*         <li><em>Kami juga melayani tujuan wisata religi seperti : ziarah wali (tulungagung, wali 5, jateng, madura, wali 8, wali 9, wali bali).</em></li>*/
/*     </ul>*/
/* */
/*     <h3>Sewa Bus Pariwisata Tujuan NUSANTARA</h3>*/
/*     <div class="blog">*/
/*         <img src="{{ base_url('assets/images/blog-bus.jpg') }}" alt="sewa-bus-malang-jawatimurtour" class="img_inner">*/
/*         <p>JAWATIMURTOUR melayani sewa Bus Pariwisata segala tujuan. Armada Bus kami memiliki fasilitas : free wifi. ac, kapasitas 30 seat, audio, lcd tv. Kami pastikan memberikan pelayanan terbaik untuk anda, dengan crew yang ramah dan profesional serta driver dengan SOD (Standart Operational Driver) menjadikan perjalanan anda aman dan nyaman.</p>*/
/*     </div>*/
/* */
/*     <div class="extra_wrapper">*/
/*         <div class="text1 col1">Tarif Bus Wisata Religi.</div>*/
/*     </div>*/
/*     <ul>*/
/*         <li><b>Ziarah Tulungagung</b> <em>Bedalem - Campurdarat - Sunan Kuning - Basarudin - Mangunsari - Pondok PETA - Pondok Panggung</em> Rp. 2.000.000</li>*/
/*         <li><b>Ziarah JATIM</b> <em>Gusdur - Mojoagung - Troloyo - Bungkul - Sunan Ampel - Sunan Giri - Sunan Gresik - Sunan Drajat - Asmorokondi - Sunan Bonang - + Syeh Kholil Bangkalan</em> Rp. 4.500.000</li>*/
/*         <li><b>Ziarah JATENG</b> <em>Sunan Muria - Sunan Kudus - Sunan Demak - Gunung Pring - Bayat</em> Rp. 5.500.000</li>*/
/*         <li><b>Ziarah JATIM + JATENG</b> Rp. 7.250.000</li>*/
/*         <li><b>Ziarah Madura</b> <em>Syeh Kholil Bangkalan - Air Mata Ibu - Batu Ampar - Asta Tinggi - Kraton Sumenep - Kali Anget</em> Rp. 6.500.000</li>*/
/*         <li><b>Ziarah Wali 9 <em>3 hari (Cirebon)</em></b> Rp. 7.500.000</li>*/
/*         <li><b>Ziarah Wali 9 <em>4 hari (Panjalu - Pamijahan)</em></b> Rp. 9.500.000</li>*/
/*         <li><b>Ziarah Wali 9 <em>5 hari (Banten)</em></b> Rp. 10.500.000</li>*/
/*         <li><b>Ziarah Wali 9 <em>6 hari (Lengkap)</em></b> Rp. 12.000.000</li>*/
/*         <li><b>Ziarah Wali Bali </b> Rp. 7.500.000</li>*/
/*     </ul>*/
/*     <p>Kami juga melayani tujuan seluruh nusantara. jangan sungkan - sungkan dan segera hubungi hotline kami di +62813 3666 1922</p>*/
/*     <ul class="detail">*/
/*         <li><em>Untuk tujuan lainnya ( luar kota ) silakan hubungi hotline kami di +62813 3666 1922.</em></li>*/
/*         <li><em>Biaya di atas belum termasuk tiket tol dan penyeberangan (Bali).</em></li>*/
/*     </ul>*/
/* {% endblock %}*/
