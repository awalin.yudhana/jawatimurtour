<?php

/* rent-car.twig */
class __TwigTemplate_dc9685727d640aecc00cff1252ddca4b1a1a57fbe4498c2fdc29357698379434 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout/main.twig", "rent-car.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/main.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "    Sewa Mobil di Malang | Yudhana Travel
";
    }

    // line 6
    public function block_content($context, array $blocks = array())
    {
        // line 7
        echo "    <div class=\"container_12\">
        <img src=\"";
        // line 8
        echo twig_escape_filter($this->env, base_url("assets/images/slider-rent-car.jpg"), "html", null, true);
        echo "\" alt=\"sewa-mobil-malang\" class=\"img_inner\">
        <div class=\"grid_12\">
            <h3>
                <a href=\"";
        // line 11
        echo twig_escape_filter($this->env, site_url("sewa-mobil-malang"), "html", null, true);
        echo "\" title=\"Sewa Mobil di Malang | Hub: 08133-666-1922\" rel=\"home\"><span>Sewa Mobil di Malang | Hub: 08133-666-1922</span></a>
            </h3>
            <hr>
            <h3 style=\"font-weight: 300; color: #373a41; text-align: center; padding-top :0px !important;\">
                <span style=\"color: #008000;\">Sewa Mobil Malang </span>untuk <span style=\"color: #008000;\">Anda</span> dan <span style=\"color: #008000;\">Keluarga Anda?</span>

            </h3>
            <h5 style=\"font-weight: 300; color: #373a41; text-align: center; padding-top :0px !important;\">
                <span>Rental Mobil di Malang Murah Carter Drop Rent Car Tour Bromo</span>
            </h5>
            <h3 style=\"text-align: center; padding-top :0px !important;\">
                <span style=\"color: #ff0000;\"> Bingung transportasi ketika di Batu atau Malang? Butuh Sewa Mobil di Malang?</span>
            </h3>

            <p>Sekarang Anda tidak perlu khawatir lagi, karena YUDHANA TRAVEL Perusahaan Jasa Sewa Mobil di Malang Batu telah hadir untuk membantu kebutuhan transportasi Anda dan keluarga. </p>

            <p>YUDHANA TRAVEL merupakan perusahaan Jasa Tour Travel yang melayani &amp; Rental <strong>Sewa Mobil Malang</strong>, Carter Drop segala jurusan, Wisata Instansi atau Sekolah. Kami fokus pada bidang pelayanan jasa transportasi terutama di Travel Malang Blitar Tulungagung PP dan <em>Sewa Rental Mobil</em> di Malang Batu. Selain menikmati perjalanan nyaman anda dengan armada kami yang terbaru dan bersih, YUDHANA TRAVEL&nbsp;juga memberikan Anda driver yang sopan, berpengalaman dan komunikatif yang akan sekaligus membantu anda untuk menemani berwisata di Kota Malang dan Batu.</p>
            <p>Tunggu apalagi? SEGERA Hubungi kami dan dapatkan penawaran serta pelayanan <strong>SEWA RENTAL MOBIL TERBAIK</strong> di Kota Malang, hanya di YUDHANA TRAVEL</p>
        </div>
    </div>

    <div class=\"container_12\">
        <div class=\"banners\" style=\"padding-top :0px !important; padding-bottom :15px !important;\">
            <div class=\"grid_4\">
                <div class=\"banner\" style=\"padding-top :0px !important;\">
                    <img src=\"";
        // line 36
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-avanza-new.jpg"), "html", null, true);
        echo "\" alt=\"\">
                    <div class=\"label\" style=\"color: #c73430 !important;\">
                        <div class=\"title\">Avanza</div>
                        <div class=\"title\">6 Tempat Duduk</div>
                        <div class=\"price\">/day<span>Rp 400.000</span></div>
                    </div>
                </div>
            </div>
            <div class=\"grid_4\">
                <div class=\"banner\" style=\"padding-top :0px !important;\">
                    <img src=\"";
        // line 46
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-inova-new.jpg"), "html", null, true);
        echo "\" alt=\"\">
                    <div class=\"label\" style=\"color: #c73430 !important;\">
                        <div class=\"title\">Inova</div>
                        <div class=\"title\">6 Tempat Duduk</div>
                        <div class=\"price\">/day<span>Rp 500.000</span></div>
                    </div>
                </div>
            </div>
            <div class=\"grid_4\">
                <div class=\"banner\" style=\"padding-top :0px !important;\">
                    <img src=\"";
        // line 56
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-elf-new.jpg"), "html", null, true);
        echo "\" alt=\"\">
                    <div class=\"label\" style=\"color: #c73430 !important;\">
                        <div class=\"title\">Isuzu Elf</div>
                        <div class=\"title\">14 Tempat Duduk</div>
                        <div class=\"price\">/day<span>Rp 800.000</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"container_12\">
        <div class=\"grid_12\">
            <div class=\"extra_wrapper\">
                <div class=\"text1 col1\">Ketentuan</div>
            </div>
            <ul class=\"detail\">
                <li>
                    Harga diatas berlaku untuk Sewa Mobil Malang <strong>Kota Malang dan Batu BELUM termasuk BBM</strong>
                </li>
                <li>Harga untuk di luar Malang ( dalam propinsi + Rp 50.000 ) Atau Hubungi Customer Service kami : 085855759990.</li>
                <li>Untuk Harga”Per Day All in” sewa mobil + sopir + BBM Luar kota hubungi customer Service kami.</li>
                <li>Tiket TOL, Parkir, dan Makan untuk sopir ditanggung oleh penyewa.</li>
                <li>Untuk Sewa Mobil Tujuan luar kota, penyewa memberikan penginapan untuk sopir (untuk luar kota lebih dari 1 hari).</li>
                <li>Pemesanan / Reservasi harus ada booking fee minimal 20%-50% terlebih dahulu untuk memastikan penjadwalan kami.</li>
                <li>Pembatalan order tidak ada pengembalian biaya booking fee.</li>
                <li>
                    Untuk perubahan/penambahan jadwal harus konfirmasi terlebih dahulu max 12 jam sebelumnya.</li>
                <li>Harga bisa berubah sewaktu-waktu tanpa pemberitahuan.</li>
            </ul>
        </div>
    </div>
    <div class=\"container_12\">
        ";
        // line 88
        $this->loadTemplate("layout/_relevan_post.twig", "rent-car.twig", 88)->display($context);
        // line 89
        echo "    </div>

";
    }

    public function getTemplateName()
    {
        return "rent-car.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  140 => 89,  138 => 88,  103 => 56,  90 => 46,  77 => 36,  49 => 11,  43 => 8,  40 => 7,  37 => 6,  32 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends 'layout/main.twig' %}*/
/* {% block title %}*/
/*     Sewa Mobil di Malang | Yudhana Travel*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="container_12">*/
/*         <img src="{{ base_url('assets/images/slider-rent-car.jpg') }}" alt="sewa-mobil-malang" class="img_inner">*/
/*         <div class="grid_12">*/
/*             <h3>*/
/*                 <a href="{{ site_url('sewa-mobil-malang') }}" title="Sewa Mobil di Malang | Hub: 08133-666-1922" rel="home"><span>Sewa Mobil di Malang | Hub: 08133-666-1922</span></a>*/
/*             </h3>*/
/*             <hr>*/
/*             <h3 style="font-weight: 300; color: #373a41; text-align: center; padding-top :0px !important;">*/
/*                 <span style="color: #008000;">Sewa Mobil Malang </span>untuk <span style="color: #008000;">Anda</span> dan <span style="color: #008000;">Keluarga Anda?</span>*/
/* */
/*             </h3>*/
/*             <h5 style="font-weight: 300; color: #373a41; text-align: center; padding-top :0px !important;">*/
/*                 <span>Rental Mobil di Malang Murah Carter Drop Rent Car Tour Bromo</span>*/
/*             </h5>*/
/*             <h3 style="text-align: center; padding-top :0px !important;">*/
/*                 <span style="color: #ff0000;"> Bingung transportasi ketika di Batu atau Malang? Butuh Sewa Mobil di Malang?</span>*/
/*             </h3>*/
/* */
/*             <p>Sekarang Anda tidak perlu khawatir lagi, karena YUDHANA TRAVEL Perusahaan Jasa Sewa Mobil di Malang Batu telah hadir untuk membantu kebutuhan transportasi Anda dan keluarga. </p>*/
/* */
/*             <p>YUDHANA TRAVEL merupakan perusahaan Jasa Tour Travel yang melayani &amp; Rental <strong>Sewa Mobil Malang</strong>, Carter Drop segala jurusan, Wisata Instansi atau Sekolah. Kami fokus pada bidang pelayanan jasa transportasi terutama di Travel Malang Blitar Tulungagung PP dan <em>Sewa Rental Mobil</em> di Malang Batu. Selain menikmati perjalanan nyaman anda dengan armada kami yang terbaru dan bersih, YUDHANA TRAVEL&nbsp;juga memberikan Anda driver yang sopan, berpengalaman dan komunikatif yang akan sekaligus membantu anda untuk menemani berwisata di Kota Malang dan Batu.</p>*/
/*             <p>Tunggu apalagi? SEGERA Hubungi kami dan dapatkan penawaran serta pelayanan <strong>SEWA RENTAL MOBIL TERBAIK</strong> di Kota Malang, hanya di YUDHANA TRAVEL</p>*/
/*         </div>*/
/*     </div>*/
/* */
/*     <div class="container_12">*/
/*         <div class="banners" style="padding-top :0px !important; padding-bottom :15px !important;">*/
/*             <div class="grid_4">*/
/*                 <div class="banner" style="padding-top :0px !important;">*/
/*                     <img src="{{ base_url('assets/images/thumbnail-avanza-new.jpg') }}" alt="">*/
/*                     <div class="label" style="color: #c73430 !important;">*/
/*                         <div class="title">Avanza</div>*/
/*                         <div class="title">6 Tempat Duduk</div>*/
/*                         <div class="price">/day<span>Rp 400.000</span></div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="grid_4">*/
/*                 <div class="banner" style="padding-top :0px !important;">*/
/*                     <img src="{{ base_url('assets/images/thumbnail-inova-new.jpg') }}" alt="">*/
/*                     <div class="label" style="color: #c73430 !important;">*/
/*                         <div class="title">Inova</div>*/
/*                         <div class="title">6 Tempat Duduk</div>*/
/*                         <div class="price">/day<span>Rp 500.000</span></div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="grid_4">*/
/*                 <div class="banner" style="padding-top :0px !important;">*/
/*                     <img src="{{ base_url('assets/images/thumbnail-elf-new.jpg') }}" alt="">*/
/*                     <div class="label" style="color: #c73430 !important;">*/
/*                         <div class="title">Isuzu Elf</div>*/
/*                         <div class="title">14 Tempat Duduk</div>*/
/*                         <div class="price">/day<span>Rp 800.000</span></div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     <div class="container_12">*/
/*         <div class="grid_12">*/
/*             <div class="extra_wrapper">*/
/*                 <div class="text1 col1">Ketentuan</div>*/
/*             </div>*/
/*             <ul class="detail">*/
/*                 <li>*/
/*                     Harga diatas berlaku untuk Sewa Mobil Malang <strong>Kota Malang dan Batu BELUM termasuk BBM</strong>*/
/*                 </li>*/
/*                 <li>Harga untuk di luar Malang ( dalam propinsi + Rp 50.000 ) Atau Hubungi Customer Service kami : 085855759990.</li>*/
/*                 <li>Untuk Harga”Per Day All in” sewa mobil + sopir + BBM Luar kota hubungi customer Service kami.</li>*/
/*                 <li>Tiket TOL, Parkir, dan Makan untuk sopir ditanggung oleh penyewa.</li>*/
/*                 <li>Untuk Sewa Mobil Tujuan luar kota, penyewa memberikan penginapan untuk sopir (untuk luar kota lebih dari 1 hari).</li>*/
/*                 <li>Pemesanan / Reservasi harus ada booking fee minimal 20%-50% terlebih dahulu untuk memastikan penjadwalan kami.</li>*/
/*                 <li>Pembatalan order tidak ada pengembalian biaya booking fee.</li>*/
/*                 <li>*/
/*                     Untuk perubahan/penambahan jadwal harus konfirmasi terlebih dahulu max 12 jam sebelumnya.</li>*/
/*                 <li>Harga bisa berubah sewaktu-waktu tanpa pemberitahuan.</li>*/
/*             </ul>*/
/*         </div>*/
/*     </div>*/
/*     <div class="container_12">*/
/*         {% include 'layout/_relevan_post.twig' %}*/
/*     </div>*/
/* */
/* {% endblock %}*/
/* */
/* */
