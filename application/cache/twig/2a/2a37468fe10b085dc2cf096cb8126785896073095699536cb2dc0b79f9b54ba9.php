<?php

/* terms-condition.twig */
class __TwigTemplate_3a5acc1abf64882e042538649497807c580c3ac974439d1e9bbfaedf4c65c560 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout/main.twig", "terms-condition.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/main.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "    Syarat dan Ketentuan | JAWATIMURTOUR
";
    }

    // line 6
    public function block_content($context, array $blocks = array())
    {
        // line 7
        echo "    <div class=\"container_12\">
        <div class=\"grid_12\">
            <h3>Ketentuan Umum</h3>
            <div>
                <div class=\"extra_wrapper\">
                    <div class=\"text1 col1\">Syarat dan Ketentuan.</div>
                </div>
                <ul class=\"detail\">
                    <li>Paket wisata masih bersifat penawaran dan bisa berubah sesuai permintaan <em>(fleksibel)</em>.</li>
                    <li>Informasi dan harga yang tercantum berlaku reguler untuk tahun 2016 <em>(tidak untuk masa high season)</em>.</li>
                    <li>Harga Berlaku Untuk WNI dan WNA Pemegang KIMS/KITAS.</li>
                    <li>Harga dapat berubah sewaktu-waktu tanpa adanya pemberitahuan sebelumnya.</li>
                    <li>Harga tidak mengikat selama belum adanya pembayaran Uang Muka.</li>
                    <li>Program dapat berubah sewaktu-waktu mengikuti keadaan lokal setempat.</li>
                    <li>Kategori anak mulai dari 2-6 tahun.</li>
                    <li>Tidak ada refund jika obyek wisata dibatalkan karena faktor force majeure, termasuk karena pengunjung yang overload, trafik jalan, cuaca dan waktu yang tidak memungkinkan.</li>
                    <li>Penjemputan / pengantaran di luar Kota Malang dikenakan biaya tambahan.</li>
                </ul>

                <div class=\"extra_wrapper\">
                    <div class=\"text1 col1\">Layanan Paket Wisata Sudah Termasuk.</div>
                </div>
                <ul class=\"detail\">
                    <li>Private Tour, Tidak Digabung Dengan Grup Lain.</li>
                    <li>Private Transportasi Full AC Eksekutif (termasuk BBM)
                        <ol>
                            <li>2-6 px by All New Avanza</li>
                            <li>7-18 pax by ELF / Long ELF</li>
                            <li>18-30 pax by Medium Bus</li>
                            <li>31-50 pax by Big Bus</li>
                        </ol>
                    </li>
                    <li>Antar – Jemput Grup di Termina / Stasiun / Hotel / Lokasi Lainnya di Kota Malang.</li>
                    <li>Biaya Penginapan <em>*sesuai kesepakatan</em>.</li>
                    <li>Tour Leader Professional & Ramah.</li>
                    <li>Pemandu Lokal di : Pulau Sempu / Kawah Ijen / Pulau Menjangan.</li>
                    <li>Penggunaan Kapal di : Pulau Sempu / Pulau Menjangan.</li>
                    <li>Peralatan Keselamatan Diri di Pulau Menjangan.</li>
                    <li>Peralatan Snorkeling di Pulau Menjangan.</li>
                    <li>Sewa Jeep di Bromo.</li>
                    <li>Penggunaan Masker di Kawah Ijen.</li>
                    <li>Air Mineral.</li>
                    <li>Welcome Snack.</li>
                    <li>Free Spanduk Tour <em>(khusus diatas 20 peserta)</em>.</li>
                    <li>Biaya Parkir.</li>
                    <li>Souvenir.</li>
                    <li>First Aid.</li>
                </ul>

                <div class=\"extra_wrapper\">
                    <div class=\"text1 col1\">Layanan Paket Wisata Belum Termasuk.</div>
                </div>
                <ul class=\"detail\">
                    <li>Tiket Kota Asal – Malang PP.</li>
                    <li>High Season Surcharge Saat Long Weekend / Libur Panjang Nasional.</li>
                    <li>Pengeluaran pribadi.</li>
                    <li>Asuransi perjalanan selama tour.</li>
                    <li>Tips untuk : Porter, Guide, Driver, Bellboy.</li>
                    <li>Biaya atau jasa lainnya yang tidak tercantum diatas.</li>
                </ul>
                <hr>
                <h1 class=\"promo_title\">Layanan Lainnya : Sewa Mobil, Pick Up - Drop Off, Bus Parisiwata</h1>
                <div class=\"container_12\">
                    <div class=\"grid_4\">
                        <div class=\"p_table_1\">
                            <div class=\"column_ribbon ribbon_style1_hot\"></div>
                            <ul>
                                <li class=\"header\"><span>Sewa Mobil Fullday</span></li>
                                <li class=\"content\">
                                    <span class=\"suffix\">Rp.</span> <span>550 K</span>
                                    <p>include bbm + parkir + makan driver</p>
                                    <a class=\"button_more\" href=\"";
        // line 78
        echo twig_escape_filter($this->env, site_url("frontend/service"), "html", null, true);
        echo "\">LEARN MORE</a>
                                </li>

                                <li style=\"height: 55px;\" class=\"css3_grid_row_2 footer_row css3_grid_row_2_responsive\"><span class=\"css3_grid_vertical_align_table\"><span class=\"css3_grid_vertical_align\">Harga di atas berlaku hanya untuk hari biasa, untuk hari libur (tahun baru / lebaran) tidak berlaku!. Hubungi Hotline Kami di nomor +6281336661922 </span></span>
                                </li>
                            </ul>

                        </div></div>
                    <div class=\"grid_4\">
                        <div class=\"p_table_1\">
                            <ul>
                                <li class=\"header\"><span> Pick Up / Drop Off Juanda</span></li>
                                <li class=\"content\">
                                    <span class=\"suffix\">Rp.</span> <span>500 K</span>
                                    <p>kapasitas 7 orang</p>
                                    <a class=\"button_more\" href=\"";
        // line 93
        echo twig_escape_filter($this->env, site_url("frontend/service"), "html", null, true);
        echo "\">LEARN MORE</a>
                                </li>

                                <li style=\"height: 55px;\" class=\"css3_grid_row_2 footer_row css3_grid_row_2_responsive\"><span class=\"css3_grid_vertical_align_table\"><span class=\"css3_grid_vertical_align\"> Privasi anda prioritas kami, walaupun anda hanya 1 orang / 2 orang maka mobil tersebut tidak mengisi dengan penumpang lain. Hotline Kami +6281336661922</span></span>
                                </li>
                            </ul>

                        </div></div>
                    <div class=\"grid_4\">
                        <div class=\"p_table_1\">
                            <ul>
                                <li class=\"header\"><span>Sewa Bus Wisata</span></li>
                                <li class=\"content\">
                                    <span class=\"suffix\">-.</span> <span>CALL</span>
                                    <p> kap 30 seat, ac, tv lcd, audio</p>
                                    <a class=\"button_more\" href=\"";
        // line 108
        echo twig_escape_filter($this->env, site_url("frontend/service"), "html", null, true);
        echo "\">LEARN MORE</a>
                                </li>

                                <li style=\"height: 55px;\" class=\"css3_grid_row_2 footer_row css3_grid_row_2_responsive\"><span class=\"css3_grid_vertical_align_table\"><span class=\"css3_grid_vertical_align\">Kami menyediakan layanan sewa bus pariwisata untuk semua tujuan. Silakan hubungi hotline kami di +6281336661922 untuk informasi lebih lanjut</span></span>
                                </li>
                            </ul>

                        </div></div>
                </div>
            </div>
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "terms-condition.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 108,  131 => 93,  113 => 78,  40 => 7,  37 => 6,  32 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends 'layout/main.twig' %}*/
/* {% block title %}*/
/*     Syarat dan Ketentuan | JAWATIMURTOUR*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="container_12">*/
/*         <div class="grid_12">*/
/*             <h3>Ketentuan Umum</h3>*/
/*             <div>*/
/*                 <div class="extra_wrapper">*/
/*                     <div class="text1 col1">Syarat dan Ketentuan.</div>*/
/*                 </div>*/
/*                 <ul class="detail">*/
/*                     <li>Paket wisata masih bersifat penawaran dan bisa berubah sesuai permintaan <em>(fleksibel)</em>.</li>*/
/*                     <li>Informasi dan harga yang tercantum berlaku reguler untuk tahun 2016 <em>(tidak untuk masa high season)</em>.</li>*/
/*                     <li>Harga Berlaku Untuk WNI dan WNA Pemegang KIMS/KITAS.</li>*/
/*                     <li>Harga dapat berubah sewaktu-waktu tanpa adanya pemberitahuan sebelumnya.</li>*/
/*                     <li>Harga tidak mengikat selama belum adanya pembayaran Uang Muka.</li>*/
/*                     <li>Program dapat berubah sewaktu-waktu mengikuti keadaan lokal setempat.</li>*/
/*                     <li>Kategori anak mulai dari 2-6 tahun.</li>*/
/*                     <li>Tidak ada refund jika obyek wisata dibatalkan karena faktor force majeure, termasuk karena pengunjung yang overload, trafik jalan, cuaca dan waktu yang tidak memungkinkan.</li>*/
/*                     <li>Penjemputan / pengantaran di luar Kota Malang dikenakan biaya tambahan.</li>*/
/*                 </ul>*/
/* */
/*                 <div class="extra_wrapper">*/
/*                     <div class="text1 col1">Layanan Paket Wisata Sudah Termasuk.</div>*/
/*                 </div>*/
/*                 <ul class="detail">*/
/*                     <li>Private Tour, Tidak Digabung Dengan Grup Lain.</li>*/
/*                     <li>Private Transportasi Full AC Eksekutif (termasuk BBM)*/
/*                         <ol>*/
/*                             <li>2-6 px by All New Avanza</li>*/
/*                             <li>7-18 pax by ELF / Long ELF</li>*/
/*                             <li>18-30 pax by Medium Bus</li>*/
/*                             <li>31-50 pax by Big Bus</li>*/
/*                         </ol>*/
/*                     </li>*/
/*                     <li>Antar – Jemput Grup di Termina / Stasiun / Hotel / Lokasi Lainnya di Kota Malang.</li>*/
/*                     <li>Biaya Penginapan <em>*sesuai kesepakatan</em>.</li>*/
/*                     <li>Tour Leader Professional & Ramah.</li>*/
/*                     <li>Pemandu Lokal di : Pulau Sempu / Kawah Ijen / Pulau Menjangan.</li>*/
/*                     <li>Penggunaan Kapal di : Pulau Sempu / Pulau Menjangan.</li>*/
/*                     <li>Peralatan Keselamatan Diri di Pulau Menjangan.</li>*/
/*                     <li>Peralatan Snorkeling di Pulau Menjangan.</li>*/
/*                     <li>Sewa Jeep di Bromo.</li>*/
/*                     <li>Penggunaan Masker di Kawah Ijen.</li>*/
/*                     <li>Air Mineral.</li>*/
/*                     <li>Welcome Snack.</li>*/
/*                     <li>Free Spanduk Tour <em>(khusus diatas 20 peserta)</em>.</li>*/
/*                     <li>Biaya Parkir.</li>*/
/*                     <li>Souvenir.</li>*/
/*                     <li>First Aid.</li>*/
/*                 </ul>*/
/* */
/*                 <div class="extra_wrapper">*/
/*                     <div class="text1 col1">Layanan Paket Wisata Belum Termasuk.</div>*/
/*                 </div>*/
/*                 <ul class="detail">*/
/*                     <li>Tiket Kota Asal – Malang PP.</li>*/
/*                     <li>High Season Surcharge Saat Long Weekend / Libur Panjang Nasional.</li>*/
/*                     <li>Pengeluaran pribadi.</li>*/
/*                     <li>Asuransi perjalanan selama tour.</li>*/
/*                     <li>Tips untuk : Porter, Guide, Driver, Bellboy.</li>*/
/*                     <li>Biaya atau jasa lainnya yang tidak tercantum diatas.</li>*/
/*                 </ul>*/
/*                 <hr>*/
/*                 <h1 class="promo_title">Layanan Lainnya : Sewa Mobil, Pick Up - Drop Off, Bus Parisiwata</h1>*/
/*                 <div class="container_12">*/
/*                     <div class="grid_4">*/
/*                         <div class="p_table_1">*/
/*                             <div class="column_ribbon ribbon_style1_hot"></div>*/
/*                             <ul>*/
/*                                 <li class="header"><span>Sewa Mobil Fullday</span></li>*/
/*                                 <li class="content">*/
/*                                     <span class="suffix">Rp.</span> <span>550 K</span>*/
/*                                     <p>include bbm + parkir + makan driver</p>*/
/*                                     <a class="button_more" href="{{ site_url('frontend/service') }}">LEARN MORE</a>*/
/*                                 </li>*/
/* */
/*                                 <li style="height: 55px;" class="css3_grid_row_2 footer_row css3_grid_row_2_responsive"><span class="css3_grid_vertical_align_table"><span class="css3_grid_vertical_align">Harga di atas berlaku hanya untuk hari biasa, untuk hari libur (tahun baru / lebaran) tidak berlaku!. Hubungi Hotline Kami di nomor +6281336661922 </span></span>*/
/*                                 </li>*/
/*                             </ul>*/
/* */
/*                         </div></div>*/
/*                     <div class="grid_4">*/
/*                         <div class="p_table_1">*/
/*                             <ul>*/
/*                                 <li class="header"><span> Pick Up / Drop Off Juanda</span></li>*/
/*                                 <li class="content">*/
/*                                     <span class="suffix">Rp.</span> <span>500 K</span>*/
/*                                     <p>kapasitas 7 orang</p>*/
/*                                     <a class="button_more" href="{{ site_url('frontend/service') }}">LEARN MORE</a>*/
/*                                 </li>*/
/* */
/*                                 <li style="height: 55px;" class="css3_grid_row_2 footer_row css3_grid_row_2_responsive"><span class="css3_grid_vertical_align_table"><span class="css3_grid_vertical_align"> Privasi anda prioritas kami, walaupun anda hanya 1 orang / 2 orang maka mobil tersebut tidak mengisi dengan penumpang lain. Hotline Kami +6281336661922</span></span>*/
/*                                 </li>*/
/*                             </ul>*/
/* */
/*                         </div></div>*/
/*                     <div class="grid_4">*/
/*                         <div class="p_table_1">*/
/*                             <ul>*/
/*                                 <li class="header"><span>Sewa Bus Wisata</span></li>*/
/*                                 <li class="content">*/
/*                                     <span class="suffix">-.</span> <span>CALL</span>*/
/*                                     <p> kap 30 seat, ac, tv lcd, audio</p>*/
/*                                     <a class="button_more" href="{{ site_url('frontend/service') }}">LEARN MORE</a>*/
/*                                 </li>*/
/* */
/*                                 <li style="height: 55px;" class="css3_grid_row_2 footer_row css3_grid_row_2_responsive"><span class="css3_grid_vertical_align_table"><span class="css3_grid_vertical_align">Kami menyediakan layanan sewa bus pariwisata untuk semua tujuan. Silakan hubungi hotline kami di +6281336661922 untuk informasi lebih lanjut</span></span>*/
/*                                 </li>*/
/*                             </ul>*/
/* */
/*                         </div></div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/* {% endblock %}*/
/* */
/* */
