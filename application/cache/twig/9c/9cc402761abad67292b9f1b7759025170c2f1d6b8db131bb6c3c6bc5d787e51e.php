<?php

/* layout/_paket_form.twig */
class __TwigTemplate_b64f4d8046889814aa935681f552119a3ec72b4c2a8aa55cacc5c83ffe4c0f79 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form id=\"bookingForm\" method=\"post\" action=\"";
        echo twig_escape_filter($this->env, site_url("frontend/tours/request"), "html", null, true);
        echo "\">
    <strong class=\"error-message\" style=\"display: block;\">";
        // line 2
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "flashdata", array()), "error", array()), "html", null, true);
        echo "</strong>
    <strong class=\"success-message\" style=\"display: block;\">";
        // line 3
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "flashdata", array()), "success", array()), "html", null, true);
        echo "</strong>
    <div class=\"fl1\">
        <div class=\"tmInput\">
            <input name=\"name\" placeHolder=\"Nama:\" type=\"text\">
        </div>
        <div class=\"tmInput\">
            <input name=\"country\" placeHolder=\"Kewarganegaraan:\">
        </div>
    </div>
    <div class=\"fl1\">
        <div class=\"tmInput\">
            <input name=\"email\" placeHolder=\"Email:\" type=\"text\">
        </div>
        <div class=\"tmInput mr0\">
            <input name=\"phone\" placeHolder=\"No Telp:\" type=\"text\">
        </div>
    </div>
    <div class=\"clear\"></div>
    <strong>Paket Trip</strong>
    <select name=\"tours\" class=\"tmSelect auto\" data-class=\"tmSelect tmSelect2 tmSelect3\">
        <option value=\"bromo-midnight\">Bromo Mignight</option>
        <option value=\"ijen-2d1n\">Ijen 2D1N</option>
        <option value=\"malang-batu-2d1n\">Malang Batu City Tour 2D1N</option>
        <option value=\"menjangan-fullday\">Menjangan Fullday</option>
        <option value=\"sempu-2d1n\">Sempu 2D1N</option>
        <option value=\"sempu-fullday\">Sempu Fullday</option>
    </select>
    <div class=\"clear\"></div>
    <strong>Check-in</strong>
    <label class=\"tmDatepicker\">
        <input type=\"text\" name=\"check_in\">
    </label>
    <div class=\"clear\"></div>
    <strong>Check-out</strong>
    <label class=\"tmDatepicker\">
        <input type=\"text\" name=\"check_out\">
    </label>
    <div class=\"clear\"></div>
    ";
        // line 42
        echo "    ";
        // line 43
        echo "    ";
        // line 44
        echo "    ";
        // line 45
        echo "    ";
        // line 46
        echo "    ";
        // line 47
        echo "    ";
        // line 48
        echo "    ";
        // line 49
        echo "    ";
        // line 50
        echo "    ";
        // line 51
        echo "    <div class=\"fl1 fl2\">
        <em>Dewasa</em>
        <select name=\"adults\" class=\"tmSelect auto\" data-class=\"tmSelect tmSelect2\">
            <option value=\"2\">2</option>
            <option value=\"3\">3</option>
            <option value=\"4\">4</option>
            <option value=\"5\">5</option>
            <option value=\"6\">6</option>
            <option value=\"7\">7</option>
            <option value=\"8\">8</option>
        </select>
        <div class=\"clear\"></div>
        ";
        // line 64
        echo "        ";
        // line 65
        echo "        ";
        // line 66
        echo "        ";
        // line 67
        echo "        ";
        // line 68
        echo "        ";
        // line 69
        echo "        ";
        // line 70
        echo "    </div>
    <div class=\"fl1 fl2\">
        <em>Anak - anak</em>
        <select name=\"children\" class=\"tmSelect auto\" data-class=\"tmSelect tmSelect2\">
            <option value=\"0\">0</option>
            <option value=\"1\">1</option>
            <option value=\"2\">2</option>
            <option value=\"3\">3</option>
            <option value=\"4\">4</option>
        </select>
    </div>
    <div class=\"clear\"></div>
    <div class=\"tmTextarea\">
        <textarea name=\"message\" placeHolder=\"Pesan\"></textarea>
    </div>
    <input type=\"submit\" class=\"btn\" value=\"Kirim\">
</form>

<script>
    \$(function (){
        \$('#bookingForm').bookingForm({
            ownerEmail: '#'
        });
    })
    \$(function() {
        \$('#bookingForm input, #bookingForm textarea').placeholder();
    });
</script>";
    }

    public function getTemplateName()
    {
        return "layout/_paket_form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 70,  111 => 69,  109 => 68,  107 => 67,  105 => 66,  103 => 65,  101 => 64,  87 => 51,  85 => 50,  83 => 49,  81 => 48,  79 => 47,  77 => 46,  75 => 45,  73 => 44,  71 => 43,  69 => 42,  28 => 3,  24 => 2,  19 => 1,);
    }
}
/* <form id="bookingForm" method="post" action="{{ site_url('frontend/tours/request') }}">*/
/*     <strong class="error-message" style="display: block;">{{ session.flashdata.error }}</strong>*/
/*     <strong class="success-message" style="display: block;">{{ session.flashdata.success }}</strong>*/
/*     <div class="fl1">*/
/*         <div class="tmInput">*/
/*             <input name="name" placeHolder="Nama:" type="text">*/
/*         </div>*/
/*         <div class="tmInput">*/
/*             <input name="country" placeHolder="Kewarganegaraan:">*/
/*         </div>*/
/*     </div>*/
/*     <div class="fl1">*/
/*         <div class="tmInput">*/
/*             <input name="email" placeHolder="Email:" type="text">*/
/*         </div>*/
/*         <div class="tmInput mr0">*/
/*             <input name="phone" placeHolder="No Telp:" type="text">*/
/*         </div>*/
/*     </div>*/
/*     <div class="clear"></div>*/
/*     <strong>Paket Trip</strong>*/
/*     <select name="tours" class="tmSelect auto" data-class="tmSelect tmSelect2 tmSelect3">*/
/*         <option value="bromo-midnight">Bromo Mignight</option>*/
/*         <option value="ijen-2d1n">Ijen 2D1N</option>*/
/*         <option value="malang-batu-2d1n">Malang Batu City Tour 2D1N</option>*/
/*         <option value="menjangan-fullday">Menjangan Fullday</option>*/
/*         <option value="sempu-2d1n">Sempu 2D1N</option>*/
/*         <option value="sempu-fullday">Sempu Fullday</option>*/
/*     </select>*/
/*     <div class="clear"></div>*/
/*     <strong>Check-in</strong>*/
/*     <label class="tmDatepicker">*/
/*         <input type="text" name="check_in">*/
/*     </label>*/
/*     <div class="clear"></div>*/
/*     <strong>Check-out</strong>*/
/*     <label class="tmDatepicker">*/
/*         <input type="text" name="check_out">*/
/*     </label>*/
/*     <div class="clear"></div>*/
/*     {#<div class="tmRadio">#}*/
/*     {#<p>Comfort</p>#}*/
/*     {#<input name="Comfort" type="radio" id="tmRadio0" data-constraints='@RadioGroupChecked(name="Comfort", groups=[RadioGroup])' checked/>#}*/
/*     {#<span>Cheap</span>#}*/
/*     {#<input name="Comfort" type="radio" id="tmRadio1" data-constraints='@RadioGroupChecked(name="Comfort", groups=[RadioGroup])' />#}*/
/*     {#<span>Standart</span>#}*/
/*     {#<input name="Comfort" type="radio" id="tmRadio2" data-constraints='@RadioGroupChecked(name="Comfort", groups=[RadioGroup])' />#}*/
/*     {#<span>Lux</span>#}*/
/*     {#</div>#}*/
/*     {#<div class="clear"></div>#}*/
/*     <div class="fl1 fl2">*/
/*         <em>Dewasa</em>*/
/*         <select name="adults" class="tmSelect auto" data-class="tmSelect tmSelect2">*/
/*             <option value="2">2</option>*/
/*             <option value="3">3</option>*/
/*             <option value="4">4</option>*/
/*             <option value="5">5</option>*/
/*             <option value="6">6</option>*/
/*             <option value="7">7</option>*/
/*             <option value="8">8</option>*/
/*         </select>*/
/*         <div class="clear"></div>*/
/*         {#<em>Rooms</em>#}*/
/*         {#<select name="Rooms" class="tmSelect auto" data-class="tmSelect tmSelect2" data-constraints="">#}*/
/*         {#<option>1</option>#}*/
/*         {#<option>1</option>#}*/
/*         {#<option>2</option>#}*/
/*         {#<option>3</option>#}*/
/*         {#</select>#}*/
/*     </div>*/
/*     <div class="fl1 fl2">*/
/*         <em>Anak - anak</em>*/
/*         <select name="children" class="tmSelect auto" data-class="tmSelect tmSelect2">*/
/*             <option value="0">0</option>*/
/*             <option value="1">1</option>*/
/*             <option value="2">2</option>*/
/*             <option value="3">3</option>*/
/*             <option value="4">4</option>*/
/*         </select>*/
/*     </div>*/
/*     <div class="clear"></div>*/
/*     <div class="tmTextarea">*/
/*         <textarea name="message" placeHolder="Pesan"></textarea>*/
/*     </div>*/
/*     <input type="submit" class="btn" value="Kirim">*/
/* </form>*/
/* */
/* <script>*/
/*     $(function (){*/
/*         $('#bookingForm').bookingForm({*/
/*             ownerEmail: '#'*/
/*         });*/
/*     })*/
/*     $(function() {*/
/*         $('#bookingForm input, #bookingForm textarea').placeholder();*/
/*     });*/
/* </script>*/
