<?php

/* tours.twig */
class __TwigTemplate_7b358661624162fa471babef1eef612308b6a8324be43e99989c52a314de3e16 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout/main.twig", "tours.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/main.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "    Contact Us - JAWATIMURTOUR
";
    }

    // line 6
    public function block_content($context, array $blocks = array())
    {
        // line 7
        echo "    <div class=\"container_12\">
        <div class=\"banners\">
            <div class=\"grid_4\">
                <div class=\"banner\">
                    <img src=\"";
        // line 11
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-beach-fullday.jpg"), "html", null, true);
        echo "\" alt=\"\">
                    <div class=\"label\">
                        <div class=\"title\">Fullday</div>
                        <div class=\"title\">Beach Of Malang</div>
                        <div class=\"price\"><span>CALL</span></div>
                        ";
        // line 17
        echo "                    </div>
                </div>
            </div>
            <div class=\"grid_4\">
                <div class=\"banner\">
                    <img src=\"";
        // line 22
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-sempu.jpg"), "html", null, true);
        echo "\" alt=\"\">
                    <div class=\"label\">
                        <div class=\"title\">Fullday</div>
                        <div class=\"title\">Sempu Island</div>
                        <div class=\"price\">from<span>Rp 199.999</span></div>
                        <a href=\"";
        // line 27
        echo twig_escape_filter($this->env, site_url("frontend/tours/sempu-fullday"), "html", null, true);
        echo "\">LEARN MORE</a>
                    </div>
                </div>
            </div>
            <div class=\"grid_4\">
                <div class=\"banner\">
                    <img src=\"";
        // line 33
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-sempu-2d1n.jpg"), "html", null, true);
        echo "\" alt=\"\">
                    <div class=\"label\">
                        <div class=\"title\">2D1N</div>
                        <div class=\"title\">Sempu Island</div>
                        <div class=\"price\">from<span>Rp 399.999</span></div>
                        <a href=\"";
        // line 38
        echo twig_escape_filter($this->env, site_url("frontend/tours/sempu-2d1n"), "html", null, true);
        echo "\">LEARN MORE</a>
                    </div>
                </div>
            </div>
            <div class=\"clear\"></div>
            <div class=\"grid_4\">
                <div class=\"banner\">
                    <img src=\"";
        // line 45
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-citytour-fullday-1.jpg"), "html", null, true);
        echo "\" alt=\"\">
                    <div class=\"label\">
                        <div class=\"title\">Fullday</div>
                        <div class=\"title\">Batu City Tour</div>
                        <div class=\"price\"><span>CALL</span></div>
                        ";
        // line 51
        echo "                    </div>
                </div>
            </div>
            <div class=\"grid_4\">
                <div class=\"banner\">
                    <img src=\"";
        // line 56
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-citytour-2d1n.jpg"), "html", null, true);
        echo "\" alt=\"\">
                    <div class=\"label\">
                        <div class=\"title\">2D1N</div>
                        <div class=\"title\">City Tour</div>
                        <div class=\"price\">from<span>Rp 850.000</span></div>
                        <a href=\"";
        // line 61
        echo twig_escape_filter($this->env, site_url("frontend/tours/malang-batu-city-tour-2d1n"), "html", null, true);
        echo "\">LEARN MORE</a>
                    </div>
                </div>
            </div>
            <div class=\"grid_4\">
                <div class=\"banner\">
                    <img src=\"";
        // line 67
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-citytour-3d1n.jpg"), "html", null, true);
        echo "\" alt=\"\">
                    <div class=\"label\">
                        <div class=\"title\">3D2N</div>
                        <div class=\"title\">City Tour</div>
                        <div class=\"price\"><span>CALL</span></div>
                        ";
        // line 73
        echo "                    </div>
                </div>
            </div>
            <div class=\"clear\"></div>
            <div class=\"grid_4\">
                <div class=\"banner\">
                    <img src=\"";
        // line 79
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-bromo.jpg"), "html", null, true);
        echo "\" alt=\"\">
                    <div class=\"label\">
                        <div class=\"title\">Midnight</div>
                        <div class=\"title\">Bromo</div>
                        <div class=\"price\">from<span>Rp 375.000</span></div>
                        <a href=\"";
        // line 84
        echo twig_escape_filter($this->env, site_url("frontend/tours/bromo-midnight"), "html", null, true);
        echo "\">LEARN MORE</a>
                    </div>
                </div>
            </div>
            <div class=\"grid_4\">
                <div class=\"banner\">
                    <img src=\"";
        // line 90
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-menjangan.jpg"), "html", null, true);
        echo "\" alt=\"\">
                    <div class=\"label\">
                        <div class=\"title\">Snorkeling</div>
                        <div class=\"title\">Menjangan Island</div>
                        <div class=\"price\">from<span>Rp 450.000</span></div>
                        <a href=\"";
        // line 95
        echo twig_escape_filter($this->env, site_url("frontend/tours/menjangan-fullday"), "html", null, true);
        echo "\">LEARN MORE</a>
                    </div>
                </div>
            </div>
            <div class=\"grid_4\">
                <div class=\"banner\">
                    <img src=\"";
        // line 101
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-ijen.jpg"), "html", null, true);
        echo "\" alt=\"\">
                    <div class=\"label\">
                        <div class=\"title\">Bluefire</div>
                        <div class=\"title\">Ijen Crater</div>
                        <div class=\"price\">from<span>Rp 699.999</span></div>
                        <a href=\"";
        // line 106
        echo twig_escape_filter($this->env, site_url("frontend/tours/ijen-2d1n"), "html", null, true);
        echo "\">LEARN MORE</a>
                    </div>
                </div>
            </div>
        </div>

        <div class=\"grid_12\">
            <h3>Ketentuan Umum</h3>
            <div>
                <div class=\"extra_wrapper\">
                    <div class=\"text1 col1\">Syarat dan Ketentuan.</div>
                </div>
                <ul class=\"detail\">
                    <li>Paket wisata masih bersifat penawaran dan bisa berubah sesuai permintaan <em>(fleksibel)</em>.</li>
                    <li>Informasi dan harga yang tercantum berlaku reguler untuk tahun 2016 <em>(tidak untuk masa high season)</em>.</li>
                    <li>Harga Berlaku Untuk WNI dan WNA Pemegang KIMS/KITAS.</li>
                    <li>Harga dapat berubah sewaktu-waktu tanpa adanya pemberitahuan sebelumnya.</li>
                    <li>Harga tidak mengikat selama belum adanya pembayaran Uang Muka.</li>
                    <li>Program dapat berubah sewaktu-waktu mengikuti keadaan lokal setempat.</li>
                    <li>Kategori anak mulai dari 2-6 tahun.</li>
                    <li>Tidak ada refund jika obyek wisata dibatalkan karena faktor force majeure, termasuk karena pengunjung yang overload, trafik jalan, cuaca dan waktu yang tidak memungkinkan.</li>
                    <li>Penjemputan / pengantaran di luar Kota Malang dikenakan biaya tambahan.</li>
                </ul>

                <div class=\"extra_wrapper\">
                    <div class=\"text1 col1\">Layanan Paket Wisata Sudah Termasuk.</div>
                </div>
                <ul class=\"detail\">
                    <li>Private Tour, Tidak Digabung Dengan Grup Lain.</li>
                    <li>Private Transportasi Full AC Eksekutif (termasuk BBM)
                        <ol>
                            <li>2-6 px by All New Avanza</li>
                            <li>7-18 pax by ELF / Long ELF</li>
                            <li>18-30 pax by Medium Bus</li>
                            <li>31-50 pax by Big Bus</li>
                        </ol>
                    </li>
                    <li>Antar – Jemput Grup di Termina / Stasiun / Hotel / Lokasi Lainnya di Kota Malang.</li>
                    <li>Biaya Penginapan <em>*sesuai kesepakatan</em>.</li>
                    <li>Tour Leader Professional & Ramah.</li>
                    <li>Pemandu Lokal di : Pulau Sempu / Kawah Ijen / Pulau Menjangan.</li>
                    <li>Penggunaan Kapal di : Pulau Sempu / Pulau Menjangan.</li>
                    <li>Peralatan Keselamatan Diri di Pulau Menjangan.</li>
                    <li>Peralatan Camping di Pulau Sempu.</li>
                    <li>Peralatan Snorkeling di Pulau Menjangan.</li>
                    <li>Sewa Jeep di Bromo.</li>
                    <li>Penggunaan Masker di Kawah Ijen.</li>
                    <li>Air Mineral.</li>
                    <li>Welcome Snack.</li>
                    <li>Free Spanduk Tour <em>(khusus diatas 20 peserta)</em>.</li>
                    <li>Biaya Parkir.</li>
                    <li>Souvenir.</li>
                    <li>First Aid.</li>
                </ul>

                <div class=\"extra_wrapper\">
                    <div class=\"text1 col1\">Layanan Paket Wisata Belum Termasuk.</div>
                </div>
                <ul class=\"detail\">
                    <li>Tiket Kota Asal – Malang PP.</li>
                    <li>High Season Surcharge Saat Long Weekend / Libur Panjang Nasional.</li>
                    <li>Pengeluaran pribadi.</li>
                    <li>Asuransi perjalanan selama tour.</li>
                    <li>Tips untuk : Porter, Guide, Driver, Bellboy.</li>
                    <li>Biaya atau jasa lainnya yang tidak tercantum diatas.</li>
                </ul>
                <hr>
                <h1 class=\"promo_title\">Layanan Lainnya : Sewa Mobil, Pick Up - Drop Off, Bus Parisiwata</h1>
                <div class=\"container_12\">
                    ";
        // line 175
        $this->loadTemplate("layout/_relevan_post.twig", "tours.twig", 175)->display($context);
        // line 176
        echo "                </div>
            </div>
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "tours.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  260 => 176,  258 => 175,  186 => 106,  178 => 101,  169 => 95,  161 => 90,  152 => 84,  144 => 79,  136 => 73,  128 => 67,  119 => 61,  111 => 56,  104 => 51,  96 => 45,  86 => 38,  78 => 33,  69 => 27,  61 => 22,  54 => 17,  46 => 11,  40 => 7,  37 => 6,  32 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends 'layout/main.twig' %}*/
/* {% block title %}*/
/*     Contact Us - JAWATIMURTOUR*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="container_12">*/
/*         <div class="banners">*/
/*             <div class="grid_4">*/
/*                 <div class="banner">*/
/*                     <img src="{{ base_url('assets/images/thumbnail-beach-fullday.jpg') }}" alt="">*/
/*                     <div class="label">*/
/*                         <div class="title">Fullday</div>*/
/*                         <div class="title">Beach Of Malang</div>*/
/*                         <div class="price"><span>CALL</span></div>*/
/*                         {#<a href="#">LEARN MORE</a>#}*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="grid_4">*/
/*                 <div class="banner">*/
/*                     <img src="{{ base_url('assets/images/thumbnail-sempu.jpg') }}" alt="">*/
/*                     <div class="label">*/
/*                         <div class="title">Fullday</div>*/
/*                         <div class="title">Sempu Island</div>*/
/*                         <div class="price">from<span>Rp 199.999</span></div>*/
/*                         <a href="{{ site_url('frontend/tours/sempu-fullday') }}">LEARN MORE</a>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="grid_4">*/
/*                 <div class="banner">*/
/*                     <img src="{{ base_url('assets/images/thumbnail-sempu-2d1n.jpg') }}" alt="">*/
/*                     <div class="label">*/
/*                         <div class="title">2D1N</div>*/
/*                         <div class="title">Sempu Island</div>*/
/*                         <div class="price">from<span>Rp 399.999</span></div>*/
/*                         <a href="{{ site_url('frontend/tours/sempu-2d1n') }}">LEARN MORE</a>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="clear"></div>*/
/*             <div class="grid_4">*/
/*                 <div class="banner">*/
/*                     <img src="{{ base_url('assets/images/thumbnail-citytour-fullday-1.jpg') }}" alt="">*/
/*                     <div class="label">*/
/*                         <div class="title">Fullday</div>*/
/*                         <div class="title">Batu City Tour</div>*/
/*                         <div class="price"><span>CALL</span></div>*/
/*                         {#<a href="#">LEARN MORE</a>#}*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="grid_4">*/
/*                 <div class="banner">*/
/*                     <img src="{{ base_url('assets/images/thumbnail-citytour-2d1n.jpg') }}" alt="">*/
/*                     <div class="label">*/
/*                         <div class="title">2D1N</div>*/
/*                         <div class="title">City Tour</div>*/
/*                         <div class="price">from<span>Rp 850.000</span></div>*/
/*                         <a href="{{ site_url('frontend/tours/malang-batu-city-tour-2d1n') }}">LEARN MORE</a>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="grid_4">*/
/*                 <div class="banner">*/
/*                     <img src="{{ base_url('assets/images/thumbnail-citytour-3d1n.jpg') }}" alt="">*/
/*                     <div class="label">*/
/*                         <div class="title">3D2N</div>*/
/*                         <div class="title">City Tour</div>*/
/*                         <div class="price"><span>CALL</span></div>*/
/*                         {#<a href="#">LEARN MORE</a>#}*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="clear"></div>*/
/*             <div class="grid_4">*/
/*                 <div class="banner">*/
/*                     <img src="{{ base_url('assets/images/thumbnail-bromo.jpg') }}" alt="">*/
/*                     <div class="label">*/
/*                         <div class="title">Midnight</div>*/
/*                         <div class="title">Bromo</div>*/
/*                         <div class="price">from<span>Rp 375.000</span></div>*/
/*                         <a href="{{ site_url('frontend/tours/bromo-midnight') }}">LEARN MORE</a>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="grid_4">*/
/*                 <div class="banner">*/
/*                     <img src="{{ base_url('assets/images/thumbnail-menjangan.jpg') }}" alt="">*/
/*                     <div class="label">*/
/*                         <div class="title">Snorkeling</div>*/
/*                         <div class="title">Menjangan Island</div>*/
/*                         <div class="price">from<span>Rp 450.000</span></div>*/
/*                         <a href="{{ site_url('frontend/tours/menjangan-fullday') }}">LEARN MORE</a>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="grid_4">*/
/*                 <div class="banner">*/
/*                     <img src="{{ base_url('assets/images/thumbnail-ijen.jpg') }}" alt="">*/
/*                     <div class="label">*/
/*                         <div class="title">Bluefire</div>*/
/*                         <div class="title">Ijen Crater</div>*/
/*                         <div class="price">from<span>Rp 699.999</span></div>*/
/*                         <a href="{{ site_url('frontend/tours/ijen-2d1n') }}">LEARN MORE</a>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/* */
/*         <div class="grid_12">*/
/*             <h3>Ketentuan Umum</h3>*/
/*             <div>*/
/*                 <div class="extra_wrapper">*/
/*                     <div class="text1 col1">Syarat dan Ketentuan.</div>*/
/*                 </div>*/
/*                 <ul class="detail">*/
/*                     <li>Paket wisata masih bersifat penawaran dan bisa berubah sesuai permintaan <em>(fleksibel)</em>.</li>*/
/*                     <li>Informasi dan harga yang tercantum berlaku reguler untuk tahun 2016 <em>(tidak untuk masa high season)</em>.</li>*/
/*                     <li>Harga Berlaku Untuk WNI dan WNA Pemegang KIMS/KITAS.</li>*/
/*                     <li>Harga dapat berubah sewaktu-waktu tanpa adanya pemberitahuan sebelumnya.</li>*/
/*                     <li>Harga tidak mengikat selama belum adanya pembayaran Uang Muka.</li>*/
/*                     <li>Program dapat berubah sewaktu-waktu mengikuti keadaan lokal setempat.</li>*/
/*                     <li>Kategori anak mulai dari 2-6 tahun.</li>*/
/*                     <li>Tidak ada refund jika obyek wisata dibatalkan karena faktor force majeure, termasuk karena pengunjung yang overload, trafik jalan, cuaca dan waktu yang tidak memungkinkan.</li>*/
/*                     <li>Penjemputan / pengantaran di luar Kota Malang dikenakan biaya tambahan.</li>*/
/*                 </ul>*/
/* */
/*                 <div class="extra_wrapper">*/
/*                     <div class="text1 col1">Layanan Paket Wisata Sudah Termasuk.</div>*/
/*                 </div>*/
/*                 <ul class="detail">*/
/*                     <li>Private Tour, Tidak Digabung Dengan Grup Lain.</li>*/
/*                     <li>Private Transportasi Full AC Eksekutif (termasuk BBM)*/
/*                         <ol>*/
/*                             <li>2-6 px by All New Avanza</li>*/
/*                             <li>7-18 pax by ELF / Long ELF</li>*/
/*                             <li>18-30 pax by Medium Bus</li>*/
/*                             <li>31-50 pax by Big Bus</li>*/
/*                         </ol>*/
/*                     </li>*/
/*                     <li>Antar – Jemput Grup di Termina / Stasiun / Hotel / Lokasi Lainnya di Kota Malang.</li>*/
/*                     <li>Biaya Penginapan <em>*sesuai kesepakatan</em>.</li>*/
/*                     <li>Tour Leader Professional & Ramah.</li>*/
/*                     <li>Pemandu Lokal di : Pulau Sempu / Kawah Ijen / Pulau Menjangan.</li>*/
/*                     <li>Penggunaan Kapal di : Pulau Sempu / Pulau Menjangan.</li>*/
/*                     <li>Peralatan Keselamatan Diri di Pulau Menjangan.</li>*/
/*                     <li>Peralatan Camping di Pulau Sempu.</li>*/
/*                     <li>Peralatan Snorkeling di Pulau Menjangan.</li>*/
/*                     <li>Sewa Jeep di Bromo.</li>*/
/*                     <li>Penggunaan Masker di Kawah Ijen.</li>*/
/*                     <li>Air Mineral.</li>*/
/*                     <li>Welcome Snack.</li>*/
/*                     <li>Free Spanduk Tour <em>(khusus diatas 20 peserta)</em>.</li>*/
/*                     <li>Biaya Parkir.</li>*/
/*                     <li>Souvenir.</li>*/
/*                     <li>First Aid.</li>*/
/*                 </ul>*/
/* */
/*                 <div class="extra_wrapper">*/
/*                     <div class="text1 col1">Layanan Paket Wisata Belum Termasuk.</div>*/
/*                 </div>*/
/*                 <ul class="detail">*/
/*                     <li>Tiket Kota Asal – Malang PP.</li>*/
/*                     <li>High Season Surcharge Saat Long Weekend / Libur Panjang Nasional.</li>*/
/*                     <li>Pengeluaran pribadi.</li>*/
/*                     <li>Asuransi perjalanan selama tour.</li>*/
/*                     <li>Tips untuk : Porter, Guide, Driver, Bellboy.</li>*/
/*                     <li>Biaya atau jasa lainnya yang tidak tercantum diatas.</li>*/
/*                 </ul>*/
/*                 <hr>*/
/*                 <h1 class="promo_title">Layanan Lainnya : Sewa Mobil, Pick Up - Drop Off, Bus Parisiwata</h1>*/
/*                 <div class="container_12">*/
/*                     {% include 'layout/_relevan_post.twig' %}*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/* {% endblock %}*/
