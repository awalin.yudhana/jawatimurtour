<?php

/* layout/_sidebar.twig */
class __TwigTemplate_a2a95385574b2d30d34bba02375e492b5f9678ce0193306d5ad753ab45f1f019 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h3 class=\"title\">CONTACT INFO</h3>
<div class=\"map\">
    <address>
        <dl>
            <dt>JAWATIMURTOUR OFFICE. <br>
                Griya Tanjung Priok Jaya 1 <br>
                Blok F-6,<br>
                Bakalan Krajan 65148, Sukun, Malang.
            </dt>
            <dd><span>Office Hours : </span><br> 08:30 AM - 17:00 PM</dd>
            <dd><span>Telephone : </span><br>+8261336661922</dd>
            <dd>E-mail : <br><a href=\"#\" class=\"col1\">service@jawatimurtour.com</a></dd>
        </dl>
    </address>
</div>

<h3 class=\"title\">Paket Lainnya</h3>
<div class=\"atgrid--widget\">
    <div class=\"atgrid atgrid--small\">
        <div class=\"atgrid__item\">
            <div class=\"atgrid__item__top\">
                <img width=\"270\" height=\"180\" src=\"";
        // line 22
        echo twig_escape_filter($this->env, base_url("assets/images/slider-sempu.jpg"), "html", null, true);
        echo "\" alt=\"paket-pulau-sempu\">\t\t\t\t<div class=\"atgrid__item__angle-wrap\">
                    <div class=\"atgrid__item__angle\" style=\"background-color:#ffed00\">FAVOURITE!</div>
                </div>
                <h4 class=\"atgrid__item__top__title\">
                    <a href=\"";
        // line 26
        echo twig_escape_filter($this->env, site_url("frontend/tours/sempu-fullday"), "html", null, true);
        echo "\">Fullday - Sempu Island</a></h4>
            </div>
        </div>
        <div class=\"atgrid__item\">
            <div class=\"atgrid__item__top\">
                <img width=\"270\" height=\"180\" src=\"";
        // line 31
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-bromo.jpg"), "html", null, true);
        echo "\" alt=\"paket-wisata-bromo\">
                <div class=\"atgrid__item__angle-wrap\">
                    <div class=\"atgrid__item__angle\" style=\"background-color:#007dff\">BEST SELLER!</div>
                </div>
                <h4 class=\"atgrid__item__top__title\">
                    <a href=\"";
        // line 36
        echo twig_escape_filter($this->env, site_url("frontend/tours/bromo-midnight"), "html", null, true);
        echo "\">Midnight - Bromo</a>
                </h4>
            </div>
        </div>
        <div class=\"atgrid__item\">
            <div class=\"atgrid__item__top\">
                <img width=\"270\" height=\"180\" src=\"";
        // line 42
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-citytour-2d1n.jpg"), "html", null, true);
        echo "\" alt=\"paket-wisata-batu\">
                <div class=\"atgrid__item__angle-wrap\">
                    <div class=\"atgrid__item__angle\" style=\"background-color:#ff0000\">NEW!</div>
                </div>
                <h4 class=\"atgrid__item__top__title\">
                    <a href=\"";
        // line 47
        echo twig_escape_filter($this->env, site_url("frontend/tours/malang-batu-city-tour-2d1n"), "html", null, true);
        echo "\">2D1N - Malang Batu City Tour</a>
                </h4>
            </div>
        </div>
    </div><!-- .atgrid -->
</div>

<h3 class=\"title\">Fan Page</h3>
<iframe src=\"https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fjawatimurtour.id%2F&tabs=timeline&width=220&height=400&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=true&appId=1522459754718112\" width=\"220\" height=\"400\" style=\"border:none;overflow:hidden\" scrolling=\"no\" frameborder=\"0\" allowTransparency=\"true\"></iframe>

<h3 class=\"title\">Twitter</h3>
<a class=\"twitter-timeline\" href=\"https://twitter.com/jawatimurtour\" data-widget-id=\"731784086381944832\">Tweets by @jawatimurtour</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\"://platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);}}(document,\"script\",\"twitter-wjs\");</script>
";
    }

    public function getTemplateName()
    {
        return "layout/_sidebar.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 47,  74 => 42,  65 => 36,  57 => 31,  49 => 26,  42 => 22,  19 => 1,);
    }
}
/* <h3 class="title">CONTACT INFO</h3>*/
/* <div class="map">*/
/*     <address>*/
/*         <dl>*/
/*             <dt>JAWATIMURTOUR OFFICE. <br>*/
/*                 Griya Tanjung Priok Jaya 1 <br>*/
/*                 Blok F-6,<br>*/
/*                 Bakalan Krajan 65148, Sukun, Malang.*/
/*             </dt>*/
/*             <dd><span>Office Hours : </span><br> 08:30 AM - 17:00 PM</dd>*/
/*             <dd><span>Telephone : </span><br>+8261336661922</dd>*/
/*             <dd>E-mail : <br><a href="#" class="col1">service@jawatimurtour.com</a></dd>*/
/*         </dl>*/
/*     </address>*/
/* </div>*/
/* */
/* <h3 class="title">Paket Lainnya</h3>*/
/* <div class="atgrid--widget">*/
/*     <div class="atgrid atgrid--small">*/
/*         <div class="atgrid__item">*/
/*             <div class="atgrid__item__top">*/
/*                 <img width="270" height="180" src="{{ base_url('assets/images/slider-sempu.jpg') }}" alt="paket-pulau-sempu">				<div class="atgrid__item__angle-wrap">*/
/*                     <div class="atgrid__item__angle" style="background-color:#ffed00">FAVOURITE!</div>*/
/*                 </div>*/
/*                 <h4 class="atgrid__item__top__title">*/
/*                     <a href="{{ site_url('frontend/tours/sempu-fullday') }}">Fullday - Sempu Island</a></h4>*/
/*             </div>*/
/*         </div>*/
/*         <div class="atgrid__item">*/
/*             <div class="atgrid__item__top">*/
/*                 <img width="270" height="180" src="{{ base_url('assets/images/thumbnail-bromo.jpg') }}" alt="paket-wisata-bromo">*/
/*                 <div class="atgrid__item__angle-wrap">*/
/*                     <div class="atgrid__item__angle" style="background-color:#007dff">BEST SELLER!</div>*/
/*                 </div>*/
/*                 <h4 class="atgrid__item__top__title">*/
/*                     <a href="{{ site_url('frontend/tours/bromo-midnight') }}">Midnight - Bromo</a>*/
/*                 </h4>*/
/*             </div>*/
/*         </div>*/
/*         <div class="atgrid__item">*/
/*             <div class="atgrid__item__top">*/
/*                 <img width="270" height="180" src="{{ base_url('assets/images/thumbnail-citytour-2d1n.jpg') }}" alt="paket-wisata-batu">*/
/*                 <div class="atgrid__item__angle-wrap">*/
/*                     <div class="atgrid__item__angle" style="background-color:#ff0000">NEW!</div>*/
/*                 </div>*/
/*                 <h4 class="atgrid__item__top__title">*/
/*                     <a href="{{ site_url('frontend/tours/malang-batu-city-tour-2d1n') }}">2D1N - Malang Batu City Tour</a>*/
/*                 </h4>*/
/*             </div>*/
/*         </div>*/
/*     </div><!-- .atgrid -->*/
/* </div>*/
/* */
/* <h3 class="title">Fan Page</h3>*/
/* <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fjawatimurtour.id%2F&tabs=timeline&width=220&height=400&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=true&appId=1522459754718112" width="220" height="400" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>*/
/* */
/* <h3 class="title">Twitter</h3>*/
/* <a class="twitter-timeline" href="https://twitter.com/jawatimurtour" data-widget-id="731784086381944832">Tweets by @jawatimurtour</a>*/
/* <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>*/
/* */
