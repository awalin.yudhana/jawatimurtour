<?php

/* layout/_relevan_post.twig */
class __TwigTemplate_bdc3b5de8aefbae0817582336aa6ebe3abc91efa631f7497611c68d4afed8f8c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<div class=\"grid_4\">
    <div class=\"p_table_1\">
        <div class=\"column_ribbon ribbon_style1_hot\"></div>
        <ul>
            <li class=\"header\"><span>Sewa Mobil Fullday</span></li>
            <li class=\"content\">
                <span class=\"suffix\">Rp.</span> <span>350 K</span>
                <p>All New Avanza + Driver</p>
                <a class=\"button_more\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("travel-carter-sewa-mobil-malang"), "html", null, true);
        echo "\">Booking</a>
            </li>

            <li style=\"height: 55px;\" class=\"css3_grid_row_2 footer_row css3_grid_row_2_responsive\"><span class=\"css3_grid_vertical_align_table\"><span class=\"css3_grid_vertical_align\">Harga di atas berlaku hanya untuk hari biasa, untuk hari libur (tahun baru / lebaran) tidak berlaku!. Hubungi Hotline Kami di nomor +6281336661922 </span></span>
            </li>
        </ul>

    </div></div>
<div class=\"grid_4\">
    <div class=\"p_table_1\">
        <ul>
            <li class=\"header\"><span> Pick Up / Drop Off Juanda</span></li>
            <li class=\"content\">
                <span class=\"suffix\">Rp.</span> <span>500 K</span>
                <p>kapasitas 7 orang</p>
                <a class=\"button_more\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, site_url("travel-carter-sewa-mobil-malang"), "html", null, true);
        echo "\">Booking</a>
            </li>

            <li style=\"height: 55px;\" class=\"css3_grid_row_2 footer_row css3_grid_row_2_responsive\"><span class=\"css3_grid_vertical_align_table\"><span class=\"css3_grid_vertical_align\"> Privasi anda prioritas kami, walaupun anda hanya 1 orang / 2 orang maka mobil tersebut tidak mengisi dengan penumpang lain. Hotline Kami +6281336661922</span></span>
            </li>
        </ul>

    </div></div>
<div class=\"grid_4\">
    <div class=\"p_table_1\">
        <ul>
            <li class=\"header\"><span>Sewa Bus Wisata</span></li>
            <li class=\"content\">
                <span class=\"suffix\">-.</span> <span>CALL</span>
                <p> kap 30 seat, ac, tv lcd, audio</p>
                <a class=\"button_more\" href=\"";
        // line 40
        echo twig_escape_filter($this->env, site_url("travel-carter-sewa-mobil-malang"), "html", null, true);
        echo "\">Booking</a>
            </li>

            <li style=\"height: 55px;\" class=\"css3_grid_row_2 footer_row css3_grid_row_2_responsive\"><span class=\"css3_grid_vertical_align_table\"><span class=\"css3_grid_vertical_align\">Kami menyediakan layanan sewa bus pariwisata untuk semua tujuan. Silakan hubungi hotline kami di +6281336661922 untuk informasi lebih lanjut</span></span>
            </li>
        </ul>

    </div></div>";
    }

    public function getTemplateName()
    {
        return "layout/_relevan_post.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 40,  48 => 25,  30 => 10,  19 => 1,);
    }
}
/* */
/* <div class="grid_4">*/
/*     <div class="p_table_1">*/
/*         <div class="column_ribbon ribbon_style1_hot"></div>*/
/*         <ul>*/
/*             <li class="header"><span>Sewa Mobil Fullday</span></li>*/
/*             <li class="content">*/
/*                 <span class="suffix">Rp.</span> <span>350 K</span>*/
/*                 <p>All New Avanza + Driver</p>*/
/*                 <a class="button_more" href="{{ site_url('travel-carter-sewa-mobil-malang') }}">Booking</a>*/
/*             </li>*/
/* */
/*             <li style="height: 55px;" class="css3_grid_row_2 footer_row css3_grid_row_2_responsive"><span class="css3_grid_vertical_align_table"><span class="css3_grid_vertical_align">Harga di atas berlaku hanya untuk hari biasa, untuk hari libur (tahun baru / lebaran) tidak berlaku!. Hubungi Hotline Kami di nomor +6281336661922 </span></span>*/
/*             </li>*/
/*         </ul>*/
/* */
/*     </div></div>*/
/* <div class="grid_4">*/
/*     <div class="p_table_1">*/
/*         <ul>*/
/*             <li class="header"><span> Pick Up / Drop Off Juanda</span></li>*/
/*             <li class="content">*/
/*                 <span class="suffix">Rp.</span> <span>500 K</span>*/
/*                 <p>kapasitas 7 orang</p>*/
/*                 <a class="button_more" href="{{ site_url('travel-carter-sewa-mobil-malang') }}">Booking</a>*/
/*             </li>*/
/* */
/*             <li style="height: 55px;" class="css3_grid_row_2 footer_row css3_grid_row_2_responsive"><span class="css3_grid_vertical_align_table"><span class="css3_grid_vertical_align"> Privasi anda prioritas kami, walaupun anda hanya 1 orang / 2 orang maka mobil tersebut tidak mengisi dengan penumpang lain. Hotline Kami +6281336661922</span></span>*/
/*             </li>*/
/*         </ul>*/
/* */
/*     </div></div>*/
/* <div class="grid_4">*/
/*     <div class="p_table_1">*/
/*         <ul>*/
/*             <li class="header"><span>Sewa Bus Wisata</span></li>*/
/*             <li class="content">*/
/*                 <span class="suffix">-.</span> <span>CALL</span>*/
/*                 <p> kap 30 seat, ac, tv lcd, audio</p>*/
/*                 <a class="button_more" href="{{ site_url('travel-carter-sewa-mobil-malang') }}">Booking</a>*/
/*             </li>*/
/* */
/*             <li style="height: 55px;" class="css3_grid_row_2 footer_row css3_grid_row_2_responsive"><span class="css3_grid_vertical_align_table"><span class="css3_grid_vertical_align">Kami menyediakan layanan sewa bus pariwisata untuk semua tujuan. Silakan hubungi hotline kami di +6281336661922 untuk informasi lebih lanjut</span></span>*/
/*             </li>*/
/*         </ul>*/
/* */
/*     </div></div>*/
