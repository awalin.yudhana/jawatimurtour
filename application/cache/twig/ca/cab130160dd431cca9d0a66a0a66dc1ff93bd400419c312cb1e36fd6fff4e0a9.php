<?php

/* contact.twig */
class __TwigTemplate_9587d72f34d225966b283b97ad316e2169c36778e5c722f404a9ec3a567ec295 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout/main.twig", "contact.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/main.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "    Contact Us - JAWATIMURTOUR
";
    }

    // line 6
    public function block_content($context, array $blocks = array())
    {
        // line 7
        echo "    <div class=\"container_12\">
        <div class=\"grid_5\">
            <h3>CONTACT INFO</h3>
            <div class=\"map\">
                <figure class=\"\">
                    <div style=\"height:311px;width:378px;max-width:100%;list-style:none; transition: none;overflow:hidden;\"><div id=\"embedded-map-canvas\" style=\"height:100%; width:100%;max-width:100%;\"><iframe style=\"height:100%;width:100%;border:0;\" frameborder=\"0\" src=\"https://www.google.com/maps/embed/v1/place?q=jawatimurtour&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU\"></iframe></div><a class=\"embed-map-html\" rel=\"nofollow\" href=\"http://www.dog-checks.com\" id=\"make-map-data\">dog checks</a><style>#embedded-map-canvas img{max-width:none!important;background:none!important;}</style></div><script src=\"https://www.dog-checks.com/google-maps-authorization.js?id=4035a2f4-be9e-0f1a-6ae9-1f9a860de15e&c=embed-map-html&u=1463027788\" defer=\"defer\" async=\"async\"></script>
                </figure>
                <address>
                    <dl>
                        <dt>JAWATIMURTOUR OFFICE. <br>
                            Griya Tanjung Priok Jaya 1 Blok F-6,<br>
                            Bakalan Krajan 65148, Sukun, Malang.
                        </dt>
                        <dd><span>Office Hours : </span>08:30 AM : 17:00 PM</dd>
                        <dd><span>Telephone : </span>+8261336661922</dd>
                        <dd>E-mail : <a href=\"#\" class=\"col1\">service@jawatimurtour.com</a></dd>
                    </dl>
                </address>
            </div>
        </div>
        <div class=\"grid_6 prefix_1\">
            <h3>GET IN TOUCH</h3>
            <form id=\"form\">
                <div class=\"success_wrapper\">
                    <div class=\"success-message\">Contact form submitted</div>
                </div>
                <label class=\"name\">
                    <input type=\"text\" placeholder=\"Name:\" data-constraints=\"@Required @JustLetters\" />
                    <span class=\"empty-message\">*This field is required.</span>
                    <span class=\"error-message\">*This is not a valid name.</span>
                </label>
                <label class=\"email\">
                    <input type=\"text\" placeholder=\"Email:\" data-constraints=\"@Required @Email\" />
                    <span class=\"empty-message\">*This field is required.</span>
                    <span class=\"error-message\">*This is not a valid email.</span>
                </label>
                <label class=\"phone\">
                    <input type=\"text\" placeholder=\"Country:\" data-constraints=\"@Required\"/>
                    <span class=\"empty-message\">*This field is required.</span>
                    <span class=\"error-message\">*This is not a valid phone.</span>
                </label>
                <label class=\"message\">
                    <textarea placeholder=\"Message:\" data-constraints='@Required @Length(min=20,max=999999)'></textarea>
                    <span class=\"empty-message\">*This field is required.</span>
                    <span class=\"error-message\">*The message is too short.</span>
                </label>
                <div>
                    <div class=\"clear\"></div>
                    <div class=\"btns\">
                        <a href=\"#\" data-type=\"reset\" class=\"btn\">Clear</a>
                        <a href=\"#\" data-type=\"submit\" class=\"btn\">Submit</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script>
        \$(function (){
            \$('#bookingForm').bookingForm({
                ownerEmail: '#'
            });
        })
    </script>
";
    }

    public function getTemplateName()
    {
        return "contact.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 7,  37 => 6,  32 => 3,  29 => 2,  11 => 1,);
    }
}
/* {% extends 'layout/main.twig' %}*/
/* {% block title %}*/
/*     Contact Us - JAWATIMURTOUR*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="container_12">*/
/*         <div class="grid_5">*/
/*             <h3>CONTACT INFO</h3>*/
/*             <div class="map">*/
/*                 <figure class="">*/
/*                     <div style="height:311px;width:378px;max-width:100%;list-style:none; transition: none;overflow:hidden;"><div id="embedded-map-canvas" style="height:100%; width:100%;max-width:100%;"><iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=jawatimurtour&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU"></iframe></div><a class="embed-map-html" rel="nofollow" href="http://www.dog-checks.com" id="make-map-data">dog checks</a><style>#embedded-map-canvas img{max-width:none!important;background:none!important;}</style></div><script src="https://www.dog-checks.com/google-maps-authorization.js?id=4035a2f4-be9e-0f1a-6ae9-1f9a860de15e&c=embed-map-html&u=1463027788" defer="defer" async="async"></script>*/
/*                 </figure>*/
/*                 <address>*/
/*                     <dl>*/
/*                         <dt>JAWATIMURTOUR OFFICE. <br>*/
/*                             Griya Tanjung Priok Jaya 1 Blok F-6,<br>*/
/*                             Bakalan Krajan 65148, Sukun, Malang.*/
/*                         </dt>*/
/*                         <dd><span>Office Hours : </span>08:30 AM : 17:00 PM</dd>*/
/*                         <dd><span>Telephone : </span>+8261336661922</dd>*/
/*                         <dd>E-mail : <a href="#" class="col1">service@jawatimurtour.com</a></dd>*/
/*                     </dl>*/
/*                 </address>*/
/*             </div>*/
/*         </div>*/
/*         <div class="grid_6 prefix_1">*/
/*             <h3>GET IN TOUCH</h3>*/
/*             <form id="form">*/
/*                 <div class="success_wrapper">*/
/*                     <div class="success-message">Contact form submitted</div>*/
/*                 </div>*/
/*                 <label class="name">*/
/*                     <input type="text" placeholder="Name:" data-constraints="@Required @JustLetters" />*/
/*                     <span class="empty-message">*This field is required.</span>*/
/*                     <span class="error-message">*This is not a valid name.</span>*/
/*                 </label>*/
/*                 <label class="email">*/
/*                     <input type="text" placeholder="Email:" data-constraints="@Required @Email" />*/
/*                     <span class="empty-message">*This field is required.</span>*/
/*                     <span class="error-message">*This is not a valid email.</span>*/
/*                 </label>*/
/*                 <label class="phone">*/
/*                     <input type="text" placeholder="Country:" data-constraints="@Required"/>*/
/*                     <span class="empty-message">*This field is required.</span>*/
/*                     <span class="error-message">*This is not a valid phone.</span>*/
/*                 </label>*/
/*                 <label class="message">*/
/*                     <textarea placeholder="Message:" data-constraints='@Required @Length(min=20,max=999999)'></textarea>*/
/*                     <span class="empty-message">*This field is required.</span>*/
/*                     <span class="error-message">*The message is too short.</span>*/
/*                 </label>*/
/*                 <div>*/
/*                     <div class="clear"></div>*/
/*                     <div class="btns">*/
/*                         <a href="#" data-type="reset" class="btn">Clear</a>*/
/*                         <a href="#" data-type="submit" class="btn">Submit</a>*/
/*                     </div>*/
/*                 </div>*/
/*             </form>*/
/*         </div>*/
/*     </div>*/
/* */
/*     <script>*/
/*         $(function (){*/
/*             $('#bookingForm').bookingForm({*/
/*                 ownerEmail: '#'*/
/*             });*/
/*         })*/
/*     </script>*/
/* {% endblock %}*/
