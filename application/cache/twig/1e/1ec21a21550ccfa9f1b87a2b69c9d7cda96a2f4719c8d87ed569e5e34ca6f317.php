<?php

/* home.twig */
class __TwigTemplate_920e8351bf13021a86c4de345313ed00a25b1581882ad8a0f5d770bd7fffeb90 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("layout/main.twig", "home.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'slider' => array($this, 'block_slider'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/main.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        // line 3
        echo "    Homepage - JAWATIMURTOUR
";
    }

    // line 6
    public function block_slider($context, array $blocks = array())
    {
        // line 7
        echo "    <div class=\"slider_wrapper\">
        <div id=\"camera_wrap\" class=\"\">
            <div data-src=\"";
        // line 9
        echo twig_escape_filter($this->env, base_url("assets/images/slider-rent-car.jpg"), "html", null, true);
        echo "\">
                <div class=\"caption fadeIn\">
                    <a href=\"";
        // line 11
        echo twig_escape_filter($this->env, site_url("sewa-mobil-malang"), "html", null, true);
        echo "\">Daftar Harga</a>
                </div>
            </div>
            <div data-src=\"";
        // line 14
        echo twig_escape_filter($this->env, base_url("assets/images/slider-bromo.jpg"), "html", null, true);
        echo "\">
                <div class=\"caption fadeIn\">
                    <h2>Midnight Bromo</h2>
                    <div class=\"price\">
                        FROM
                        <span>375K</span>
                    </div>
                    <a href=\"";
        // line 21
        echo twig_escape_filter($this->env, site_url("frontend/tours/bromo-midnight"), "html", null, true);
        echo "\">LEARN MORE</a>
                </div>
            </div>
            ";
        // line 25
        echo "                ";
        // line 26
        echo "                    ";
        // line 27
        echo "                    ";
        // line 28
        echo "                        ";
        // line 29
        echo "                        ";
        // line 30
        echo "                    ";
        // line 31
        echo "                    ";
        // line 32
        echo "                ";
        // line 33
        echo "            ";
        // line 34
        echo "            ";
        // line 35
        echo "                ";
        // line 36
        echo "                    ";
        // line 37
        echo "                    ";
        // line 38
        echo "                        ";
        // line 39
        echo "                        ";
        // line 40
        echo "                    ";
        // line 41
        echo "                    ";
        // line 42
        echo "                ";
        // line 43
        echo "            ";
        // line 44
        echo "        </div>
    </div>
";
    }

    // line 48
    public function block_content($context, array $blocks = array())
    {
        // line 49
        echo "    <div class=\"container_12\">
        <div class=\"grid_4\">
            <div class=\"banner\">
                <img src=\"";
        // line 52
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-sempu.jpg"), "html", null, true);
        echo "\" alt=\"\">
                <div class=\"label\">
                    <div class=\"title\">Fullday</div>
                    <div class=\"title\">Sempu Island</div>
                    <div class=\"price\">FROM<span>199K</span></div>
                    <a href=\"";
        // line 57
        echo twig_escape_filter($this->env, site_url("frontend/tours/sempu-fullday"), "html", null, true);
        echo "\">LEARN MORE</a>
                </div>
            </div>
        </div>
        <div class=\"grid_4\">
            <div class=\"banner\">
                <img src=\"";
        // line 63
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-bromo.jpg"), "html", null, true);
        echo "\" alt=\"\">
                <div class=\"label\">
                    <div class=\"title\">Midnight</div>
                    <div class=\"title\">Bromo</div>
                    <div class=\"price\">FROM<span>375K</span></div>
                    <a href=\"";
        // line 68
        echo twig_escape_filter($this->env, site_url("frontend/tours/bromo-midnight"), "html", null, true);
        echo "\">LEARN MORE</a>
                </div>
            </div>
        </div>
        <div class=\"grid_4\">
            <div class=\"banner\">
                <img src=\"";
        // line 74
        echo twig_escape_filter($this->env, base_url("assets/images/thumbnail-batu.jpg"), "html", null, true);
        echo "\" alt=\"\">
                <div class=\"label\">
                    <div class=\"title\">2D1N</div>
                    <div class=\"title\">Batu City Tour</div>
                    <div class=\"price\">FROM<span>850K</span></div>
                    <a href=\"";
        // line 79
        echo twig_escape_filter($this->env, site_url("frontend/tours/malang-batu-city-tour-2d1n"), "html", null, true);
        echo "\"\">LEARN MORE</a>
                </div>
            </div>
        </div>
        <div class=\"clear\"></div>
        <div class=\"grid_6\">
            <h3>Booking Form</h3>
            ";
        // line 86
        $this->loadTemplate("layout/_paket_form.twig", "home.twig", 86)->display($context);
        // line 87
        echo "        </div>
        <div class=\"grid_5 prefix_1\">
            <h3>Welcome</h3>
            <img src=\"";
        // line 90
        echo twig_escape_filter($this->env, base_url("assets/images/logo-oval.png"), "html", null, true);
        echo "\" alt=\"\" class=\"img_inner fleft\" style=\"width:100px !important;\">
            <div class=\"extra_wrapper\">
                <p><b>JAWATIMURTOUR</b> kini hadir sebagai agent tour and travel terbaik untuk anda. Dengan motto</p>
                <strong><i>\"Make Your Trip Easier\"</i></strong>
            </div>
            <div class=\"clear cl1\"></div>
            <p>kami memeberikan pelayanan terbaik bagi para pelanggan. Kepuasan dan Kenyamanan perjalanan anda adalah fokus utama kami.</p>
            <p>Dengan didukung oleh para tour guide professional yang mempunyai kemampuan multibahasa dan armada yang aman dan nyaman. Kepuasan anda adalah jaminan layanan kami</p>
            <h4>Clients’ Quotes</h4>
            <blockquote class=\"bq1\">
                <img src=\"";
        // line 100
        echo twig_escape_filter($this->env, base_url("assets/images/testimonial-pristo.jpg"), "html", null, true);
        echo "\" alt=\"\" class=\"img_inner noresize fleft\">
                <div class=\"extra_wrapper\">
                    <p>Outbound yang dangat mengesankan bersama JAWATIMURTOUR. Itenerary dapat disesuaikan saat di lapangan, belum lagi dapat MUG yang bagus. Terima kasih JAWATIMURTOUR.com sudah menjadi partner perusahaan kami.</p>
                    <div class=\"alright\">
                        <div class=\"col1\">PT. PRISTO KUKUH ABADI</div>
                        <a href=\"#\" class=\"btn\">More</a>
                    </div>
                </div>
            </blockquote>
        </div>
        <div class=\"grid_12\">
            <h3 class=\"head1\">Layanan Lainnya</h3>
        </div>
        ";
        // line 113
        $this->loadTemplate("layout/_relevan_post.twig", "home.twig", 113)->display($context);
        // line 114
        echo "    </div>
";
    }

    public function getTemplateName()
    {
        return "home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  214 => 114,  212 => 113,  196 => 100,  183 => 90,  178 => 87,  176 => 86,  166 => 79,  158 => 74,  149 => 68,  141 => 63,  132 => 57,  124 => 52,  119 => 49,  116 => 48,  110 => 44,  108 => 43,  106 => 42,  104 => 41,  102 => 40,  100 => 39,  98 => 38,  96 => 37,  94 => 36,  92 => 35,  90 => 34,  88 => 33,  86 => 32,  84 => 31,  82 => 30,  80 => 29,  78 => 28,  76 => 27,  74 => 26,  72 => 25,  66 => 21,  56 => 14,  50 => 11,  45 => 9,  41 => 7,  38 => 6,  33 => 3,  30 => 2,  11 => 1,);
    }
}
/* {% extends 'layout/main.twig' %}*/
/* {% block title %}*/
/*     Homepage - JAWATIMURTOUR*/
/* {% endblock %}*/
/* */
/* {% block slider %}*/
/*     <div class="slider_wrapper">*/
/*         <div id="camera_wrap" class="">*/
/*             <div data-src="{{ base_url('assets/images/slider-rent-car.jpg') }}">*/
/*                 <div class="caption fadeIn">*/
/*                     <a href="{{ site_url('sewa-mobil-malang') }}">Daftar Harga</a>*/
/*                 </div>*/
/*             </div>*/
/*             <div data-src="{{ base_url('assets/images/slider-bromo.jpg') }}">*/
/*                 <div class="caption fadeIn">*/
/*                     <h2>Midnight Bromo</h2>*/
/*                     <div class="price">*/
/*                         FROM*/
/*                         <span>375K</span>*/
/*                     </div>*/
/*                     <a href="{{ site_url('frontend/tours/bromo-midnight') }}">LEARN MORE</a>*/
/*                 </div>*/
/*             </div>*/
/*             {#<div data-src="{{ base_url('assets/images/slider-sempu.jpg') }}">#}*/
/*                 {#<div class="caption fadeIn">#}*/
/*                     {#<h2>Sempu Island</h2>#}*/
/*                     {#<div class="price">#}*/
/*                         {#FROM#}*/
/*                         {#<span>199K</span>#}*/
/*                     {#</div>#}*/
/*                     {#<a href="{{ site_url('frontend/tours/sempu-fullday') }}">LEARN MORE</a>#}*/
/*                 {#</div>#}*/
/*             {#</div>#}*/
/*             {#<div data-src="{{ base_url('assets/images/slider-batu.jpg') }}">#}*/
/*                 {#<div class="caption fadeIn">#}*/
/*                     {#<h2>Malang Batu City Tour</h2>#}*/
/*                     {#<div class="price">#}*/
/*                         {#FROM#}*/
/*                         {#<span>850K</span>#}*/
/*                     {#</div>#}*/
/*                     {#<a href="{{ site_url('frontend/tours/malang-batu-city-tour-2d1n') }}">LEARN MORE</a>#}*/
/*                 {#</div>#}*/
/*             {#</div>#}*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <div class="container_12">*/
/*         <div class="grid_4">*/
/*             <div class="banner">*/
/*                 <img src="{{ base_url('assets/images/thumbnail-sempu.jpg') }}" alt="">*/
/*                 <div class="label">*/
/*                     <div class="title">Fullday</div>*/
/*                     <div class="title">Sempu Island</div>*/
/*                     <div class="price">FROM<span>199K</span></div>*/
/*                     <a href="{{ site_url('frontend/tours/sempu-fullday') }}">LEARN MORE</a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <div class="grid_4">*/
/*             <div class="banner">*/
/*                 <img src="{{ base_url('assets/images/thumbnail-bromo.jpg') }}" alt="">*/
/*                 <div class="label">*/
/*                     <div class="title">Midnight</div>*/
/*                     <div class="title">Bromo</div>*/
/*                     <div class="price">FROM<span>375K</span></div>*/
/*                     <a href="{{ site_url('frontend/tours/bromo-midnight') }}">LEARN MORE</a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <div class="grid_4">*/
/*             <div class="banner">*/
/*                 <img src="{{ base_url('assets/images/thumbnail-batu.jpg') }}" alt="">*/
/*                 <div class="label">*/
/*                     <div class="title">2D1N</div>*/
/*                     <div class="title">Batu City Tour</div>*/
/*                     <div class="price">FROM<span>850K</span></div>*/
/*                     <a href="{{ site_url('frontend/tours/malang-batu-city-tour-2d1n') }}"">LEARN MORE</a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <div class="clear"></div>*/
/*         <div class="grid_6">*/
/*             <h3>Booking Form</h3>*/
/*             {% include 'layout/_paket_form.twig' %}*/
/*         </div>*/
/*         <div class="grid_5 prefix_1">*/
/*             <h3>Welcome</h3>*/
/*             <img src="{{ base_url('assets/images/logo-oval.png') }}" alt="" class="img_inner fleft" style="width:100px !important;">*/
/*             <div class="extra_wrapper">*/
/*                 <p><b>JAWATIMURTOUR</b> kini hadir sebagai agent tour and travel terbaik untuk anda. Dengan motto</p>*/
/*                 <strong><i>"Make Your Trip Easier"</i></strong>*/
/*             </div>*/
/*             <div class="clear cl1"></div>*/
/*             <p>kami memeberikan pelayanan terbaik bagi para pelanggan. Kepuasan dan Kenyamanan perjalanan anda adalah fokus utama kami.</p>*/
/*             <p>Dengan didukung oleh para tour guide professional yang mempunyai kemampuan multibahasa dan armada yang aman dan nyaman. Kepuasan anda adalah jaminan layanan kami</p>*/
/*             <h4>Clients’ Quotes</h4>*/
/*             <blockquote class="bq1">*/
/*                 <img src="{{ base_url('assets/images/testimonial-pristo.jpg') }}" alt="" class="img_inner noresize fleft">*/
/*                 <div class="extra_wrapper">*/
/*                     <p>Outbound yang dangat mengesankan bersama JAWATIMURTOUR. Itenerary dapat disesuaikan saat di lapangan, belum lagi dapat MUG yang bagus. Terima kasih JAWATIMURTOUR.com sudah menjadi partner perusahaan kami.</p>*/
/*                     <div class="alright">*/
/*                         <div class="col1">PT. PRISTO KUKUH ABADI</div>*/
/*                         <a href="#" class="btn">More</a>*/
/*                     </div>*/
/*                 </div>*/
/*             </blockquote>*/
/*         </div>*/
/*         <div class="grid_12">*/
/*             <h3 class="head1">Layanan Lainnya</h3>*/
/*         </div>*/
/*         {% include 'layout/_relevan_post.twig' %}*/
/*     </div>*/
/* {% endblock %}*/
