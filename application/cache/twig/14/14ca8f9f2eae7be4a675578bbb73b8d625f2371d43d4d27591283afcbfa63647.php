<?php

/* layout/paket-detail.twig */
class __TwigTemplate_5ad3d5fa08b5354bb84afdd607600952d9b84ec066ec7d4a6305f9c36a72afa1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'slider' => array($this, 'block_slider'),
            'content' => array($this, 'block_content'),
            'price_tag' => array($this, 'block_price_tag'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <title>";
        // line 4
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 5
        $this->loadTemplate("layout/_head.twig", "layout/paket-detail.twig", 5)->display($context);
        // line 6
        echo "</head>
<body
        ";
        // line 8
        if (((current_url() == site_url("frontend/home")) || (current_url() == site_url()))) {
            // line 9
            echo "            class=\"page1\" id=\"top\"
        ";
        }
        // line 11
        echo ">
<!--==============================header=================================-->
";
        // line 13
        $this->loadTemplate("layout/_header.twig", "layout/paket-detail.twig", 13)->display($context);
        // line 14
        $this->displayBlock('slider', $context, $blocks);
        // line 15
        echo "<!--==============================Content=================================-->
<div class=\"content\">
    <div class=\"container_12\">
        <div class=\"grid_9\">

            ";
        // line 20
        $this->displayBlock('content', $context, $blocks);
        // line 21
        echo "            <hr>
            <div class=\"container_12\">
                ";
        // line 23
        $this->loadTemplate("layout/_post_detail_form.twig", "layout/paket-detail.twig", 23)->display($context);
        // line 24
        echo "            </div>
        </div>
        <div class=\"grid_3\">
            ";
        // line 27
        $this->displayBlock('price_tag', $context, $blocks);
        // line 28
        echo "            ";
        $this->loadTemplate("layout/_sidebar.twig", "layout/paket-detail.twig", 28)->display($context);
        // line 29
        echo "        </div>
    </div>
</div>
<!--==============================footer=================================-->
";
        // line 33
        $this->loadTemplate("layout/_footer.twig", "layout/paket-detail.twig", 33)->display($context);
        // line 34
        $this->loadTemplate("layout/_footer_script.twig", "layout/paket-detail.twig", 34)->display($context);
        // line 35
        echo "</body>
</html>

";
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
    }

    // line 14
    public function block_slider($context, array $blocks = array())
    {
    }

    // line 20
    public function block_content($context, array $blocks = array())
    {
    }

    // line 27
    public function block_price_tag($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "layout/paket-detail.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  109 => 27,  104 => 20,  99 => 14,  94 => 4,  87 => 35,  85 => 34,  83 => 33,  77 => 29,  74 => 28,  72 => 27,  67 => 24,  65 => 23,  61 => 21,  59 => 20,  52 => 15,  50 => 14,  48 => 13,  44 => 11,  40 => 9,  38 => 8,  34 => 6,  32 => 5,  28 => 4,  23 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/* <head>*/
/*     <title>{% block title %}{% endblock %}</title>*/
/*     {% include 'layout/_head.twig' %}*/
/* </head>*/
/* <body*/
/*         {% if current_url() ==  site_url('frontend/home') or current_url() == site_url() %}*/
/*             class="page1" id="top"*/
/*         {% endif %}*/
/* >*/
/* <!--==============================header=================================-->*/
/* {% include 'layout/_header.twig' %}*/
/* {% block slider %}{% endblock %}*/
/* <!--==============================Content=================================-->*/
/* <div class="content">*/
/*     <div class="container_12">*/
/*         <div class="grid_9">*/
/* */
/*             {% block content %}{% endblock %}*/
/*             <hr>*/
/*             <div class="container_12">*/
/*                 {% include 'layout/_post_detail_form.twig' %}*/
/*             </div>*/
/*         </div>*/
/*         <div class="grid_3">*/
/*             {% block price_tag %}{% endblock %}*/
/*             {% include 'layout/_sidebar.twig' %}*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <!--==============================footer=================================-->*/
/* {% include 'layout/_footer.twig' %}*/
/* {% include 'layout/_footer_script.twig' %}*/
/* </body>*/
/* </html>*/
/* */
/* */
